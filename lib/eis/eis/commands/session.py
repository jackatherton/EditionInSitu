import logging
from typing import Optional, TYPE_CHECKING
from uuid import UUID

from eis.command import CommandId, EISClientCommand
from eis.user import EISUser
from satnet.command import command
from eis.constants import CLIENT, SERVER

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession

if SERVER:
    from eis.server.user import RemoteEISUser

logger = logging.getLogger(__name__)


@command(id=CommandId.ADD_USER)
class AddUserCommand(EISClientCommand):
    _fields = ['user']

    def __init__(self, user: Optional[EISUser] = None) -> None:
        super().__init__()
        self.user = user

    def handle(self, session: 'EISRemoteSession') -> None:
        if SERVER:
            if self.user is not None and isinstance(self.user, RemoteEISUser):
                self.user.session = session
                session.editor.add_user(self.user)


@command(id=CommandId.REMOVE_USER)
class RemoveUserCommand(EISClientCommand):
    _fields = ['user_id']

    def __init__(self, user_id: Optional[UUID] = None) -> None:
        super().__init__()
        self.user_id = user_id

    def handle(self, session: 'EISRemoteSession') -> None:
        if self.user_id is not None:
            session.editor.remove_user_by_id(self.user_id)
