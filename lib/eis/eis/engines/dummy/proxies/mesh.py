from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.base import DummyGraphProxyBase
from eis.graph.mesh import Mesh, MeshProxy


@proxy(DummyEngine, Mesh)
class DummyMeshProxy(MeshProxy, DummyGraphProxyBase):
    def __init__(self, engine: Engine, proxied: Mesh) -> None:
        super().__init__(enigne=engine, proxied=proxied)
