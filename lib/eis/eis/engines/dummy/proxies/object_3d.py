import logging
from typing import List, Generic, TypeVar

from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.base import DummyGraphProxyBase
from eis.graph.object_3d import Object3D, Object3DProxy
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

O = TypeVar('O', bound='Object3D')

logger = logging.getLogger(__name__)


@proxy(DummyEngine, Object3D)
class DummyObject3DProxy(Generic[O], Object3DProxy[O], DummyGraphProxyBase):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, DummyEngine)
        Object3DProxy.__init__(self, engine=engine, proxied=proxied)
        logger.info("Added a new object: {}".format(proxied.name))

    @property
    def bound_box(self) -> List[Vector3]:
        return [Vector3(), Vector3()]

    def add_child(self, child: O) -> None:
        if not child.proxy:
            logger.error("Trying to add a child ({}) without a proxy".format(child))
            return

        assert isinstance(child.proxy, DummyObject3DProxy)

    def gui(self, gui: bool) -> None:
        pass

    def remove_child(self, child: O) -> None:
        if child.proxy:
            assert isinstance(child.proxy, DummyObject3DProxy)

    def remove(self) -> None:
        pass

    def dispose(self) -> None:
        pass

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        pass
