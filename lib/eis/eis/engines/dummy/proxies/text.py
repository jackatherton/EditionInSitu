from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.object_3d import DummyObject3DProxy
from eis.graph.text import Text


@proxy(DummyEngine, Text)
class DummyTextProxy(DummyObject3DProxy[Text]):
    def __init__(self, engine: Engine, proxied: Text) -> None:
        super().__init__(engine=engine, proxied=proxied)
