from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.base import DummyGraphProxyBase
from eis.graph.texture import Texture, TextureProxy


@proxy(DummyEngine, Texture)
class DummyTextureProxy(TextureProxy, DummyGraphProxyBase):
    def __init__(self, engine: Engine, proxied: Texture) -> None:
        super().__init__(engine=engine, proxied=proxied)
