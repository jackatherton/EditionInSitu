import builtins
import logging
from math import pi
from typing import Dict, List, Optional, Tuple, cast

from direct.showbase.ShowBase import ShowBase  # type: ignore
from panda3d import core  # type: ignore

from eis.converters import FormatConverter, converter
from eis.engine import EngineConverter
from eis.engines.panda3d.constants import TEXTURE_SORT_TO_USAGE
# from eis.engines.panda3d.engine import Panda3DEngine
from eis.graph import Color, Normal, TexCoord, Vertex
from eis.graph.geometry import Geometry
from eis.graph.light import Light
from eis.graph.material import Material, MaterialTexture
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.graph.texture import Texture
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.vector4 import Vector4  # type: ignore

logger = logging.getLogger(__name__)

DEBUG_CONVERSION = False


@converter
class Panda3DFormatConverter(FormatConverter):

    @staticmethod
    def handles(extension: str) -> bool:
        return extension in ['bam', 'egg']

    @classmethod
    def import_path(cls, path: str) -> Optional[Scene]:
        # Panda might not be running, we'll need an instance
        if hasattr(builtins, "base"):
            show_base = builtins.base
            builtin = True
        else:
            show_base = ShowBase(windowType='none')
            builtin = False

        node = None
        try:
            node = show_base.loader.load_model(path)
        except IOError:
            logger.error("Could not load file \"{}\"".format(path))
        finally:
            if not builtin:
                show_base.destroy()

        if not node:
            return None

        model = Panda3DConverter.engine_to_eis(engine_graph=node, name=path[path.rindex("/") + 1:path.rindex(".")])
        if model:
            return Scene(model=model)
        else:
            return None

    @staticmethod
    def import_data(data: bytes) -> Optional[Scene]:
        raise Exception("Scenes can only be loaded from a file path.")

    @staticmethod
    def export_data(scene: Scene, path: str) -> bytes:
        raise NotImplemented("Exporting Panda3D scenes is not supported.")


class Panda3DConverter(EngineConverter[core.NodePath]):
    """
    Converter between Panda3D and EIS
    """

    @classmethod
    def engine_to_eis(cls, engine_graph: core.NodePath, name: Optional[str] = None) -> Optional[Model]:
        logger.info("Converting from Panda3D format to EIS")

        if DEBUG_CONVERSION:
            print("=============================================")
            from eis.engines.panda3d.engine import Panda3DEngine
            print(Panda3DEngine.trace_node(engine_graph))
            print("=============================================")

        textures = cls._get_textures_from_node(node=engine_graph)
        materials = cls._get_materials_from_node(node=engine_graph, textures=textures)
        root, meshes = cls._process_nodes(node=engine_graph, materials=materials)

        if root:
            logger.info("Done!")
            if name:
                root.name = name

            model = Model(
                root=root,
                meshes=meshes,
                materials=list(materials.values()),
                textures=list(textures.values())
            )

            if DEBUG_CONVERSION:
                print(model.trace())

            return model
        else:
            logger.error("No root node returned while trying to convert from panda node to EIS")
            return None

    @staticmethod
    def _get_textures_from_node(node: core.NodePath) -> Dict[str, Texture]:
        textures = {}  # type: Dict[str, Texture]
        for p3d_texture in node.find_all_textures():
            # We consider all images as 8bits per channel, RGBA
            # (except for DXT1 and DXT5 compressed textures)
            p3d_compression = p3d_texture.get_compression()
            p3d_component_type = p3d_texture.get_component_type()
            if p3d_compression == core.Texture.CM_dxt1:
                format = 'rgb'
                compression = Texture.Compression.DXT1
                data = bytes(p3d_texture.get_ram_image())
                component_type = Texture.ComponentType.UNKNOWN
            elif p3d_compression == core.Texture.CM_dxt5:
                format = 'rgba'
                compression = Texture.Compression.DXT5
                data = bytes(p3d_texture.get_ram_image())
                component_type = Texture.ComponentType.UNKNOWN
            else:
                format = 'rgba'
                compression = Texture.Compression.NONE
                data = bytes(p3d_texture.get_ram_image_as("RGBA"))
                if p3d_component_type == core.Texture.T_unsigned_byte:
                    component_type = Texture.ComponentType.UNSIGNED_BYTE
                elif p3d_component_type == core.Texture.T_unsigned_short:
                    component_type = Texture.ComponentType.UNSIGNED_SHORT
                elif p3d_component_type == core.Texture.T_float:
                    component_type = Texture.ComponentType.FLOAT
                else:
                    raise ValueError("Wrong component type")

            # No proxy used here, we want to convert and forget about the source data, not wrap it
            texture = Texture(
                name=p3d_texture.name,
                data=data,
                format=format,
                size_x=p3d_texture.x_size,
                size_y=p3d_texture.y_size,
                component_type=component_type,
                compression=compression
            )

            textures[texture.name] = texture
        return textures

    @staticmethod
    def _get_materials_from_node(node: core.NodePath, textures: Dict[str, Texture]) -> Dict[str, Material]:
        materials: Dict[str, Material] = {}

        def process_node(node_path: core.NodePath) -> None:
            p3d_node = node_path.node()
            if p3d_node.is_geom_node() and p3d_node.get_num_geoms() > 0:
                for geom_idx in range(p3d_node.get_num_geoms()):
                    geom_state = p3d_node.get_geom_state(geom_idx)

                    # Get the material
                    material_attrib = geom_state.get_attrib(core.MaterialAttrib)
                    if material_attrib is None:
                        # No material?
                        continue

                    p3d_material = material_attrib.get_material()
                    if p3d_material.name in materials:
                        # We already processed that material
                        continue

                    # Get the textures used by this p3d_material
                    texture_attrib = geom_state.get_attrib(core.TextureAttrib)
                    material_textures: List[MaterialTexture] = []
                    for texture_stage in texture_attrib.get_on_stages():
                        texture_name = texture_attrib.get_on_texture(texture_stage).name
                        texture = textures.get(texture_name)
                        if not texture:
                            logger.warning("Could not find texture {} for material {}".format(
                                texture_name, p3d_material.name))
                            continue
                        material_textures.append(MaterialTexture(texture_id=texture.uuid,
                                                                 usage=TEXTURE_SORT_TO_USAGE.get(texture_stage.sort)))

                    # Shading model and normal strength are stored in the emission
                    alpha = 1.0
                    if p3d_material.emission.x == 1:
                        shading_model = Material.ShadingModel.EMISSIVE
                    elif p3d_material.emission.x == 3:
                        shading_model = Material.ShadingModel.TRANSPARENT
                        alpha = p3d_material.emission.z
                    else:
                        shading_model = Material.ShadingModel.DEFAULT

                    # No proxy used here, we want to convert and forget about the source data, not wrap it
                    material = Material(
                        name=p3d_material.name,
                        color=Vector4((*p3d_material.base_color.xyz, alpha)
                                      ) if p3d_material.base_color else Vector4((1.0, 1.0, 1.0, 1.0)),
                        double_sided=p3d_material.twoside,
                        normal_map_strength=p3d_material.emission.y,
                        refraction=p3d_material.refractive_index,
                        roughness=p3d_material.roughness,
                        metallic=p3d_material.metallic,
                        shading_model=shading_model,
                        textures=material_textures
                    )

                    materials[material.name] = material

            # Go through its children
            for child in node_path.children:
                process_node(child)

        process_node(node)

        return materials

    @staticmethod
    def _process_nodes(node: core.NodePath, materials: Dict[str, Material]) -> Tuple[Optional[Object3D], List[Mesh]]:
        meshes: Dict[int, Mesh] = {}

        def process_node(node_path: core.NodePath) -> Optional[Object3D]:
            nonlocal meshes

            object: Optional[Object3D] = None

            p3d_node = node_path.node()
            if p3d_node.is_of_type(core.LensNode):
                # Handle light
                # NOTE: LIGHT ATTENUATION DIFFER FROM LOADED BAM
                if p3d_node.is_of_type(core.Spotlight):
                    # Spot Light
                    type = Light.LightType.SPOTLIGHT
                    spot_fov = p3d_node.exponent / pi * 180.0
                elif p3d_node.is_of_type(core.PointLight):
                    # Point Light
                    # NOTE: THIS ALSO CATCHES SPHERE LIGHTS
                    type = Light.LightType.POINT_LIGHT
                    spot_fov = 0.0
                else:
                    # Well, we need a default type
                    logger.warning("Unsupported light type for node \"{}\"".format(node_path.name))
                    type = Light.LightType.POINT_LIGHT
                    spot_fov = 0.0

                # No proxy used here, we want to convert and forget about the source data, not wrap it
                object = Light(
                    name=node_path.get_name(),
                    matrix=Matrix44(node_path.get_mat()),
                    type=type,
                    radius=p3d_node.max_distance,
                    energy=p3d_node.get_color().w,
                    color=cast(Color, tuple(p3d_node.get_color().xyz)),
                    spot_fov=spot_fov
                )

            elif p3d_node.is_geom_node() and p3d_node.get_num_geoms() > 0:
                mesh = meshes.get(node_path.get_key())
                if mesh:
                    logger.debug("Reusing mesh")

                if not mesh:
                    geometries = []  # type: List[Geometry]
                    for geom_idx in range(p3d_node.get_num_geoms()):
                        geom = p3d_node.get_geom(geom_idx)
                        geom_state = p3d_node.get_geom_state(geom_idx)
                        material = None

                        # Get the material
                        if materials:
                            # TODO: If None maybe we should create them
                            material_attrib = geom_state.get_attrib(core.MaterialAttrib)
                            if material_attrib is not None:
                                p3d_material = material_attrib.get_material()
                                p3d_material_id = p3d_material.name
                                material = materials.get(p3d_material_id) if p3d_material_id else None

                        # Get the geometry
                        # In Panda3D, a Geom can have only one primitive type (as Geometry in EIS)
                        # So we can just get the type for the first geom
                        geom_vertex_data = geom.get_vertex_data()
                        geom_num_arrays = geom_vertex_data.get_num_arrays()
                        geom_num_primitives = geom.get_num_primitives()

                        if geom_num_arrays == 0 or geom_num_primitives == 0:
                            logger.warning("Geom contains no vertices or primitives")
                            continue

                        # Create the object
                        geom_primitive = geom.get_primitive(0)
                        primitive_type = None  # type: Optional[Geometry.PrimitiveType]
                        if geom_primitive.is_exact_type(core.GeomTriangles):
                            primitive_type = Geometry.PrimitiveType.TRIANGLE
                        elif geom_primitive.is_exact_type(core.GeomTristrips):
                            primitive_type = Geometry.PrimitiveType.TRIANGLE_STRIP

                        # No proxy used here, we want to convert and forget about the source data, not wrap it
                        geometry = Geometry(
                            name=p3d_node.name + str(geom_idx),
                            material_id=material.uuid if material else None,
                            primitive_type=primitive_type
                        )
                        geometries.append(geometry)

                        for geometry_index in range(geom_num_arrays):
                            p3d_vertex_reader = core.GeomVertexReader(geom_vertex_data, 'vertex')
                            p3d_normal_reader = core.GeomVertexReader(geom_vertex_data, 'normal')
                            p3d_texcoord_reader = core.GeomVertexReader(geom_vertex_data, 'texcoord')
                            p3d_color_reader = core.GeomVertexReader(geom_vertex_data, 'color')

                            num_rows = geom_vertex_data.get_num_rows()

                            geometry.vertices = cast(List[Vertex], [tuple(p3d_vertex_reader.get_data3f())
                                                                    for i in range(num_rows)]) if p3d_vertex_reader.has_column() else []
                            geometry.normals = cast(List[Normal], [tuple(p3d_normal_reader.get_data3f())
                                                                   for i in range(num_rows)]) if p3d_normal_reader.has_column() else []
                            geometry.texcoords = cast(List[TexCoord], [tuple(p3d_texcoord_reader.get_data2f()) for i in range(
                                num_rows)]) if p3d_texcoord_reader.has_column() else []
                            geometry.colors = cast(List[Color], [tuple(p3d_color_reader.get_data4f())
                                                                 for i in range(num_rows)]) if p3d_color_reader.has_column() else []

                        for p3d_primitives in geom.primitives:
                            vertices = p3d_primitives.get_vertex_list()
                            geometry.primitives += [
                                vertices[p3d_primitives.get_primitive_start(p):p3d_primitives.get_primitive_end(p)]
                                for p in range(p3d_primitives.get_num_primitives())
                            ]

                    # No proxy used here, we want to convert and forget about the source data, not wrap it
                    mesh = Mesh(
                        name=p3d_node.name,
                        geometries=geometries
                    )

                    meshes[node_path.get_key()] = mesh

                # Create the object
                # No proxy used here, we want to convert and forget about the source data, not wrap it
                object = Object3D(
                    name=node_path.get_name(),
                    children=[child for child in [process_node(node)
                                                  for node in node_path.children] if child is not None],
                    mesh_id=mesh.uuid,
                    matrix=Matrix44(node_path.get_mat())
                )

            else:
                # Generic Object
                # No proxy used here, we want to convert and forget about the source data, not wrap it
                object = Object3D(
                    name=node_path.get_name(),
                    children=[child for child in [process_node(node)
                                                  for node in node_path.children] if child is not None],
                    matrix=Matrix44(node_path.get_mat())
                )

            return object

        object_3d = process_node(node_path=node)
        return object_3d, list(meshes.values())

        # endregion
