import logging
import os
from typing import Any, Dict, List, Optional, TYPE_CHECKING

from direct.gui.OnscreenText import OnscreenText  # type: ignore
from direct.showbase.ShowBase import ShowBase  # type: ignore
from panda3d import core  # type: ignore
from panda3d.core import CollisionTraverser, LRotation, Mat4, Vec3  # type: ignore

from eis import BASE_PATH
from eis.engine import Engine, defaultEngineConfig
from eis.engines.panda3d.converter import Panda3DConverter
from eis.engines.panda3d.picker import Panda3DPicker
from eis.graph.light import Light
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.picker import Picker
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore

try:
    from rpcore import RenderPipeline  # type: ignore
    from rpcore.native import RPLight  # type: ignore
except:
    pass

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.engines.panda3d.proxies.object_3d import Panda3DObject3DProxy

# Silly global task manager from panda3d
global taskMgr

logger = logging.getLogger(__name__)
defaultEngineConfig['renderer.render_pipeline'] = True
defaultEngineConfig['renderer.fov'] = 90.0


class Panda3DEngine(Engine[core.NodePath, Mat4, LRotation, Vec3]):
    """
    Engine
    """

    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(config=config)
        self._base: Optional[ShowBase] = None
        self._use_render_pipeline = False
        self._render_pipeline: 'RenderPipeline' = None

        self._status_text: Optional[OnscreenText] = None

    def initialize(self) -> None:
        # Check if we use render pipeline
        self._use_render_pipeline = not self._headless and self._config.get('renderer.render_pipeline')
        if self._use_render_pipeline:
            # RenderPipeline must be created and initialized before calling super on ShowBase
            try:
                from rpcore import RenderPipeline
                self._render_pipeline = RenderPipeline()
                self._render_pipeline.pre_showbase_init()
            except:
                self._use_render_pipeline = False

        self._base = ShowBase(windowType='none' if self._headless else 'onscreen')
        self._base.cTrav = CollisionTraverser()

        if self._use_render_pipeline:
            # Once ShowBase is initialized we can continue with render pipeline setup
            self._render_pipeline.set_loading_screen_image(
                os.path.join(BASE_PATH, "res/splash.png"))
            self._render_pipeline.create(self._base)

        # Initialize engine
        if not self._headless:
            self.setup_input()

        # TODO: Ask the rendering engine whether it's running in immersive mode
        if not self._headless:
            self._base.camLens.setFov(self._config.get('renderer.fov'))

        # Add root node
        self._root.update_proxy(self)
        self._root.proxy.node_path.reparent_to(self.base.render)

    def shutdown(self) -> None:
        self._base.shutdown()
        self._base.destroy()
        del self._base

    @property
    def base(self) -> ShowBase:
        return self._base

    @property
    def use_render_pipeline(self) -> bool:
        return self._use_render_pipeline

    @property
    def headless(self) -> bool:
        return self._headless

    def engine_matrix(self, mat: Matrix44) -> Mat4:
        return Mat4(*mat.flat)

    def eis_matrix(self, mat: Mat4) -> Matrix44:
        return Matrix44(mat)

    def engine_quaternion(self, quat: Quaternion) -> LRotation:
        # return Quat(quat[3], quat[0], quat[1], quat[2])  #, quat[3])
        return LRotation(*quat)

    def eis_quaternion(self, quat: LRotation) -> Quaternion:
        return Quaternion(quat)

    def engine_vector(self, vec: Vector3) -> Vec3:
        return Vec3(*vec)

    def eis_vector(self, vec: Vec3) -> Vector3:
        return Vector3(vec)

    def setup_input(self) -> None:
        """
        Setup debug and utility input
        Proper EIS input should be implemented using InputMethods
        :return:
        """

        # Hotkeys to connect to pstats
        self._base.accept("p", core.PStatClient.connect)

        # Wireframe + debug + buffer viewer
        self._base.accept("f3", self._base.toggle_wireframe)
        self._base.accept("f11", lambda: self._base.win.save_screenshot("screenshot.png"))

        def print_position() -> None:
            """ Prints the camera position and hpr """
            pos = self._base.cam.get_pos(self._base.render)
            hpr = self._base.cam.get_hpr(self._base.render)
            print("(Vec3({}, {}, {}), Vec3({}, {}, {})),".format(pos.x, pos.y, pos.z, hpr.x, hpr.y, hpr.z))

        self._base.accept("j", print_position)

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        # Step Panda3D
        taskMgr.step()  # type: ignore

    def read_model_from_file(self, path: str) -> Optional[Model]:
        logger.info("Reading model from file \"{}\" with Panda3D".format(path))

        try:
            node = self.base.loader.load_model(path)
        except IOError:
            logger.error("Could not load file \"{}\"".format(path))
            return None

        if node is None:
            return None

        return Panda3DConverter.engine_to_eis(engine_graph=node, name=path[path.rindex("/") + 1:path.rindex(".")])

    def _add_model(self, model: Model, parent: Optional[Object3D] = None) -> bool:
        assert isinstance(model, Model)
        assert not parent or isinstance(parent, Object3D)

        if not model.root.proxy:
            logger.error("Loaded model \"{}\" has no proxy!".format(model))
            return False

        if parent and not parent.proxy:
            logger.error("Parent \"{}\" has no proxy!".format(parent))
            return False

        logger.debug("Loading model \"{}\" into Panda3D".format(model))

        if not parent:
            parent = self._root

        parent.add_child(model.root)

        if self.use_render_pipeline:
            self.prepare_scene(model.root.proxy)

        logger.debug("Model \"{}\" loaded!".format(model))

        return True

    def prepare(self, proxy: 'Panda3DObject3DProxy') -> None:
        """
        Prepare the node path for inclusion in the graph

        :param proxy: core.NodePath
        :return: None
        """
        if self.use_render_pipeline:
            parent = proxy.node_path.parent
            dummy = core.NodePath("Temporary Dummy Node")
            proxy.node_path.reparent_to(dummy)
            self.prepare_scene(proxy)
            if parent:
                proxy.node_path.reparent_to(parent)
            else:
                proxy.node_path.detach_node()

    def prepare_scene(self, proxy: 'Panda3DObject3DProxy') -> None:
        # The only way to match a Panda3D light to a RenderPipeline one is the order in the result
        # of the node path parser. So we first parse the lights in the Panda3D of the node and then we get
        # the lights returned by the RenderPipeline prepare_scene.
        p3d_root = proxy.node_path.get_ancestors()[-1]

        p3d_lights: List[core.NodePath] = []
        for light in p3d_root.find_all_matches("**/+PointLight"):
            p3d_lights.append(light)
        for light in p3d_root.find_all_matches("**/+Spotlight"):
            p3d_lights.append(light)

        res = self._render_pipeline.prepare_scene(p3d_root)

        rp_lights: List[RPLight] = []
        if res:
            lights = res.get('lights')
            if lights:
                for light in lights:
                    rp_lights.append(light)

        if not len(p3d_lights) or not len(rp_lights):
            return

        # Then we traverse the proxied object to see if it matches one of the lights from the Panda3D graph.
        def check_light(obj: Object3D):
            if isinstance(obj, Light):
                for p3d_light in p3d_lights:
                    if p3d_light == obj.proxy.light_node_path:
                        # We use the index in the Panda3D lights list and use it to match the RenderPipeline light.
                        # We keep a reference to the RenderPipeline light in the panda3d light proxy for later removal.
                        obj.proxy.associate_rp_light(rp_lights[p3d_lights.index(p3d_light)])

        proxy.proxied.traverse(check_light)

    def prepare_default(self, node: core.NodePath) -> None:
        """
        Prepare the node to be rendered with the default effect

        :param node: core.NodePath
        :return: None
        """
        if self.use_render_pipeline:
            self._render_pipeline.set_effect(node, os.path.join(
                BASE_PATH, "res/rp_effects/default.yaml"), {"normal_mapping": False}, 0)

    def prepare_text(self, node: core.NodePath) -> None:
        """
        Prepare the node path containing a TextNode

        :param node: core.NodePath
        :return: None
        """
        if self.use_render_pipeline:
            self._render_pipeline.set_effect(node, os.path.join(BASE_PATH, "res/rp_effects/text.yaml"), {}, 100)

    def prepare_gui(self, node: core.NodePath) -> None:
        """
        Prepare the node path to be drawn as GUI (on top of everything else)

        :param node: core.NodePath
        :return: None
        """
        if self.use_render_pipeline:
            self._render_pipeline.set_effect(node, os.path.join(BASE_PATH, "res/rp_effects/gui.yaml"), {
                "render_gbuffer": False,
                "render_shadow": False,
                "render_voxelize": False,
                "render_envmap": False,
                "render_forward": True,
                "alpha_testing": True,
                "normal_mapping": False,
                "parallax_mapping": False,
            }, 100)

    def convert_model(self, model: Model) -> core.NodePath:
        logger.debug("Converting model {} to Panda3D".format(model))
        model.convert(engine=self)

        return model.root.proxy.node_path if model.root.proxy else None

    def move_camera(self, matrix: Matrix44) -> None:
        self.base.camera.set_mat(self.engine_matrix(matrix))

    def get_picker(self, editor: 'ClientEditor') -> Picker:
        return Panda3DPicker(self, editor)

    def show_status(self, status: str) -> None:
        self._status_text = OnscreenText(text=status, pos=(0, 0), scale=0.15,
                                         shadow=(0, 0, 0, 0.50), fg=(1.0, 1.0, 1.0, 1.0))

    def hide_status(self) -> None:
        if self._status_text:
            self._status_text.destroy()

    def remove_light(self, rp_light: 'RPLight') -> None:
        if self.use_render_pipeline:
            self._render_pipeline.remove_light(rp_light)

    @classmethod
    def trace_node(cls, node: core.NodePath, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "\033[0;32m" + str(node) + "\033[0;0m (key: " + str(node.get_key()) + ")\n"
        ret += indent + "    \033[1;30m>>>\033[0;0m " + str(node.node())  # node already has \n in its string + "\n"
        for n in node.children:
            ret += cls.trace_node(n, level + 1)
        return ret
