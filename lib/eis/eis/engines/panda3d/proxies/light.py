import logging
from typing import Any, List, Optional, TYPE_CHECKING

from math import pi
from panda3d import core

from eis.engine import Engine, proxy
from eis.engines.panda3d.engine import Panda3DEngine
from eis.engines.panda3d.proxies.object_3d import Panda3DObject3DProxy
from eis.graph.light import Light, LightProxy
from satmath.matrix44 import Matrix44

if TYPE_CHECKING:
    from rpcore.native import RPLight  # type: ignore

logger = logging.getLogger(__name__)


@proxy(Panda3DEngine, Light)
class Panda3DLightProxy(LightProxy, Panda3DObject3DProxy[Light, core.Light]):

    def __init__(self, engine: Engine, proxied: Light) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._light_node: Optional[Any] = None
        self._light_node_path: Optional[core.NodePath] = None

    @property
    def light(self) -> Optional[Any]:
        return self._light_node

    @property
    def light_node_path(self) -> Optional[core.NodePath]:
        return self._light_node_path

    @property
    def radius(self) -> float:
        return self._radius

    @radius.setter
    def radius(self, radius: float) -> None:
        self._radius = max(0.0, radius)
        if self._light_node:
            if self._engine.use_render_pipeline:
                self._light_node.radius = radius
            else:
                self._light_node.max_distance = radius

    @property
    def energy(self) -> float:
        return self._energy

    @energy.setter
    def energy(self, energy: float) -> None:
        self._energy = max(0.0, energy)
        if self._light_node:
            if self._engine.use_render_pipeline:
                self._light_node.energy = energy
            else:
                self._light_node.set_color(core.LColor(*self._color, energy))

    @property
    def color(self) -> List[float]:
        return self._color

    @color.setter
    def color(self, color: List[float]) -> None:
        self._color = color
        if self._light_node:
            if self._engine.use_render_pipeline:
                self._light_node.color = color
            else:
                self._light_node.set_color(core.LColor(*color, self._energy))

    def associate_rp_light(self, light: 'RPLight') -> None:
        self._light_node = light

    def make(self) -> None:
        super().make()

        light = self._proxied
        self._radius = light.radius
        self._energy = light.energy
        self._color = light.color[0:3]

        # Create the light node
        if light.type == Light.LightType.POINT_LIGHT:
            self._light_node = core.PointLight(light.name)
        elif light.type == Light.LightType.SPOTLIGHT:
            self._light_node = core.Spotlight(light.name)
            self._light_node.exponent = light.spot_fov * pi / 180.0
        else:
            logger.warning("Unsupported light type \"{}\" encountered".format(light.type))

        if self._light_node is not None:
            self._light_node.set_color(core.LColor(*light.color[0:3], light.energy))
            self._light_node.max_distance = light.radius
            self._light_node_path = core.NodePath(self._light_node)
            self._light_node_path.reparent_to(self._node_path)

        # We need to signal the engine for this new light
        self._engine.prepare(self)

    def removed_from_scene(self) -> None:
        if self._light_node:
            self._engine.remove_light(self._light_node)

    def remove(self) -> None:
        if self._light_node:
            self._engine.remove_light(self._light_node)

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        super().set_matrix(matrix=matrix, matrix_offset=matrix_offset)
        if self._engine.use_render_pipeline and self._light_node:
            # RPLight does not know the slightest about its parent. Poor thingie...
            if self._proxied.parent:
                parent_matrix = self._proxied.parent.matrix_world
            else:
                parent_matrix = Matrix44.identity()

            world_matrix = matrix_offset * matrix * parent_matrix
            trans, rot, scale = world_matrix.decompose()
            self._light_node.pos = core.LVector3f(*trans)
            if self.proxied.type == Light.LightType.SPOTLIGHT:
                lpoint = core.Mat4(*(world_matrix).flat).xform_vec((0, 0, -1))
                self._light_node.direction = lpoint
