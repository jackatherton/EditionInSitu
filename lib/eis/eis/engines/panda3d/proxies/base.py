from typing import Generic, Optional, TypeVar

from eis.engines.panda3d.engine import Panda3DEngine
from panda3d import core

N = TypeVar('N', bound=core.PandaNode)


class Panda3DGraphProxyBase(Generic[N]):

    def __init__(self, engine: Panda3DEngine) -> None:
        assert isinstance(engine, Panda3DEngine)
        self._panda3d = engine
        self._node: Optional[N] = None

    @property
    def panda3d(self) -> Panda3DEngine:
        return self._panda3d

    @property
    def node(self) -> Optional[N]:
        return self._node
