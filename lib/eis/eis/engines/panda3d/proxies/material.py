import logging
from typing import Optional

from panda3d import core

from eis.engine import Engine, proxy
from eis.engines.panda3d.engine import Panda3DEngine
from eis.engines.panda3d.proxies.base import Panda3DGraphProxyBase
from eis.graph.material import Material, MaterialProxy

logger = logging.getLogger(__name__)


@proxy(Panda3DEngine, Material)
class Panda3DMaterialProxy(MaterialProxy, Panda3DGraphProxyBase[core.Material]):
    def __init__(self, engine: Engine, proxied: Material) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._material: Optional[core.Material] = None

    @property
    def material(self) -> core.Material:
        return self._material

    def make(self) -> None:
        super().make()
        self._material = core.Material()
        self._material.setTwoside(self._proxied.double_sided)

    def update(self) -> None:
        super().update()
        self._material.name = self._proxied.name
        self._material.base_color = core.LColor(*self._proxied.color)
        self._material.roughness = self._proxied.roughness
        self._material.refractive_index = self._proxied.refraction
        self._material.metallic = self._proxied.metallic

        emission = core.LColor(0, 0, 0, 0)
        if self._proxied.shading_model == Material.ShadingModel.EMISSIVE:
            emission.x = 1
        elif self._proxied.shading_model == Material.ShadingModel.TRANSPARENT:
            emission.x = 3
        else:  # Default
            emission.x = 0
        emission.y = self._proxied.normal_map_strength
        emission.z = self._proxied.color[3]
        self._material.emission = emission
