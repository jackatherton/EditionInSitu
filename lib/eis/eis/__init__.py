"""
Shared EIS library for code common to both client and server
"""

__author__ = "François Ubald Brien"
__copyright__ = "Copyright 2017, Société des arts technologiques"
__credits__ = ["Nicolas Bouillot", "François Ubald Brien", "Emmanuel Durand", "Michał Seta", "Jérémie Soria"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "François Ubald Brien"
__email__ = "metalab@sat.qc.ca"
__status__ = "Development"

import os
from enum import IntEnum, unique

# Base path, use this when trying to figure out where the projet's root is
BASE_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
EIS_CONFIG_FILENAME = "eis.json"
LOCAL_CONFIG_FILENAME = "local.json"


@unique
class EISObjectId(IntEnum):
    """
    Serializable value object ids
    """
    USER_VO = 0x01
    PEER_VO = 0x02
    PEER_USER_VO = 0x03
    MODEL = 0x04
    MATERIAL_TEXTURE = 0x05


@unique
class EISEntityId(IntEnum):
    """
    Serializable entity ids
    """
    SCENE = 0x01
    OBJECT_3D = 0x02
    GEOM_OBJECT_3D = 0x03
    MESH = 0x04
    GEOMETRY = 0x05
    MATERIAL = 0x06
    TEXTURE = 0x07
    TEXTURE_VIDEO = 0x08
    LIGHT = 0x09
    CAMERA = 0x0a
    SOUND_OBJECT = 0x0b
    SATIE_OBJECT = 0x0c
    SATIE_SWITCHER_OBJECT = 0x0d
    SATIE_SWITCHER_VIDEO_OBJECT = 0x0e
    SATIE_INFINITE_OBJECT = 0x0f
    CAMERA_PRESENTATION = 0x10

    # Graph Primitives
    PRIMITIVE_BOX = 0x20
    PRIMITIVE_CIRCLE = 0x21
    PRIMITIVE_CONE = 0x22
    PRIMITIVE_CYLINDER = 0x23
    PRIMITIVE_PLANE = 0x24
    PRIMITIVE_RING = 0x25
    PRIMITIVE_SPHERE = 0x26
    PRIMITIVE_TORUS = 0x27
    PRIMITIVE_TEXT = 0x28

    # Shape Primitives
    SHAPE = 0x30
    SHAPE_ARROW = 0x31
    SHAPE_CIRCLE = 0x32
    SHAPE_CROSSHAIR = 0x33
    SHAPE_RECTANGLE = 0x34
    SHAPE_RING = 0x35

    # Behaviors
    BEHAVIOR_TEST = 0x40
    BEHAVIOR_ANIMATION = 0x41
    BEHAVIOR_AUTO_KEYFRAME = 0x42
    BEHAVIOR_HIGHLIGHT = 0x43
    BEHAVIOR_RANDOM_MOTION = 0x44
    BEHAVIOR_TRACKER = 0x45
    BEHAVIOR_PHYSICS = 0x46
    BEHAVIOR_BODY_PHYSICS = 0x47
    BEHAVIOR_VIDEO = 0x48
    BEHAVIOR_HIDDEN = 0x49
    BEHAVIOR_VIDEO_LOD = 0x4a

    # Animation
    ANIMATION_CURVE = 0x60

    # Event handlers
    EVENT_HANDLER_COLLISION = 0x70

    # CORE
    USER = 0x80
    PEER_SESSION = 0x81
    PEER_USER = 0x82

    # Inputs
    VIVE_INPUT = 0x90
    VIVE_PEER_INPUT = 0x91
