from enum import IntEnum, unique
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satnet.entity import entity


@unique
class PlaneAxis(IntEnum):
    XY = 0
    XZ = 1
    YZ = 2


@entity(id=EISEntityId.PRIMITIVE_PLANE)
class Plane(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/PlaneGeometry.js
    """

    width = Sync[float]('_width', 1.0, on_changed=Primitive.sync_invalidate)
    height = Sync[float]('_height', 1.0, on_changed=Primitive.sync_invalidate)
    width_segments = Sync[int]('_width_segments', 1, on_changed=Primitive.sync_invalidate)
    height_segments = Sync[int]('_height_segments', 1, on_changed=Primitive.sync_invalidate)
    axis = Sync[PlaneAxis]('_axis', PlaneAxis.XY, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            width: float = 1.0,
            height: float = 1.0,
            width_segments: int = 1,
            height_segments: int = 1,
            axis: PlaneAxis = PlaneAxis.XY,
            **kwargs: Any
    ) -> None:
        parameters = {
            'width': width,
            'height': height,
            'width_segments': width_segments,
            'height_segments': height_segments,
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_PLANE, parameters=parameters)

        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._width = width
        self._height = height
        self._width_segments = width_segments
        self._height_segments = height_segments
        self._axis = axis

    def build(self) -> None:
        super().build()

        half_width = self._width / 2
        half_height = self._height / 2

        grid_x = self._width_segments
        grid_y = self._height_segments

        grid_x1 = grid_x + 1
        grid_y1 = grid_y + 1

        segment_width = self._width / grid_x
        segment_height = self._height / grid_y

        # generate vertices, normals and uvs
        for iy in range(0, grid_y1):
            y = iy * segment_height - half_height
            for ix in range(0, grid_x1):
                x = ix * segment_width - half_width
                if self._axis == PlaneAxis.XY:
                    self._geometry.vertices.append((x, -y, 0.0))
                    self._geometry.normals.append((0.0, 0.0, 1.0))
                elif self._axis == PlaneAxis.XZ:
                    self._geometry.vertices.append((x, 0.0, -y))
                    self._geometry.normals.append((0.0, 1.0, 0.0))
                elif self._axis == PlaneAxis.YZ:
                    self._geometry.vertices.append((0.0, x, -y))
                    self._geometry.normals.append((1.0, 0.0, 0.0))

                self._geometry.texcoords.append((1.0 - ix / grid_x, 1.0 - (iy / grid_y)))

        # indices
        for iy in range(0, grid_y):
            for ix in range(0, grid_x):
                a = ix + grid_x1 * iy
                b = ix + grid_x1 * (iy + 1)
                c = (ix + 1) + grid_x1 * (iy + 1)
                d = (ix + 1) + grid_x1 * iy

                # faces
                self._geometry.primitives.extend([[a, b, d], [b, c, d]])

    def _copy(self, graph_type: Type['Plane'], *args: Any, **kwargs: Any) -> 'Plane':
        return super()._copy(
            graph_type,
            *args,
            width=self._width,
            height=self._height,
            width_segments=self._width_segments,
            height_segments=self._height_segments,
            axis=self._axis,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Plane'], *args: Any, **kwargs: Any) -> 'Plane':
        return super()._copy_shared(
            graph_type,
            *args,
            width=self._width,
            height=self._height,
            width_segments=self._width_segments,
            height_segments=self._height_segments,
            axis=self._axis,
            **kwargs
        )
