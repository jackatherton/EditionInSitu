import math
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_TORUS)
class Torus(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/TorusGeometry.js
    """

    radius = Sync[float]('_radius', 0.5, on_changed=Primitive.sync_invalidate)
    tube = Sync[float]('_width_segments', 0.25, on_changed=Primitive.sync_invalidate)
    radial_segments = Sync[int]('_radial_segments', 8, on_changed=Primitive.sync_invalidate)
    tubular_segments = Sync[int]('_tubular_segments', 16, on_changed=Primitive.sync_invalidate)
    arc = Sync[float]('_arc', math.pi * 2.0, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            radius: float = 0.5,
            tube: float = 0.25,
            radial_segments: int = 8,
            tubular_segments: int = 16,
            arc: float = math.pi * 2.0,
            **kwargs
    ) -> None:
        parameters = {
            'radius': radius,
            'tube': tube,
            'radial_segments': radial_segments,
            'tubular_segments': tubular_segments,
            'arc': arc
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_TORUS, parameters=parameters)

        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._radius = radius
        self._tube = tube
        self._radial_segments = radial_segments
        self._tubular_segments = tubular_segments
        self._arc = arc

    def build(self) -> None:
        super().build()

        center = Vector3()
        vertex = Vector3()
        normal = Vector3()

        # generate vertices, normals and uvs
        for j in range(0, self._radial_segments + 1):
            for i in range(0, self._tubular_segments + 1):
                u = i / self._tubular_segments * self._arc
                v = j / self._radial_segments * math.pi * 2

                # vertex
                vertex.x = (self._radius + self._tube * math.cos(v)) * math.cos(u)
                vertex.y = (self._radius + self._tube * math.cos(v)) * math.sin(u)
                vertex.z = self._tube * math.sin(v)

                self._geometry.vertices.append((vertex.x, vertex.y, vertex.z))

                # normal
                center.x = self._radius * math.cos(u)
                center.y = self._radius * math.sin(u)
                normal = (vertex - center).normalized

                self._geometry.normals.append((normal.x, normal.y, normal.z))

                # uv
                self._geometry.texcoords.append((i / self._tubular_segments, j / self._radial_segments))

        # generate indices
        for j in range(1, self._radial_segments + 1):
            for i in range(1, self._tubular_segments + 1):
                # indices
                a = (self._tubular_segments + 1) * j + i - 1
                b = (self._tubular_segments + 1) * (j - 1) + i - 1
                c = (self._tubular_segments + 1) * (j - 1) + i
                d = (self._tubular_segments + 1) * j + i

                # faces
                self._geometry.primitives.extend([[a, b, d], [b, c, d]])

    def _copy(self, graph_type: Type['Torus'], *args: Any, **kwargs: Any) -> 'Torus':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius,
            tube=self._tube,
            radial_segments=self._radial_segments,
            tubular_segments=self._tubular_segments,
            arc=self._arc,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Torus'], *args: Any, **kwargs: Any) -> 'Torus':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            tube=self._tube,
            radial_segments=self._radial_segments,
            tubular_segments=self._tubular_segments,
            arc=self._arc,
            **kwargs
        )
    ...
