import math
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_CYLINDER)
class Cylinder(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/CylinderGeometry.js
    """

    radius_top = Sync[float]('_radius_top', 0.5, on_changed=Primitive.sync_invalidate)
    radius_bottom = Sync[float]('_radius_bottom', 0.5, on_changed=Primitive.sync_invalidate)
    height = Sync[float]('_height', 1.0, on_changed=Primitive.sync_invalidate)
    radial_segments = Sync[int]('_radial_segments', 32, on_changed=Primitive.sync_invalidate)
    height_segments = Sync[int]('_height_segments', 1, on_changed=Primitive.sync_invalidate)
    open_ended = Sync[bool]('_open_ended', False, on_changed=Primitive.sync_invalidate)
    theta_start = Sync[float]('_theta_start', 0.0, on_changed=Primitive.sync_invalidate)
    theta_length = Sync[float]('_theta_length', 2.0 * math.pi, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            radius_top: float = 0.5,
            radius_bottom: float = 0.5,
            height: float = 1.0,
            radial_segments: int = 32,
            height_segments: int = 1,
            open_ended: bool = False,
            theta_start: float = 0.0,
            theta_length: float = 2.0 * math.pi,
            **kwargs
    ) -> None:
        parameters = {
            'radius_top': radius_top,
            'radius_bottom': radius_bottom,
            'height': height,
            'radial_segments': radial_segments,
            'height_segments': height_segments,
            'open_ended': open_ended,
            'theta_start': theta_start,
            'theta_length': theta_length
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_CYLINDER, parameters=parameters)

        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._radius_top = radius_top
        self._radius_bottom = radius_bottom
        self._height = height
        self._radial_segments = radial_segments
        self._height_segments = height_segments
        self._open_ended = open_ended
        self._theta_start = theta_start
        self._theta_length = theta_length

    def build(self) -> None:
        super().build()

        half_height = self._height / 2.0
        index = 0
        index_array = []

        def generate_torso() -> None:
            nonlocal index

            normal = Vector3()
            vertex = [0.00, 0.00, 0.00]

            # this will be used to calculate the normal
            slope = (self._radius_bottom - self._radius_top) / self._height

            # generate vertices, normals and uvs
            for y in range(0, self._height_segments + 1):
                index_row = []
                v = y / self._height_segments

                # calculate the radius of the current row
                radius = v * (self._radius_bottom - self._radius_top) + self._radius_top

                for x in range(0, self._radial_segments + 1):
                    u = x / self._radial_segments
                    theta = u * self._theta_length + self._theta_start
                    sin_theta = math.sin(theta)
                    cos_theta = math.cos(theta)

                    # vertex
                    self._geometry.vertices.append((
                        radius * sin_theta,
                        - v * self._height + half_height,
                        radius * cos_theta
                    ))

                    # normal
                    normal.x = sin_theta
                    normal.y = slope
                    normal.z = cos_theta
                    normal.normalize()
                    self._geometry.normals.append((normal.x, normal.y, normal.z))

                    # uv
                    self._geometry.texcoords.append((u, 1.0 - v))

                    # save index of vertex in respective row
                    index_row.append(index)
                    index += 1

                # now save vertices of the row in our index array
                index_array.append(index_row)

            # generate indices
            for x in range(0, self._radial_segments):
                for y in range(0, self._height_segments):
                    # we use the index array to access the correct indices
                    a = index_array[y][x]
                    b = index_array[y + 1][x]
                    c = index_array[y + 1][x + 1]
                    d = index_array[y][x + 1]

                    # faces
                    self._geometry.primitives.extend([[a, b, d], [b, c, d]])

        def generate_cap(top: bool) -> None:
            nonlocal index

            uv = [0.00, 0.00]
            vertex = [0.00, 0.00, 0.00]

            radius = self._radius_top if top else self._radius_bottom
            sign = 1.0 if top else -1.0

            # save the index of the first center vertex
            center_index_start = index

            # first we generate the center vertex data of the cap.
            # because the geometry needs one set of uvs per face,
            # we must generate a center vertex per face/segment

            for x in range(1, self._radial_segments + 1):
                # vertex
                self._geometry.vertices.append((0.0, half_height * sign, 0.0))

                # normal
                self._geometry.normals.append((0.0, sign, 0.0))

                # uv
                self._geometry.texcoords.append((0.5, 0.5))

                # increase index
                index += 1

            # save the index of the last center vertex
            center_index_end = index

            # now we generate the surrounding vertices, normals and uvs
            for x in range(0, self._radial_segments + 1):
                u = x / self._radial_segments
                theta = u * self._theta_length + self._theta_start
                cos_theta = math.cos(theta)
                sin_theta = math.sin(theta)

                # vertex
                self._geometry.vertices.append((
                    radius * sin_theta,
                    half_height * sign,
                    radius * cos_theta
                ))

                # normal
                self._geometry.normals.append((0.0, sign, 0.0))

                # uv
                self._geometry.texcoords.append((
                    (cos_theta * 0.5) + 0.5,
                    (sin_theta * 0.5 * sign) + 0.5
                ))

                # increase index
                index += 1

            # generate indices
            for x in range(0, self._radial_segments):
                c = center_index_start + x
                i = center_index_end + x

                if top:
                    # face top
                    self._geometry.primitives.append([i, i + 1, c])
                else:
                    # face bottom
                    self._geometry.primitives.append([i + 1, i, c])

        generate_torso()
        if not self._open_ended:
            if self._radius_top > 0.0:
                generate_cap(True)
            if self._radius_bottom > 0.0:
                generate_cap(False)

    # region Lifecycle

    # endregion

    # region Copy

    def _copy(self, graph_type: Type['Cylinder'], *args: Any, **kwargs: Any) -> 'Cylinder':
        return super()._copy(
            graph_type,
            *args,
            radius_top=self._radius_top,
            radius_bottom=self._radius_bottom,
            height=self._height,
            radial_segments=self._radial_segments,
            height_segments=self._height_segments,
            open_ended=self._open_ended,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Cylinder'], *args: Any, **kwargs: Any) -> 'Cylinder':
        return super()._copy_shared(
            graph_type,
            *args,
            radius_top=self._radius_top,
            radius_bottom=self._radius_bottom,
            height=self._height,
            radial_segments=self._radial_segments,
            height_segments=self._height_segments,
            open_ended=self._open_ended,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    # endregion
    ...
