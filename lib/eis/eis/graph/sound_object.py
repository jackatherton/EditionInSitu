from typing import Optional, List, Any, Callable, Dict, Type
from abc import abstractmethod


from eis import EISEntityId
from eis.constants import CLIENT
from eis.graph.object_3d import Object3D
from satnet.entity import entity


@entity(id=EISEntityId.SOUND_OBJECT)
class SoundObject(Object3D):

    _fields = ['_properties', '_step_callbacks']

    def __init__(
            self,
            name: Optional[str] = None,
            properties: Optional[Dict[str, Any]] = None,
            step_callbacks: Optional[List[Callable]] = None,
            *args,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            *args,
            **kwargs)

        self._step_callbacks: List[Callable] = step_callbacks
        self._properties: Dict[str, Any] = properties

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._last_editor_matrix = None
        self._last_matrix_world_with_offset = self.matrix_world_with_offset
        self._attached = False

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)
        position_changed = False
        if self._attached:
            if self._last_editor_matrix != self._scene.editor.matrix:
                position_changed = True
                self._last_editor_matrix = self._scene.editor.matrix

            if self._last_matrix_world_with_offset != self.matrix_world_with_offset:
                self._last_matrix_world_with_offset = self.matrix_world_with_offset
                if not position_changed:
                    position_changed = True

        if position_changed:
            self._update_sound_spatialization()
        if self._step_callbacks is not None:
            [fun() for fun in self._step_callbacks]

    @abstractmethod
    def _update_sound_spatialization(self) -> None:
        raise NotImplementedError()

    def _instantiate_sound(self) -> None:
        """
        Instantiates and enables the playback of the sound
        Should be extended in every child
        The super() should be called at the END of the redifiniton in order to activate the properties on an activated sound

        :return: None
        """
        self._attached = True
        if self._last_editor_matrix != self._scene.editor.matrix:
            self._last_editor_matrix = self._scene.editor.matrix
        if self._properties is not None:
            for name, value in self._properties.items():
                self._activate_property(name, value)

    def _remove_sound(self) -> None:
        """
        Disables the playback of the sound
        Should be extended in every child

        :return: None
        """
        self._attached = False

    def added_to_editor(self) -> None:
        super().added_to_editor()
        if CLIENT:
            self._instantiate_sound()
            self._update_sound_spatialization()

    def removed_from_editor(self) -> None:
        super().removed_from_editor()
        if CLIENT:
            self._remove_sound()

    def set_property(self, name: str, value: Any) -> None:
        """
        Sets a property of the sound
        These properties can include volume, panning, sound effects and others

        :return: None
        """
        if self._properties is not None and name in self._properties and self._properties[name] == value:
            return
        if self._properties is None:
            self._properties = {}
        self._properties[name] = value
        if self._attached:
            self._activate_property(name, value)

    def get_property(self, name: str) -> Any:
        if self._properties is not None:
            return self._properties[name]

    @abstractmethod
    def _activate_property(self, name: str, value: Any) -> None:
        raise NotImplementedError()

    def add_to_step_callbacks(self, func: Callable) -> None:
        if self._step_callbacks is None:
            self._step_callbacks = []
        self._step_callbacks.append(func)

    def remove_from_step_callbacks(self, func: Callable) -> None:
        if self._step_callbacks is not None and func in self._step_callbacks:
            self._step_callbacks.remove(func)

    def _copy(self, graph_type: Type['SoundObject'], *args: Any, **kwargs: Any) -> 'SoundObject':
        return super()._copy(
            graph_type,
            *args,
            step_callbacks=self._step_callbacks,
            properties=self._properties,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SoundObject'], *args: Any, **kwargs: Any) -> 'SoundObject':
        return super()._copy_shared(
            graph_type,
            *args,
            step_callbacks=self._step_callbacks,
            properties=self._properties,
            **kwargs
        )
