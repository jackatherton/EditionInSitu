from abc import ABCMeta, abstractmethod
from typing import Dict, List, Optional, TYPE_CHECKING, Any
from uuid import UUID

from eis import EISObjectId
from eis.entity import SyncEntity, Sync
from eis.graph.object_3d import Object3D
from satnet import SerializablePrefix
from satnet.serialization import Serializable, serializable

if TYPE_CHECKING:
    from eis.engine import Engine
    from eis.graph.animation_curve import AnimationCurve
    from eis.graph.behavior import Behavior
    from eis.graph.event_handler import EventHandler
    from eis.graph.geometry import Geometry
    from eis.graph.material import Material
    from eis.graph.mesh import Mesh
    from eis.graph.texture import Texture


class ModelDelegate(metaclass=ABCMeta):

    @abstractmethod
    def on_object3d_added(self, model: 'Model', object3d: Object3D) -> None:
        pass

    @abstractmethod
    def on_object3d_removed(self, model: 'Model', object3d: Object3D) -> None:
        pass

    @abstractmethod
    def on_animation_curve_added(self, model: 'Model', animation_curve: 'AnimationCurve') -> None:
        pass

    @abstractmethod
    def on_animation_curve_removed(self, model: 'Model', animation_curve: 'AnimationCurve') -> None:
        pass

    @abstractmethod
    def on_behavior_added(self, model: 'Model', behavior: 'Behavior') -> None:
        pass

    @abstractmethod
    def on_behavior_removed(self, model: 'Model', behavior: 'Behavior') -> None:
        pass

    @abstractmethod
    def on_event_handler_added(self, model: 'Model', behavior: 'Behavior', key: str, event_handler: 'EventHandler') -> None:
        pass

    @abstractmethod
    def on_event_handler_removed(self, model: 'Model', behavior: 'Behavior', key: str) -> None:
        pass

    @abstractmethod
    def on_geometry_added(self, model: 'Model', geometry: 'Geometry') -> None:
        pass

    @abstractmethod
    def on_geometry_removed(self, model: 'Model', geometry: 'Geometry') -> None:
        pass

    @abstractmethod
    def on_material_added(self, model: 'Model', material: 'Material') -> None:
        pass

    @abstractmethod
    def on_material_removed(self, model: 'Model', material: 'Material') -> None:
        pass

    @abstractmethod
    def on_texture_added(self, model: 'Model', texture: 'Texture') -> None:
        pass

    @abstractmethod
    def on_texture_removed(self, model: 'Model', texture: 'Texture') -> None:
        pass

    @abstractmethod
    def on_mesh_added(self, model: 'Model', mesh: 'Mesh') -> None:
        pass

    @abstractmethod
    def on_mesh_removed(self, model: 'Model', mesh: 'Mesh') -> None:
        pass

    @abstractmethod
    def on_sync_changed(self, entity: SyncEntity, sync: Sync, value: Any) -> None:
        """
        Called when a sync's value has changed.

        :param entity: SyncEntity
        :param sync: Sync
        :param value: Any
        :return: None
        """

    @abstractmethod
    def set_object_behaviors(self, model: 'Model', object: 'Object3D') -> None:
        pass

    @abstractmethod
    def unset_object_behaviors(self, model: 'Model', object: 'Object3D') -> None:
        pass


@serializable(prefix=SerializablePrefix.CUSTOM, id=EISObjectId.MODEL)
class Model(Serializable):
    """
    A Model is a container for a branch of the graph.
    It holds the 3d objects and also all the required meshes, materials and textures.
    It is used in order to package and transport parts of the graph over the network
    or on the filesystem.
    """

    _fields = ['_root', '_animation_curves', '_meshes', '_materials', '_textures']

    def __init__(
            self,
            root: Optional['Object3D'] = None,
            meshes: Optional[List['Mesh']] = None,
            materials: Optional[List['Material']] = None,
            textures: Optional[List['Texture']] = None,
            delegate: Optional[ModelDelegate] = None
    ) -> None:
        super().__init__()

        self._all_syncable_entities: Dict[UUID, SyncEntity] = {}
        self._object_uuid_map: Dict[UUID, Object3D] = {}

        self._root: Object3D = root or Object3D("Model Root")
        self._animation_curves: List[AnimationCurve] = []
        self._animation_curve_map: Dict[UUID, AnimationCurve] = {}
        self._behavior_map: Dict[UUID, Behavior] = {}
        self._meshes: List[Mesh] = meshes or []
        self._mesh_map: Dict[UUID, Mesh] = {}
        self._materials: List[Material] = materials or []
        self._material_map: Dict[UUID, Material] = {}
        self._textures: List[Texture] = textures or []
        self._texture_map: Dict[UUID, Texture] = {}

        self.delegate = delegate

    def __str__(self) -> str:
        return "{}\033[0;0m({}, \033[1;30m{}\033[0;0m)".format(self.__class__.__name__, self.root.name, self.root.uuid)

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)
        ret += self.root.trace(level=level + 1)
        for material in self.materials:
            ret += material.trace(level=level + 1)
        for texture in self.textures:
            ret += indent + "    " + str(texture) + "\n"
        for animation_curve in self.animation_curves:
            ret += indent + "    " + str(animation_curve) + "\n"
        for mesh in self.meshes:
            ret += mesh.trace(level=level + 1)
        return ret

    def initialize(self) -> None:
        super().initialize()

        self._animation_curve_map = {curve.uuid: curve for curve in self._animation_curves}
        self._mesh_map = {mesh.uuid: mesh for mesh in self._meshes}
        self._material_map = {material.uuid: material for material in self._materials}
        self._texture_map = {texture.uuid: texture for texture in self._textures}

        self._all_syncable_entities = {
            **self._animation_curve_map,
            **self._mesh_map,
            **self._material_map,
            **self._texture_map
        }

        # Model Ready! Set silently, we are not syncing this yet.
        self._root.set_model(self, True)

    @property
    def root(self) -> 'Object3D':
        return self._root

    @property
    def animation_curves(self) -> List['AnimationCurve']:
        return self._animation_curves

    def get_animation_curve_by_uuid(self, uuid: UUID) -> Optional['AnimationCurve']:
        return self._animation_curve_map.get(uuid)

    @property
    def name(self) -> str:
        return self._root.name

    def get_entity_by_uuid(self, uuid: UUID) -> Optional['SyncEntity']:
        return self._all_syncable_entities.get(uuid)

    def get_object_by_uuid(self, uuid: UUID) -> Optional['Object3D']:
        return self._object_uuid_map.get(uuid)

    def get_behavior_by_uuid(self, uuid: UUID) -> Optional['Behavior']:
        return self._behavior_map.get(uuid)

    @property
    def meshes(self) -> List['Mesh']:
        return self._meshes

    def get_mesh_by_uuid(self, uuid: UUID) -> Optional['Mesh']:
        return self._mesh_map.get(uuid)

    @property
    def materials(self) -> List['Material']:
        return self._materials

    def get_material_by_uuid(self, uuid: UUID) -> Optional['Material']:
        return self._material_map.get(uuid)

    @property
    def textures(self) -> List['Texture']:
        return self._textures

    def get_texture_by_uuid(self, uuid: UUID) -> Optional['Texture']:
        return self._texture_map.get(uuid)

    def add_object(self, object: 'Object3D', silent: bool = False) -> None:
        assert object
        self._object_uuid_map[object.uuid] = object
        self._all_syncable_entities[object.uuid] = object
        if not object.managed:
            object.on_sync_changed = self.on_sync_changed
        if self.delegate and not silent:
            self.delegate.on_object3d_added(self, object)

    def remove_object(self, object: 'Object3D', silent: bool = False) -> None:
        assert object
        del self._object_uuid_map[object.uuid]
        del self._all_syncable_entities[object.uuid]
        object.on_sync_changed = None
        if self.delegate and not silent:
            self.delegate.on_object3d_removed(self, object)

    def add_animation_curve(self, animation_curve: 'AnimationCurve', silent: bool = False) -> None:
        assert animation_curve
        if animation_curve in self._animation_curves:
            return
        animation_curve.add_owner(self)
        self._animation_curves.append(animation_curve)
        self._animation_curve_map[animation_curve.uuid] = animation_curve
        self._all_syncable_entities[animation_curve.uuid] = animation_curve
        if not animation_curve.managed:
            animation_curve.on_sync_changed = self.on_sync_changed
        if self.delegate and not silent:
            self.delegate.on_animation_curve_added(self, animation_curve)

    def remove_animation_curve(self, animation_curve: 'AnimationCurve', silent: bool = False) -> None:
        assert animation_curve
        remove = animation_curve.remove_owner(self)
        if remove:
            self._animation_curves.remove(animation_curve)
            del self._animation_curve_map[animation_curve.uuid]
            del self._all_syncable_entities[animation_curve.uuid]
            animation_curve.on_sync_changed = None
            if self.delegate and not silent:
                self.delegate.on_animation_curve_removed(self, animation_curve)

    def add_behavior(self, behavior: 'Behavior', silent: bool = False) -> None:
        assert behavior
        behavior.add_owner(self)

        if behavior.uuid not in self._behavior_map:
            self._behavior_map[behavior.uuid] = behavior
            self._all_syncable_entities[behavior.uuid] = behavior
            if not behavior.managed:
                behavior.on_sync_changed = self.on_sync_changed
            if self.delegate and not silent:
                self.delegate.on_behavior_added(self, behavior)

    def remove_behavior(self, behavior: 'Behavior', silent: bool = False) -> None:
        assert behavior

        if behavior.remove_owner(self):
            del self._behavior_map[behavior.uuid]
            del self._all_syncable_entities[behavior.uuid]
            behavior.on_sync_changed = None
            if self.delegate and not silent:
                self.delegate.on_behavior_removed(self, behavior)

    def add_event_handler(self, behavior: 'Behavior', key: str, event_handler: 'EventHandler', silent: bool = False) -> None:
        assert key
        assert behavior
        assert event_handler
        behavior = self._behavior_map[behavior.uuid]
        behavior.event_handlers[key] = event_handler
        event_handler.parent = behavior
        if self.delegate and not silent:
            self.delegate.on_event_handler_added(self, behavior=behavior, key=key, event_handler=event_handler)

    def remove_event_handler(self, behavior: 'Behavior', key: str, silent: bool = False) -> None:
        assert behavior
        behavior = self._behavior_map[behavior.uuid]
        behavior.event_handlers.pop(key)
        if self.delegate and not silent:
            self.delegate.on_event_handler_removed(self, behavior=behavior, key=key)

    def add_geometry(self, geometry: 'Geometry', silent: bool = False) -> None:
        assert geometry
        self._all_syncable_entities[geometry.uuid] = geometry
        if not geometry.managed:
            geometry.on_sync_changed = self.on_sync_changed
        if self.delegate and not silent:
            self.delegate.on_geometry_added(self, geometry)

    def remove_geometry(self, geometry: 'Geometry', silent: bool = False) -> None:
        assert geometry
        del self._all_syncable_entities[geometry.uuid]
        geometry.on_sync_changed = None
        if self.delegate and not silent:
            self.delegate.on_geometry_removed(self, geometry)

    def add_mesh(self, mesh: 'Mesh', silent: bool = False, used: bool = True) -> None:
        assert mesh

        # We need to count references, even if not adding the mesh
        if used:
            mesh.add_owner(self)
        # Assume that a mesh with the same uuid is the same, ignore it
        if mesh.uuid not in self._mesh_map:
            self._meshes.append(mesh)
            self._mesh_map[mesh.uuid] = mesh
            self._all_syncable_entities[mesh.uuid] = mesh
            if not mesh.managed:
                mesh.on_sync_changed = self.on_sync_changed
            if self.delegate and not silent:
                self.delegate.on_mesh_added(self, mesh)

    def remove_mesh(self, mesh: 'Mesh', silent: bool = False) -> None:
        assert mesh
        remove = mesh.remove_owner(self)
        if remove:
            self._meshes.remove(mesh)
            del self._mesh_map[mesh.uuid]
            del self._all_syncable_entities[mesh.uuid]
            mesh.on_sync_changed = None
            if self.delegate and not silent:
                self.delegate.on_mesh_removed(self, mesh)

    def add_material(self, material: 'Material', silent: bool = False, used: bool = True) -> None:
        assert material

        # We need to count references even if not adding the material
        if used:
            material.add_owner(self)

        # Assume that a material with the same uuid is the same, ignore it
        if material.uuid not in self._material_map:
            self._materials.append(material)
            self._material_map[material.uuid] = material
            self._all_syncable_entities[material.uuid] = material
            if not material.managed:
                material.on_sync_changed = self.on_sync_changed
            if self.delegate and not silent:
                self.delegate.on_material_added(self, material)

    def remove_material(self, material: 'Material', silent: bool = False) -> None:
        assert material
        remove = material.remove_owner(self)
        if remove:
            self._materials.remove(material)
            del self._material_map[material.uuid]
            del self._all_syncable_entities[material.uuid]
            material.on_sync_changed = None
            if self.delegate and not silent:
                self.delegate.on_material_removed(self, material)

    def add_texture(self, texture: 'Texture', silent: bool = False) -> None:
        assert texture

        # We need to count references even if not adding the texture
        texture.add_owner(self)

        # Assume that a texture with the same uuid is the same, ignore it
        if texture.uuid not in self._texture_map:
            self._textures.append(texture)
            self._texture_map[texture.uuid] = texture
            self._all_syncable_entities[texture.uuid] = texture
            if not texture.managed:
                texture.on_sync_changed = self.on_sync_changed
            if self.delegate and not silent:
                self.delegate.on_texture_added(self, texture)

    def remove_texture(self, texture: 'Texture', silent: bool = False) -> None:
        assert texture
        remove = texture.remove_owner(self)
        if remove:
            self._textures.remove(texture)
            del self._texture_map[texture.uuid]
            del self._all_syncable_entities[texture.uuid]
            texture.on_sync_changed = None
            if self.delegate and not silent:
                self.delegate.on_texture_removed(self, texture)

    def convert(self, engine: 'Engine') -> None:
        self._root.update_proxy(engine)

    def on_scene_loaded(self) -> None:
        self._root.on_scene_loaded()

    def on_scene_unloaded(self) -> None:
        self._root.on_scene_loaded()

    def on_sync_changed(self, entity: SyncEntity, sync: Sync, value: Any) -> None:
        if self.delegate:
            self.delegate.on_sync_changed(entity=entity, sync=sync, value=value)

    def copy(self) -> 'Model':
        # Copying only the root, the rest will get re-registered in the new model,
        # otherwise we end up with duplicated of every shared entity, one in the tree
        # one in the model.
        return Model(root=self._root.copy())

    def copy_shared(self) -> 'Model':
        # Copying only the root, the rest will get re-registered in the new model,
        # otherwise we end up with duplicated of every shared entity, one in the tree
        # one in the model.
        return Model(root=self._root.copy_shared())
