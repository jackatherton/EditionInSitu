import copy
import logging
import numpy
from enum import IntEnum, unique
from typing import Any, List, Optional, TYPE_CHECKING, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph import Color, Normal, TexCoord, Vertex
from eis.graph.base import GraphBase, GraphProxyBase
from eis.graph.material import Material
from satmath.vector3 import Vector3
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.graph.mesh import Mesh
    from eis.graph.model import Model
    from eis.graph.object_3d import Object3D

logger = logging.getLogger(__name__)


class GeometryProxy(GraphProxyBase['Geometry']):
    pass


@entity(id=EISEntityId.GEOMETRY)
class Geometry(GraphBase[GeometryProxy]):
    """
    Geometry
    Holds the vertices, texture coordinates, normals, and vertex colors
    Also has the names (ids) of the material and textures
    """

    _fields = ['_vertices', '_normals', '_texcoords', '_colors', '_primitives', '_primitive_type']
    _default_material = Material(name="default_material", color=(0.8, 0.8, 0.8, 1.0))

    def material_id_changed(self, sync: Sync, previous_material_id: Optional[UUID], value: Optional[UUID]) -> None:
        material: Optional[Material] = None
        if self._mesh is not None:
            for model in self._mesh.owners.keys():
                if material is not None:
                    assert(material is model.get_material_by_uuid(self._material_id))
                material = model.get_material_by_uuid(self._material_id)

        # Retrieve the referenced instance, setter will take care of registration
        if material is not None:
            self.material = material

    material_id = Sync[UUID]('_material_id', _default_material.uuid, on_changed=material_id_changed)

    @property
    def material(self) -> Optional[Material]:
        return self._material or Geometry._default_material

    @material.setter
    def material(self, material: Material) -> None:
        if material != self._material:
            if self._mesh is not None:
                if self._material is not None:
                    for model in self._mesh.owners.keys():
                        self._material.removed_from_model(model)
                if material is not None:
                    for model in self._mesh.owners.keys():
                        material.added_to_model(model)
            previous_material = self._material
            self._material = material
            self.material_id = material.uuid if material is not None else None
            self.invalidate_graph()

            # added/removed_from_geometry callbacks for the material
            if previous_material is not None:
                previous_material.removed_from_geometry(self)
            if self._material is not None:
                self._material.added_to_geometry(self)

            # added/removed_from_object3d callbacks for the material
            if self._mesh is not None:
                for object3d in self._mesh.objects3d:
                    if previous_material is not None:
                        previous_material.removed_from_object3d(object3d)
                    if self._material is not None:
                        self._material.added_to_object3d(object3d)

    @unique
    class PrimitiveType(IntEnum):
        POINT = 0
        TRIANGLE = 1
        TRIANGLE_STRIP = 2

    def __init__(
            self,
            name: Optional[str] = None,
            vertices: Optional[List[Vertex]] = None,
            normals: Optional[List[Normal]] = None,
            texcoords: Optional[List[TexCoord]] = None,
            colors: Optional[List[Color]] = None,
            primitives: Optional[List[List[int]]] = None,
            primitive_type: Optional[PrimitiveType] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None) -> None:
        super().__init__(name=name)

        self._vertices = vertices or []
        self._normals = normals or []
        self._texcoords = texcoords or []
        self._colors = colors or []
        self._primitives = primitives or []
        self._primitive_type = primitive_type or Geometry.PrimitiveType.TRIANGLE
        self._material = material
        self._material_id = material.uuid if material is not None else material_id

        self._mesh: Optional['Mesh'] = None
        self._geometry_updated = True

    def initialize(self):
        super().initialize()
        self._mesh: Optional['Mesh'] = None
        self._geometry_updated = True
        if self._material is not None:
            self._material.added_to_geometry(self)

    def __str__(self) -> str:
        return "\033[0;36m" + super().__str__()

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)
        if self.material:
            ret += self.material.trace(level=level + 1)
        else:
            ret += indent + "    Material(" + str(self.material_id) + ")\n"
        return ret

    @property
    def bound_box(self) -> List[Vector3]:
        if not self._vertices:
            return [Vector3(), Vector3()]

        bbox = [self._vertices[0], self._vertices[0]]
        for vertex in self._vertices:
            bbox[0] = numpy.minimum(bbox[0], vertex)
            bbox[1] = numpy.maximum(bbox[1], vertex)

        return bbox

    @property
    def geometry_updated(self) -> bool:
        return self._geometry_updated

    @property
    def mesh(self) -> Optional['Mesh']:
        return self._mesh

    @mesh.setter
    def mesh(self, value: Optional['Mesh']) -> None:
        if self._mesh != value:
            self._mesh = value
            self.invalidate()

    @property
    def vertices(self) -> List[Vertex]:
        return self._vertices

    @vertices.setter
    def vertices(self, value: List[Vertex]) -> None:
        if self._vertices != value:
            self._vertices = value
            self._geometry_updated = True
            self.invalidate_graph()

    @property
    def normals(self) -> List[Normal]:
        return self._normals

    @normals.setter
    def normals(self, value: List[Normal]) -> None:
        if self._normals != value:
            self._normals = value
            self._geometry_updated = True
            self.invalidate_graph()

    @property
    def texcoords(self) -> List[TexCoord]:
        return self._texcoords

    @texcoords.setter
    def texcoords(self, value: List[TexCoord]) -> None:
        if self._texcoords != value:
            self._texcoords = value
            self._geometry_updated = True
            self.invalidate_graph()

    @property
    def colors(self) -> List[Color]:
        return self._colors

    @colors.setter
    def colors(self, value: List[Color]) -> None:
        if self._colors != value:
            self._colors = value
            self._geometry_updated = True
            self.invalidate_graph()

    @property
    def vertex_count(self) -> int:
        return len(self._vertices)

    @property
    def primitives(self) -> List[List[int]]:
        return self._primitives

    @primitives.setter
    def primitives(self, value: List[List[int]]) -> None:
        if self._primitives != value:
            self._primitives = value
            self._geometry_updated = True
            self.invalidate()

    def add_primitive(self, vertex_ids: List[int]) -> None:
        self._primitives.append(vertex_ids)

    @property
    def primitive_type(self) -> 'PrimitiveType':
        return self._primitive_type

    def added_to_model(self, model: 'Model') -> None:
        # Register the geometry in the model
        model.add_geometry(self)

        # Retrieve if we only hold the reference id
        if not self._material and self._material_id:
            # Retrieve the referenced instance, setter will take care of registration
            self.material = model.get_material_by_uuid(self._material_id)
            if not self._material:
                logger.warning("Could not retrieve material id \"{}\"".format(self._material_id))

        # Add to model
        if self._material is not None:
            self._material.added_to_model(model)

    def removed_from_model(self, model: 'Model') -> None:
        # Unregister ourselves
        model.remove_geometry(self)

        # Remove from model
        if self._material is not None:
            self._material.removed_from_model(model)

    def added_to_object3d(self, object3d: 'Object3D') -> None:
        if self._material is not None:
            self._material.added_to_object3d(object3d)

    def removed_from_object3d(self, object3d: 'Object3D') -> None:
        if self._material is not None:
            self._material.removed_from_object3d(object3d)

    def step(self, now: float, dt: float) -> None:
        super().step(now, dt)
        if self._material:
            self._material.step(now=now, dt=dt)
        self._geometry_updated = False

    def _copy(self, graph_type: Type['Geometry'], *args: Any, **kwargs: Any) -> 'Geometry':
        return super()._copy(
            graph_type,
            *args,
            vertices=self._vertices.copy(),
            normals=self._normals.copy(),
            texcoords=self._texcoords.copy(),
            colors=self._colors.copy(),
            primitives=copy.deepcopy(self._primitives),
            primitive_type=self._primitive_type,
            material=self._material.copy() if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Geometry'], *args: Any, **kwargs: Any) -> 'Geometry':
        return super()._copy_shared(
            graph_type,
            *args,
            vertices=self._vertices.copy(),
            normals=self._normals.copy(),
            texcoords=self._texcoords.copy(),
            colors=self._colors.copy(),
            primitives=copy.deepcopy(self._primitives),
            primitive_type=self._primitive_type,
            # Materials are shared, so no copy, not even _shared
            material=self._material if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            **kwargs
        )
