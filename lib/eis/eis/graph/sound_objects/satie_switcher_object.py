from typing import Optional, Any, Type

from eis import EISEntityId
from eis.editor_support.satie_manager import SatieManager
from eis.graph.sound_objects.satie_object import SatieObject
from eis.editor_support.switcher_manager import SwitcherManager
from satnet.entity import entity


@entity(id=EISEntityId.SATIE_SWITCHER_OBJECT)
class SatieSwitcherObject(SatieObject):

    _fields = ['_path']

    def __init__(
            self,
            name: Optional[str] = None,
            path: Optional[str] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            **kwargs)

        self._path = path

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._audio_handle = None
        self._plugin = "MonoIn"

    def _instantiate_sound(self) -> None:
        if self._path is None:
            return

        switcher_manager = SwitcherManager()
        self._audio_handle = switcher_manager.new_audio(self._path)

        # register callback to switcher manager
        switcher_manager.link_audio(handle=self._audio_handle)

        super()._instantiate_sound()

        # Set this property here since it owns the handle anyway
        self.set_property(name="bus", value=self._audio_handle - 1)

    def _remove_sound(self) -> None:
        if self._audio_handle is not None:
            switcher_manager = SwitcherManager()
            switcher_manager.unlink_audio(self._audio_handle)
            switcher_manager.close_audio(self._audio_handle)
            self._audio_handle = None
        super()._remove_sound()

    def _copy(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy(
            graph_type,
            *args,
            path=self._path,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy_shared(
            graph_type,
            *args,
            path=self._path,
            **kwargs
        )
