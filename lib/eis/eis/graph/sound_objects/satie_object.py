from typing import Optional, Any, Type

from eis import EISEntityId
from eis.editor_support.satie_manager import SatieManager
from eis.graph.sound_object import SoundObject
from satnet.entity import entity


@entity(id=EISEntityId.SATIE_OBJECT)
class SatieObject(SoundObject):

    _fields = ['_group']

    def __init__(
            self,
            name: Optional[str] = None,
            group: Optional[str] = None,
            *args,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            *args,
            **kwargs)

        self._group = group or "default"
        self._plugin = None

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._audio_generator = None

    def _update_sound_spatialization(self) -> None:
        if self._last_editor_matrix is None:
            return

        translation_vector = self._last_matrix_world_with_offset.translation - self._last_editor_matrix.translation

        # This is temporary, divide the translation by the average scale of the object so that the bigger scale, the closer we hear it
        # TODO: Update this so that the scale affects the volume rather than the distance. See EIS-398
        scale = self._last_matrix_world_with_offset.scale
        translation_vector /= (scale[0] + scale[1] + scale[2]) / 3.0

        listener_rotation = self._scene.editor.rotation.mul_vector3(translation_vector)

        target = SatieManager().satie.nodes.get(str(self.uuid))
        if target:
            target.update(listener_rotation)

    def _instantiate_sound(self) -> None:
        satie_manager = SatieManager(self._scene.editor.config)
        self._audio_generator = satie_manager.satie.get_plugin_by_name(self._plugin)
        if self._audio_generator is not None:
            satie_manager.satie.add_source_node(
                str(self.uuid),
                self._audio_generator,
                group=self._group
            )
            if self._properties is not None:
                for name, value in self._properties.items():
                    satie_manager.satie.node_set(str(self.uuid), name, value)
            self._attached = True

        super()._instantiate_sound()

    def _remove_sound(self) -> None:
        SatieManager().satie.delete_node(node_name=str(self.uuid))
        super()._remove_sound()

    def _activate_property(self, name: str, value: Any) -> None:
        SatieManager().satie.node_set(str(self.uuid), name, value)

    def _copy(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy(
            graph_type,
            *args,
            group=self._group,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy_shared(
            graph_type,
            *args,
            group=self._group,
            **kwargs
        )
