from typing import Optional, Any, Type

from eis import EISEntityId
from eis.editor_support.satie_manager import SatieManager
from eis.graph.sound_objects.satie_object import SatieObject
from satnet.entity import entity


@entity(id=EISEntityId.SATIE_SWITCHER_VIDEO_OBJECT)
class SatieSwitcherVideoObject(SatieObject):

    def __init__(
            self,
            name: Optional[str] = None,
            handle: Optional[int] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            **kwargs)

        self._audio_handle = handle
        self._managed = True

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._plugin = "MonoIn"
        self._supposed_to_play = False
        self.set_property(name="gate", value=1)

    @property
    def audio_handle(self) -> Optional[int]:
        return self._audio_handle

    @audio_handle.setter
    def audio_handle(self, handle: Optional[int]) -> None:
        if self._audio_handle != handle:
            self._audio_handle = handle
            if handle is None:
                self._remove_sound()
            else:
                self.set_property(name="bus", value=self._audio_handle - 1)
                # If the handle was none when it last tried to instantiate, this will be the case, so instantiate again
                if self._supposed_to_play is True and self._attached is False:
                    self._instantiate_sound()

    def _instantiate_sound(self) -> None:
        self._supposed_to_play = True
        if self._audio_handle is None:
            return

        super()._instantiate_sound()

        # Set this property here since it owns the handle anyway
        self.set_property(name="bus", value=self._audio_handle - 1)

    def _remove_sound(self) -> None:
        self._supposed_to_play = False
        super()._remove_sound()

    def _copy(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy(
            graph_type,
            *args,
            path=self._path,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy_shared(
            graph_type,
            *args,
            path=self._path,
            **kwargs
        )
