from typing import Optional, Any, Type

from eis import EISEntityId
from eis.editor_support.satie_manager import SatieManager
from eis.graph.sound_objects.satie_object import SatieObject
from satnet.entity import entity


@entity(id=EISEntityId.SATIE_INFINITE_OBJECT)
class SatieInfiniteObject(SatieObject):

    _fields = ['_plugin']

    def __init__(
            self,
            name: Optional[str] = None,
            plugin: Optional[str] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            **kwargs)

        self._plugin = plugin

    def _copy(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy(
            graph_type,
            *args,
            plugin=self._plugin,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy_shared(
            graph_type,
            *args,
            plugin=self._plugin,
            **kwargs
        )
