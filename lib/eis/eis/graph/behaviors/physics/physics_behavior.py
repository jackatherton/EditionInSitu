import logging
from typing import Any, Dict, List, Optional

import pybullet as bullet

from eis import EISEntityId
from eis.constants import CLIENT
from eis.graph.behavior import Behavior, behavior
from eis.graph.event_handler import EventHandler, event_handler
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


@event_handler(id=EISEntityId.EVENT_HANDLER_COLLISION)
class CollisionHandler(EventHandler):
    def __init__(self,
                 parent: Optional['PhysicsBehavior'] = None,
                 trigger_behavior: Optional[str] = None,
                 trigger_kwargs: Optional[Dict[str, Any]] = None,
                 untrigger_behavior: Optional[str] = None,
                 untrigger_kwargs: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(parent=parent if isinstance(parent, PhysicsBehavior) else None,
                         trigger_behavior=trigger_behavior,
                         trigger_kwargs=trigger_kwargs,
                         untrigger_behavior=untrigger_behavior,
                         untrigger_kwargs=untrigger_kwargs)

    def step(self, now: float, dt: float) -> None:
        if not CLIENT:
            return

        parent = self._parent
        if not parent:
            return

        body = parent.body
        if body == -1:
            return

        contact_points = bullet.getContactPoints(bodyA=body)
        nb_points = len(contact_points)
        if not self._triggered and nb_points:
            self.on_triggered()
        elif self._triggered and not nb_points:
            self.on_untriggered()


@behavior(id=EISEntityId.BEHAVIOR_PHYSICS)
class PhysicsBehavior(Behavior):
    """
    Behavior adding physics properties to the object
    :param density: float - The higher the density, the higher the mass. If 0.0, the object is
    passive and will not move but will interact with other physics-enabled objects
    """

    _fields = ['_density', '_bounding_mesh']  # type: ignore

    def __init__(self, density: Optional[float] = None, bounding_mesh: Optional[str] = None) -> None:
        super().__init__()
        self._bound_box: Optional[List[float]] = None
        self._bound_volume = 0.0
        self._mass = 0.0
        self._density = density or 0.0
        self._collider = -1
        self._body = -1
        self._bounding_mesh: Optional[str] = bounding_mesh or None

        self._lateral_friction = 0.9
        self._spinning_friction = 0.001
        self._rolling_friction = 0.001
        self._restitution = 0.4

    def __del__(self) -> None:
        if self._body != -1:
            self.remove_body()

    @property
    def density(self) -> Optional[float]:
        return self._density

    @density.setter
    def density(self, value: float) -> None:
        self._density = value
        self.compute_mass()

    @property
    def mass(self) -> float:
        return self._mass

    @mass.setter
    def mass(self, value: float) -> None:
        self._mass = value
        self.remove_body()
        self.create_body()

    @property
    def body(self) -> int:
        return self._body

    @property
    def lateral_friction(self) -> float:
        return self._lateral_friction

    @lateral_friction.setter
    def lateral_friction(self, value: float) -> None:
        self._lateral_friction = value
        self.set_dynamics()

    @property
    def spinning_friction(self) -> float:
        return self._spinning_friction

    @spinning_friction.setter
    def spinning_friction(self, value: float) -> None:
        self._spinning_friction = value
        self.set_dynamics()

    @property
    def rolling_friction(self) -> float:
        return self._rolling_friction

    @rolling_friction.setter
    def rolling_friction(self, value: float) -> None:
        self._rolling_friction = value
        self.set_dynamics()

    @property
    def restitution(self) -> float:
        return self._restitution

    @restitution.setter
    def restitution(self, value: float) -> None:
        self._restitution = value
        self.set_dynamics()

    def compute_bound_box(self) -> None:
        if self._bounding_mesh is not None:
            self._bound_volume = 1.0
            self._collider = self.create_mesh_collider()
            self._bound_box = 1.0
        else:
            self._bound_box = (self._graphbase.bound_box[1] - self._graphbase.bound_box[0]) / 2.0
            bound_volume = 1.0
            for dim in self._bound_box:
                bound_volume *= dim
            self._bound_volume = bound_volume

            self._collider = self.create_box_collider()

        self.compute_mass()

    def compute_mass(self) -> None:
        self.mass = self._bound_volume * self._density  # Important to use the setter here to recreate the bullet body

    def create_body(self) -> None:
        # It seems bullet uses an indirect coordinated system so we just take the opposite of the rotation
        orn = self._graphbase.matrix_world.decompose()[1].xyzw

        self._body = bullet.createMultiBody(baseMass=self._mass,
                                            baseCollisionShapeIndex=self._collider,
                                            basePosition=self._graphbase.matrix_world.translation,
                                            baseOrientation=orn)

        if self._body == -1:
            logger.warning('Failed to create body for physics engine')
        else:
            self.set_dynamics()

    def remove_body(self) -> None:
        if self._body != -1:
            bullet.removeBody(self._body)
            self._body = -1

    def create_mesh_collider(self) -> int:
        return bullet.createCollisionShape(bullet.GEOM_MESH, fileName=self._bounding_mesh)

    def create_box_collider(self) -> int:
        return bullet.createCollisionShape(bullet.GEOM_BOX, halfExtents=self._bound_box)

    def set_dynamics(self) -> None:
        bullet.changeDynamics(bodyUniqueId=self._body,
                              linkIndex=-1,
                              lateralFriction=self._lateral_friction,
                              spinningFriction=self._spinning_friction,
                              rollingFriction=self._rolling_friction,
                              restitution=self._restitution)

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)
        if CLIENT and self._bound_box is None:
            # We cannot do it in the added_to_object callback because when we load behaviors from a project file
            # the client's objects have not been attached to the scene yet and don't have a proxy, hence no bounding box yet
            self.compute_bound_box()

        if CLIENT and self._body != -1 and self._mass != 0.0:
            position, orientation = bullet.getBasePositionAndOrientation(bodyUniqueId=self._body)

            orn = Quaternion(orientation)

            matrix_world = self._graphbase.matrix_world
            matrix_world = Matrix44.from_translation(
                Vector3(position)) * Matrix44.from_quaternion(orn) * Matrix44.from_scale(matrix_world.scale)
            self._graphbase.matrix_world = matrix_world
