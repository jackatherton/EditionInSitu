import logging
from time import time
from enum import IntEnum, unique
from typing import Any, List, Optional, TYPE_CHECKING, Type

from eis import EISEntityId
from eis.graph.base import GraphProxyBase, SharedGraphEntity
from eis.graph.behavior import Behavior
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.graph.model import Model
    from eis.graph.material import MaterialTexture
    from eis.graph.object_3d import Object3D

logger = logging.getLogger(__name__)


class TextureProxy(GraphProxyBase['Texture']):
    pass


@entity(id=EISEntityId.TEXTURE)
class Texture(SharedGraphEntity[TextureProxy]):
    """
    Texture object
    """

    _fields = ['_data', '_format', '_size_x', '_size_y', '_component_type', '_compression', '_synced_behaviors']

    @unique
    class Compression(IntEnum):
        NONE = 0
        DXT1 = 1
        DXT5 = 3

    @unique
    class ComponentType(IntEnum):
        UNKNOWN = -1
        UNSIGNED_BYTE = 0
        UNSIGNED_SHORT = 1
        FLOAT = 2

    def __init__(
            self,
            name: Optional[str] = None,
            data: Optional[bytes] = None,
            format: Optional[str] = None,
            size_x: Optional[int] = None,
            size_y: Optional[int] = None,
            component_type: Optional[ComponentType] = None,
            compression: Optional[Compression] = None,
            synced_behaviors: Optional[List[Behavior]] = None,
            local_behaviors: Optional[List[Behavior]] = None) -> None:
        super().__init__(name=name)

        self._timestamp = time()
        self._data = data or bytes()
        self._format = format or 'rgb'
        self._size_x = size_x if size_x is not None else 0
        self._size_y = size_y if size_y is not None else 0
        self._component_type = component_type if component_type is not None else Texture.ComponentType.UNSIGNED_BYTE
        self._compression = compression or Texture.Compression.NONE
        self._material_textures: List['MaterialTexture'] = []

        self._synced_behaviors: List[Behavior] = synced_behaviors if synced_behaviors is not None else []
        self._local_behaviors: List[Behavior] = local_behaviors if local_behaviors is not None else []

    def initialize(self) -> None:
        super().initialize()

        for behavior in self._synced_behaviors:
            behavior.graphbase = self
        for behavior in self._local_behaviors:
            behavior.graphbase = self

    def __str__(self) -> str:
        return "\033[0;34m" + super().__str__()

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)
        for behavior in self.behaviors:
            ret += behavior.trace(level=level + 1)
        return ret

    @property
    def behaviors(self) -> List[Behavior]:
        return self._synced_behaviors + self._local_behaviors

    @property
    def data(self) -> bytes:
        return self._data

    @data.setter
    def data(self, data: bytes) -> None:
        self._data = data
        self._timestamp = time()
        self.invalidate()

    @property
    def format(self) -> str:
        return self._format

    @property
    def compression(self) -> 'Compression':
        return self._compression

    @property
    def material_textures(self) -> List['MaterialTexture']:
        return self._material_textures

    # These 2 methods should only be called by material_texture
    def add_material_texture(self, material_texture: 'MaterialTexture') -> None:
        if material_texture not in self._material_textures:
            self._material_textures.append(material_texture)

    def remove_material_texture(self, material_texture: 'MaterialTexture') -> None:
        if material_texture in self._material_textures:
            self._material_textures.remove(material_texture)

    @property
    def size_x(self) -> int:
        return self._size_x

    @size_x.setter
    def size_x(self, width: int) -> None:
        self._size_x = width

    @property
    def size_y(self) -> int:
        return self._size_y

    @size_y.setter
    def size_y(self, height: int) -> None:
        self._size_y = height

    @property
    def timestamp(self) -> float:
        return self._timestamp

    @property
    def component_type(self) -> int:
        return self._component_type

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model)
        for behavior in self.behaviors:
            behavior.added_to_model(model)
        model.add_texture(self)

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model)
        for behavior in self.behaviors:
            behavior.removed_from_model(model)
        model.remove_texture(self)

    def added_to_object3d(self, object3d: 'Object3D') -> None:
        pass

    def removed_from_object3d(self, object3d: 'Object3D') -> None:
        pass

    def add_behavior(self, behavior: Behavior, synchronize: bool = True) -> None:
        """
        Add a behavior to the graph entity.

        :param behavior: Behavior
        :return: None
        """
        assert behavior

        # Control that the behavior can be set to this object
        if not behavior.can_control(self):
            return

        for existing_behavior in self.behaviors:
            if type(existing_behavior) == type(behavior):
                logger.warning("There already is a behavior of type \"{}\" on this object".format(type(behavior)))
                return

        if synchronize:
            self._synced_behaviors.append(behavior)
        else:
            self._local_behaviors.append(behavior)
        behavior.graphbase = self

    def get_behavior_by_type(self, behavior_type: Behavior) -> Optional[Behavior]:
        """
        Get the behavior of the specified type or None if there is none

        :param behavior_type: Behavior type
        :return: Behavior of the specified type or None
        """
        for behavior in self.behaviors:
            if isinstance(behavior, behavior_type):
                return behavior
        return None

    def remove_behavior(self, behavior: Behavior) -> None:
        """
        Remove a behavior from the graph entity

        :param behavior: Behavior
        :return: None
        """
        assert behavior
        if behavior in self._synced_behaviors:
            self._synced_behaviors.remove(behavior)
        elif behavior in self._local_behaviors:
            self._local_behaviors.remove(behavior)
        behavior.graphbase = None

    def remove_behavior_by_type(self, behavior_type: Type[Behavior]) -> None:
        """
        Remove all behaviors of the given type

        :param behavior_type: Behavior type
        :return: None
        """
        for behavior in self.behaviors:
            if isinstance(behavior, behavior_type):
                self.remove_behavior(behavior)

    def step(self, now: float, dt: float) -> None:
        # Behaviors are ran first
        for behavior in self.behaviors:
            behavior.step(now=now, dt=dt)

        super().step(now=now, dt=dt)

    def _copy(self, graph_type: Type['Texture'], *args: Any, **kwargs: Any) -> 'Texture':
        return super()._copy(
            graph_type,
            *args,
            data=bytes(self._data),
            format=self._format,
            size_x=self._size_x,
            size_y=self._size_y,
            component_type=self._component_type,
            compression=self._compression,
            synced_behaviors=self._synced_behaviors.copy(),  # We don't want to share behaviors
            local_behaviors=self._local_behaviors.copy(),
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Texture'], *args: Any, **kwargs: Any) -> 'Texture':
        return super()._copy_shared(
            graph_type,
            *args,
            data=self._data,  # Can we really share the data like this?
            format=self._format,
            size_x=self._size_x,
            size_y=self._size_y,
            component_type=self._component_type,
            compression=self._compression,
            synced_behaviors=self._synced_behaviors.copy(),  # We don't want to share behaviors
            local_behaviors=self._local_behaviors.copy(),
            **kwargs
        )
