import math
from typing import Any, Type

from eis.entity import Sync
from eis.graph.helpers import AxisHelper
from eis.graph.helpers.arrow_square import ArrowSquare
from eis.graph.material import Material


class ScaleHelper(AxisHelper):
    arrow_radius = 0.025
    arrow_head_radius = 0.1
    arrow_head_length = 0.2

    def update_axis(self, sync: Sync, value: Any) -> None:
        self._x_axis.length = self._radius
        self._y_axis.length = self._radius
        self._z_axis.length = self._radius

    def __init__(
            self,
            *args: Any,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._x_material = Material(color=(1.00, 0.00, 0.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._x_material.managed = True
        self._x_axis = ArrowSquare(
            material=self._x_material,
            length=self._radius,
            radius=ScaleHelper.arrow_radius,
            head_radius=ScaleHelper.arrow_head_radius,
            head_length=ScaleHelper.arrow_head_length
        )
        self._x_axis.managed = True
        self._x_axis.z_rotation = math.pi / 2.0
        self.add_child(self._x_axis)

        self._y_material = Material(color=(0.00, 1.00, 0.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._y_material.managed = True
        self._y_axis = ArrowSquare(
            material=self._y_material,
            length=self._radius,
            radius=ScaleHelper.arrow_radius,
            head_radius=ScaleHelper.arrow_head_radius,
            head_length=ScaleHelper.arrow_head_length
        )
        self._y_axis.managed = True
        self.add_child(self._y_axis)

        self._z_material = Material(color=(0.00, 0.00, 1.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._z_material.managed = True
        self._z_axis = ArrowSquare(
            material=self._z_material,
            length=self._radius,
            radius=ScaleHelper.arrow_radius,
            head_radius=ScaleHelper.arrow_head_radius,
            head_length=ScaleHelper.arrow_head_length
        )
        self._z_axis.managed = True
        self._z_axis.x_rotation = -math.pi / 2.0
        self.add_child(self._z_axis)

    # region Copy

    def _copy(self, graph_type: Type['ScaleHelper'], *args: Any, **kwargs: Any) -> 'ScaleHelper':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['ScaleHelper'], *args: Any, **kwargs: Any) -> 'ScaleHelper':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            **kwargs
        )

    # endregion
