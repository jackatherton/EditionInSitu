from typing import Any, Optional, Type

from eis.entity import Sync
from eis.graph import Color
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from eis.graph.primitives.box import Box
from eis.graph.primitives.cylinder import Cylinder


class ArrowSquare(Object3D):

    def update_arrow(self, sync: Sync, previous_value: Any, value: Any) -> None:
        self._material.color = self._color

        self._body.radius_top = self._radius
        self._body.radius_bottom = self._radius
        self._body.height = self._length - self._head_length
        self._body.y = self._body.height / 2.0

        self._head.width = self._head_radius * 2.0
        self._head.depth = self._head_radius * 2.0
        self._head.height = self._head_length
        self._head.y = self._body.height + (self._head.height / 2.0)

    radius = Sync[float]('_radius', 0.025, on_changed=update_arrow)
    length = Sync[float]('_length', 1.0, on_changed=update_arrow)
    head_radius = Sync[float]('_head_radius', 0.05, on_changed=update_arrow)
    head_length = Sync[float]('_head_length', 0.125, on_changed=update_arrow)
    color = Sync[Color]('_color', (1.00, 1.00, 0.00, 1.00), on_changed=update_arrow)

    def __init__(
            self,
            *args: Any,
            color: Color = (1.00, 1.00, 0.00, 1.00),
            length: float = 1.0,
            radius: float = 0.025,
            head_radius: float = 0.05,
            head_length: float = 0.125,
            material: Optional[Material] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._color = color
        self._length = length
        self._radius = radius
        self._head_radius = head_radius
        self._head_length = head_length

        if not material:
            material = Material(
                color=self._color,
                shading_model=Material.ShadingModel.EMISSIVE
            )
            material.managed = True
        else:
            self._color = material.color

        self._body = Cylinder(
            material=material,
            radius_top=self._radius,
            radius_bottom=self._radius,
            height=self._length - self._head_length
        )
        self._body.managed = True
        self._body.y = self._body.height / 2.0
        self.add_child(self._body)

        self._head = Box(
            material=material,
            width=self._head_radius * 2.0,
            depth=self._head_radius * 2.0,
            height=self._head_length
        )
        self._head.managed = True
        self._head.y = self._body.height + (self._head.height / 2.0)
        self.add_child(self._head)

    # region Copy

    def _copy(self, graph_type: Type['ArrowSquare'], *args: Any, **kwargs: Any) -> 'ArrowSquare':
        return super()._copy(
            graph_type,
            *args,
            color=self._color,
            radius=self._radius,
            length=self._length,
            head_radius=self._head_radius,
            head_length=self._head_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['ArrowSquare'], *args: Any, **kwargs: Any) -> 'ArrowSquare':
        return super()._copy_shared(
            graph_type,
            *args,
            color=self._color,
            radius=self._radius,
            length=self._length,
            head_radius=self._head_radius,
            head_length=self._head_length,
            **kwargs
        )

    # endregion
