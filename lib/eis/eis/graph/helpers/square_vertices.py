from math import pi
from typing import Any, List, Optional, Type

from eis.entity import Sync
from eis.graph import Color
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from eis.graph.primitives.cone import Cone
from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3


class SquareVertices(Object3D):

    def update_dimensions(self, sync: Sync, previous_value: Any, value: Any) -> None:
        if value != previous_value:
            self._update_vertices()

    def update_color(self, sync: Sync, previous_value: Any, value: Any) -> None:
        if value != previous_value:
            self._color = value
            self._material.color = value

    color = Sync[Color]('_color', (1.0, 1.0, 0.0, 1.0), on_changed=update_color)
    width = Sync[float]('_width', 1.0, on_changed=update_dimensions)
    height = Sync[float]('_height', 1.0, on_changed=update_dimensions)

    def __init__(
        self,
        *args: Any,
        color: Color = (1.0, 1.0, 0.0, 1.0),
        width: float = 1.0,
        height: float = 1.0,
        material: Optional[Material] = None,
        **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._color = color
        self._width = width
        self._height = height

        if material is None:
            material = Material(
                color=self._color,
                shading_model=Material.ShadingModel.EMISSIVE
            )

        self._material = material
        self._vertices: List[Object3D] = []

        for vertex in self._vertices:
            self.remove_child(vertex)
        self._vertices.clear()

        for index in range(0, 4):
            arrow = Cone(
                material=self._material,
                radius=0.05,
                height=0.125
            )
            self.add_child(arrow)
            self._vertices.append(arrow)

        self._update_vertices()

    def _update_vertices(self):
        half_width = self._width / 2.0
        half_height = self._height / 2.0

        for index in range(len(self._vertices)):
            self._vertices[index].matrix = Matrix44.from_euler(Euler((0.0, pi / 2.0 * float(index), 0.0))) * \
                Matrix44.from_translation(Vector3((half_width, 0.0, half_height))) * \
                Matrix44.from_euler(Euler((pi / 2.0, 0.0, 3.0 * pi / 4.0)))

    def _copy(self, graph_type: Type['SquareVertices'], *args: Any, **kwargs: Any) -> 'SquareVertices':
        return super()._copy(
            graph_type,
            *args,
            color=self._color,
            width=self._width,
            height=self._height,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SquareVertices'], *args: Any, **kwargs: Any) -> 'SquareVertices':
        return super()._copy_shared(
            graph_type,
            *args,
            color=self._color,
            width=self._width,
            height=self._height,
            **kwargs
        )
