from typing import Any, Type

from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives.shape import Shape
from eis.graph.helpers import AxisHelper


class Axis(AxisHelper):

    def update_axis(self, sync: Sync, previous_value: Any, value: Any) -> None:
        self.update_geometry()

    length = Sync[float]('_length', 1000.0, on_changed=update_axis)
    gutter = Sync[float]('_gutter', 0.0, on_changed=update_axis)

    def __init__(
            self,
            *args: Any,
            length: float = 1000.0,
            gutter: float = 0.0,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._length = length
        self._gutter = gutter

        self._x_material = Material(color=(1.00, 0.00, 0.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._x_material.managed = True
        self._x_axis = Shape(material=self._x_material)
        self._x_axis.managed = True
        self.add_child(self._x_axis)

        self._y_material = Material(color=(0.00, 1.00, 0.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._y_material.managed = True
        self._y_axis = Shape(material=self._y_material)
        self._y_axis.managed = True
        self.add_child(self._y_axis)

        self._z_material = Material(color=(0.00, 0.00, 1.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._z_material.managed = True
        self._z_axis = Shape(material=self._z_material)
        self._z_axis.managed = True
        self.add_child(self._z_axis)

        self.update_geometry()

    def update_geometry(self) -> None:
        self._x_axis.points = [
            (-self._length, 0.00, 0.00),
            (-self._gutter, 0.00, 0.00),
            (self._gutter, 0.00, 0.00),
            (self._length, 0.00, 0.00)
        ]
        self._y_axis.points = [
            (0.00, -self._length, 0.00),
            (0.00, -self._gutter, 0.00),
            (0.00, self._gutter, 0.00),
            (0.00, self._length, 0.00)
        ]
        self._z_axis.points = [
            (0.00, 0.00, -self._length),
            (0.00, 0.00, -self._gutter),
            (0.00, 0.00, self._gutter),
            (0.00, 0.00, self._length)
        ]

    def _copy(self, graph_type: Type['Axis'], *args: Any, **kwargs: Any) -> 'Axis':
        return super()._copy(
            graph_type,
            *args,
            length=self._length,
            gutter=self._gutter,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Axis'], *args: Any, **kwargs: Any) -> 'Axis':
        return super()._copy_shared(
            graph_type,
            *args,
            length=self._length,
            gutter=self._gutter,
            **kwargs
        )
