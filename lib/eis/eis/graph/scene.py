import logging
from typing import List, Optional, TYPE_CHECKING

from eis import EISEntityId
from eis.entity import SyncEntity
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from satlib.language import fancy_namer
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.editor import Editor
    from eis.engine import Engine

logger = logging.getLogger(__name__)


@entity(id=EISEntityId.SCENE)
class Scene(SyncEntity):
    """
    Scene
    Holds the scene graph root, as well as global configurations
    """

    _fields: List[str] = ['_name', '_model']

    def __init__(
            self,
            name: Optional[str] = None,
            model: Optional[Model] = None,
            engine: Optional['Engine'] = None
    ) -> None:
        super().__init__()
        self._name = name if name else (model.name if model else fancy_namer.generate_name())
        self._model = model or Model()
        self._engine = engine
        self._editor: Optional[Editor] = None

    def initialize(self) -> None:
        super().initialize()

        # Propagate the scene ownership in hierarchy
        self._model.root.scene = self

        if self._editor:
            # Will call the added_to_editor if needed
            self._model.root.editor = self._editor

        # Only add the delegate after, otherwise it'll try so sync the model initialization
        # Also only set the delegate if we have an engine, because we don't want to overwrite
        # an existing delegate in the model (mostly relevant in the unit tests)
        if self._engine:
            self._model.delegate = self._engine.delegate

    @property
    def model(self) -> Model:
        return self._model

    @property
    def editor(self) -> Optional['Editor']:
        return self._editor

    @editor.setter
    def editor(self, value: Optional['Editor']) -> None:
        if self._editor != value:
            if self._editor is not None:
                self._model.on_scene_unloaded()
            self._editor = value
            if self._editor is not None:
                self._model.on_scene_loaded()
                self.added_to_editor()
            else:
                self.removed_from_editor()
            self.model.root.editor = value

    def added_to_editor(self) -> None:
        # Add every children to editor recursively
        def add_child_to_editor(obj: Object3D) -> None:
            obj.added_to_editor()
            for child in obj.children:
                add_child_to_editor(child)

        add_child_to_editor(self._model.root)

    def removed_from_editor(self) -> None:
        # Remove every children to editor recursively
        def remove_child_from_editor(obj: Object3D) -> None:
            obj.removed_from_editor()
            for child in obj.children:
                remove_child_from_editor(child)

        remove_child_from_editor(self._model.root)

    @property
    def engine(self) -> Optional['Engine']:
        return self._engine

    @engine.setter
    def engine(self, value: 'Engine') -> None:
        if self._engine != value:
            self._engine = value
            if self._engine:
                self._model.delegate = self._engine.delegate

    @property
    def name(self) -> str:
        return self._name

    def step(self, now: float, dt: float) -> None:
        """
        The scene doesn't step its object3ds, only internal scene stuff, if any.
        The object3ds are stepped by the engine.
        :param now: float
        :param dt: float
        :return: None
        """
        pass

    def add_child(self, child: Object3D) -> Object3D:
        """
        Helper to add objects to the top of the scene

        :param child: Object3D
        :return:
        """
        return self._model.root.add_child(child=child)

    def remove_child(self, child: Object3D) -> Optional[Object3D]:
        """
        Helper to remove objects from the top of the scene

        :param child: Object3D
        :return: Optional[Object3D]
        """
        return self._model.root.remove_child(child=child)
