import logging
from typing import Any, Dict, Optional, TYPE_CHECKING
from uuid import UUID

from eis.action import ActionId, EISAction
from eis.entity import Sync, SyncEntity
from eis.graph.base import GraphBase
from eis.graph.behavior import Behavior
from eis.graph.material import Material
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.primitives import Primitive
from eis.graph.texture import Texture
from satnet.action import action

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@action(id=ActionId.ADD_MODEL)
class AddModelAction(EISAction):
    _fields = ['model']

    def __init__(self, model: Optional[Model] = None) -> None:
        super().__init__()
        self.model = model

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.model is not None:
            session.editor.add_model(model=self.model, parent=session.editor._added_objects)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.model and self.model.root:
            self.model.root.remove()


@action(id=ActionId.ADD_OBJECT3D)
class AddObject3DAction(EISAction):
    _fields = ['object', 'parent_id']

    def __init__(self, object: Optional[Object3D] = None) -> None:
        super().__init__()
        self.object = object
        self.parent_id = object.parent.uuid if object and object.parent else None

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.object is None:
            logger.warning("No object to add in action.")
            return

        parent = None
        if self.parent_id:
            parent = session.editor.scene.model.get_object_by_uuid(self.parent_id)
            if not parent:
                logger.warning("Could not find parent id \"{}\" when adding object \"{}\"".format(
                    self.parent_id, self.object))
        if not parent:
            parent = session.editor.scene.model.root

        parent.add_child(self.object)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.object is None:
            logger.warning("No object to remove in action revert.")
            return

        self.object.remove()


@action(id=ActionId.REMOVE_OBJECT3D)
class RemoveObject3DAction(EISAction):
    _fields = ['object_id']

    def __init__(self, object: Optional[Object3D] = None) -> None:
        super().__init__()

        self.object_id = object.uuid if object else None

        self.parent: Optional[Object3D] = None
        self.object: Optional[Object3D] = None

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.object_id is None:
            logger.warning("No object id to remove in action")
            return

        self.object = session.editor.scene.model.get_object_by_uuid(self.object_id)
        if not self.object:
            logger.warning("Could not find object id \"{}\" for removal".format(self.object_id))
            return

        self.parent = self.object.parent
        self.object.remove()

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.object is None:
            logger.warning("No object to add in action revert.")
            return

        if self.parent:
            self.parent.add_child(self.object)
        else:
            session.editor.scene.model.root.add_child(self.object)


@action(id=ActionId.ADD_BEHAVIOR)
class AddBehaviorAction(EISAction):
    _fields = ['behavior', 'object_id']

    def __init__(self, object: Optional[GraphBase] = None, behavior: Optional[Behavior] = None) -> None:
        super().__init__()
        self.behavior = behavior
        self.object_id = object.uuid if object else (
            behavior.graphbase.uuid if behavior and behavior.graphbase else None)

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.behavior is None:
            logger.warning("No behavior to add in action.")
            return

        object = None
        if self.object_id:
            object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
            if not object:
                logger.warning("Could not find entity id \"{}\" when adding behavior \"{}\"".format(
                    self.object_id, self.behavior))
        if not object:
            object = session.editor.scene.model.root

        object.add_behavior(self.behavior)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.behavior is None:
            logger.warning("No behavior to remove in action revert.")
            return

        object = None
        if self.object_id:
            object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
            if not object:
                logger.warning("Could not find entity id \"{}\" when adding behavior \"{}\"".format(
                    self.object_id, self.behavior))
        if not object:
            object = session.editor.scene.model.root

        object.remove_behavior(self.behavior)


@action(id=ActionId.REMOVE_BEHAVIOR)
class RemoveBehaviorAction(EISAction):
    _fields = ['behavior_id']

    def __init__(self, behavior: Optional[Behavior] = None) -> None:
        super().__init__()
        self.behavior_id = behavior.uuid if behavior else None

        self.graphbase: Optional[GraphBase] = None
        self.behavior: Optional[Behavior] = None

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.behavior_id is None:
            logger.warning("No behavior id to remove in action")
            return

        self.behavior = session.editor.scene.model.get_behavior_by_uuid(self.behavior_id)
        if not self.behavior:
            logger.warning("Could not find behavior id \"{}\" for removal".format(self.behavior_id))
            return

        self.graphbase = self.behavior.graphbase
        if self.graphbase:
            self.graphbase.remove_behavior(self.behavior)
        else:
            session.editor.scene.model.remove_behavior(self.behavior)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.behavior is None:
            logger.warning("No behavior to add in action revert.")
            return

        if self.graphbase:
            self.graphbase.add_behavior(self.behavior)
        else:
            session.editor.scene.model.root.add_behavior(self.behavior)


@action(id=ActionId.ADD_MATERIAL)
class AddMaterialAction(EISAction):
    _fields = ['material']

    def __init__(self, material: Optional[Material] = None) -> None:
        super().__init__()
        self.material = material

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.material is None:
            logger.warning("No material to add in action.")
            return

        session.editor.scene.model.add_material(self.material, used=False)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.material is None:
            logger.warning("No material to remove in action revert.")
            return

        logger.error("NOT IMPLEMENTED")


@action(id=ActionId.REMOVE_MATERIAL)
class RemoveMaterialAction(EISAction):
    _fields = ['material_id']

    def __init__(self, material: Optional[Material] = None) -> None:
        super().__init__()
        self.material_id = material.uuid if material else None

        self.material: Optional[Material] = None

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.material_id is None:
            logger.warning("No material id to remove in action")
            return

        self.material = session.editor.scene.model.get_material_by_uuid(self.material_id)
        if not self.material:
            logger.warning("Could not find material id \"{}\" for removal".format(self.material_id))
            return

        session.editor.scene.model.remove_material(self.material)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.material is None:
            logger.warning("No material to add in action revert.")
            return

        logger.error("NOT IMPLEMENTED")


@action(id=ActionId.ADD_TEXTURE)
class AddTextureAction(EISAction):
    _fields = ['texture']

    def __init__(self, texture: Optional[Texture] = None) -> None:
        super().__init__()
        self.texture = texture

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.texture is None:
            logger.warning("No texture to add in action.")
            return

        session.editor.scene.model.add_texture(self.texture)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.texture is None:
            logger.warning("No texture to remove in action revert.")
            return

        logger.error("NOT IMPLEMENTED")


@action(id=ActionId.REMOVE_TEXTURE)
class RemoveTextureAction(EISAction):
    _fields = ['texture_id']

    def __init__(self, texture: Optional[Texture] = None) -> None:
        super().__init__()
        self.texture_id = texture.uuid if texture else None

        self.texture = None  # type: Optional[Texture]

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.texture_id is None:
            logger.warning("No texture id to remove in action")
            return

        self.texture = session.editor.scene.model.get_texture_by_uuid(self.texture_id)
        if not self.texture:
            logger.warning("Could not find texture id \"{}\" for removal".format(self.texture_id))
            return
        session.editor.scene.model.remove_texture(self.texture)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.texture is None:
            logger.warning("No texture to add in action revert.")
            return

        logger.error("NOT IMPLEMENTED")


@action(id=ActionId.ADD_MESH)
class AddMeshAction(EISAction):
    _fields = ['mesh']

    def __init__(self, mesh: Optional[Mesh] = None) -> None:
        super().__init__()
        self.mesh = mesh

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.mesh is None:
            logger.warning("No mesh to add in action.")
            return

        session.editor.scene.model.add_mesh(self.mesh, used=False)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.mesh is None:
            logger.warning("No mesh to remove in action revert.")
            return

        logger.error("NOT IMPLEMENTED")


@action(id=ActionId.REMOVE_MESH)
class RemoveMeshAction(EISAction):
    _fields = ['mesh_id']

    def __init__(self, mesh: Optional[Mesh] = None) -> None:
        super().__init__()
        self.mesh_id = mesh.uuid if mesh else None

        self.mesh = None  # type: Optional[Mesh]

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.mesh_id is None:
            logger.warning("No mesh id to remove in action")
            return

        self.mesh = session.editor.scene.model.get_mesh_by_uuid(self.mesh_id)
        if not self.mesh:
            logger.warning("Could not find mesh id \"{}\" for removal".format(self.mesh_id))
            return

        session.editor.scene.model.remove_mesh(self.mesh)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.mesh is None:
            logger.warning("No mesh to add in action revert.")
            return

        logger.error("NOT IMPLEMENTED")


@action(id=ActionId.CHANGE_PROPERTY)
class ChangePropertyAction(EISAction):
    _fields = ['entity_id', 'sync_name', 'value']

    def __init__(
            self,
            entity: Optional[SyncEntity] = None,
            sync: Optional[Sync] = None
    ) -> None:
        super().__init__()
        self.entity_id = entity.uuid if entity else None
        self.sync_name = sync.name if sync else None
        self.value = sync.get(entity) if sync and entity else None

        self.sync: Optional[Sync[Any]] = None
        self.entity: Optional[SyncEntity] = None
        self.previous_value: Any = None

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.entity_id is None:
            logger.warning("No sync entity id in action")
            return

        if self.sync_name is None:
            logger.warning("No sync name in action")
            return

        self.entity = session.editor.scene.model.get_entity_by_uuid(self.entity_id)
        if not self.entity:
            logger.warning("Could not find entity id \"{}\" for syncing of \"{}\" property".format(
                self.entity_id, self.sync_name))
            return

        self.sync = self.entity.get_sync_by_name(self.sync_name)
        if not self.sync:
            logger.warning("Could not find sync \"{}\" in \"{}\"".format(self.sync_name, self.entity))
            return

        # Set value silently, we don't want to recreate another synchronization cycle
        self.sync.set_silently(self.entity, self.value)

    def revert(self, session: 'EISRemoteSession') -> None:
        if not self.sync:
            logger.warning("No sync to revert previous value onto.")
            return

        if not self.entity:
            logger.warning("Could not find entity id \"{}\" for reverting of \"{}\" property".format(
                self.entity_id, self.sync_name))
            return

        self.sync.set_silently(self.entity, self.previous_value)


@action(id=ActionId.SET_MATERIAL)
class SetMaterialAction(EISAction):
    """
    This action changes the material of the given object. If the object has multiple geometries,
    all materials will be replaced.

    :param object_uuid: Optional[UUID] - UUID of the Object3D.
    :param model: Optional[Model] - Model containing the Material and its Textures, if any.
    """
    _fields = ['object_uuid', 'model']

    def __init__(self, object_uuid: Optional[UUID] = None, model: Optional[Model] = None) -> None:
        super().__init__()
        self.object_uuid = object_uuid
        self.model = model
        self.materials: Dict[UUID, Material] = {}

    def apply(self, session: 'EISRemoteSession') -> None:
        if self.object_uuid is None:
            logger.warning("No object specified for replacing its material.")
            return

        if self.model is None or not self.model.materials:
            logger.warning("No material specified as a replacement.")
            return

        material = self.model.materials[0]
        object = session.editor.scene.model.get_object_by_uuid(self.object_uuid)
        if object is None:
            logger.warning(f"No object exists with the given UUID: {self.object_uuid}")
            return

        if not object.mesh:
            logger.warning(f"Object with UUID {self.object_uuid} has no geometry")
            return

        for texture in self.model.textures:
            session.editor.scene.model.add_texture(texture=texture)

        if isinstance(object, Primitive):
            self.materials[object.uuid] = object.material
            object.material = material
        else:
            for geometry in object.mesh.geometries:
                self.materials[geometry.uuid] = geometry.material
                geometry.material = material

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.object_uuid is None:
            logger.warning("No object specified for reverting material replacement.")
            return

        object = session.editor.scene.model.get_object_by_uuid(self.object_uuid)
        assert(object is not None)

        if isinstance(object, Primitive):
            assert(object.uuid in self.materials)
            object.material = self.materials[object.uuid]
        else:
            if object.mesh is None:
                return
            for geometry in object.mesh.geometries:
                assert(geometry.uuid in self.materials)
                geometry.material = self.materials[geometry.uuid]
