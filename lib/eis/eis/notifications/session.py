from typing import Optional, TYPE_CHECKING

from eis.constants import CLIENT, SERVER
from eis.notification import EISServerNotification, NotificationId
from satnet.notification import notification

if TYPE_CHECKING:
    from eis.user import EISUser
    from eis.client.session import EISLocalSession

    if CLIENT:
        from eis.client.session import Peer
    elif SERVER:
        from eis.server.session import Peer

if CLIENT:
    from eis.client.user import PeerUser
elif SERVER:
    from eis.server.user import PeerUser


@notification(id=NotificationId.PEER_CONNECTED)
class PeerConnected(EISServerNotification):
    """
    Notification used to tell all sessions that a new peer has joined
    """

    _fields = ['peer']

    def __init__(self, peer: Optional['Peer'] = None) -> None:
        super().__init__()
        self.peer = peer

    def handle(self, session: 'EISLocalSession') -> None:
        if self.peer is not None:
            session.add_peer(self.peer)


@notification(id=NotificationId.PEER_DISCONNECTED)
class PeerDisconnected(EISServerNotification):
    """
    Notification used to tell all session that a peer has left
    """

    _fields = ['peer_id']

    def __init__(self, peer_id: Optional[int] = None) -> None:
        super().__init__()
        self.peer_id = peer_id

    def handle(self, session: 'EISLocalSession') -> None:
        if self.peer_id is not None:
            session.remove_peer(self.peer_id)


@notification(id=NotificationId.USER_ADDED)
class UserAdded(EISServerNotification):
    """
    Notification used to tell all sessions that a user has joined a session
    """

    _fields = ['peer_id', 'user']

    def __init__(self, peer_id: Optional[int] = None, user: Optional['PeerUser'] = None) -> None:
        super().__init__(context=user.uuid if user else None)
        self.peer_id = peer_id
        self.user = user

    def handle(self, session: 'EISLocalSession') -> None:
        if self.peer_id is not None and self.user is not None:
            peer = session.get_peer(self.peer_id)
            if not peer:
                return
            peer.add_user(self.user)


@notification(id=NotificationId.USER_REMOVED)
class UserRemoved(EISServerNotification):
    """
    Notification used to tell all session that a user has left a session
    """

    _fields = ['peer_id']

    def __init__(self, peer_id: Optional[int] = None, user: Optional['EISUser'] = None) -> None:
        super().__init__(context=user.uuid if user else None)
        self.peer_id = peer_id

    def handle(self, session: 'EISLocalSession') -> None:
        if self.peer_id is not None and self.context is not None:
            peer = session.get_peer(self.peer_id)
            if not peer:
                return
            peer.remove_user(self.context)
