import os
from typing import Any, Optional
from eis.assets.asset import Asset


class FileAsset(Asset):
    def __init__(self, data: Any, path: str) -> None:
        super().__init__(data=data, path=path)

    @classmethod
    def factory_create_asset(cls, path: str) -> Optional['Asset']:
        _, file_extension = os.path.splitext(path.lower())
        for plugin in cls._asset_types:
            asset = plugin.create_asset(path)
            if asset:
                return asset
        return None
