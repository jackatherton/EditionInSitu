from typing import Any, Optional

from eis.assets.asset import Asset

from pysatie.plugin import Plugin


class SatieAsset(Asset):
    def __init__(self, data: Any, plugin: Optional[str]) -> None:
        super().__init__(data=data, path=plugin)
        self._plugin = plugin
        self._sound_object = None

    @classmethod
    def factory_create_asset(cls, plug: Any) -> Optional['Asset']:
        for plugin in cls._asset_types:
            asset = plugin.create_asset(plug)
            if asset:
                return asset
        return None

    @property
    def plugin(self) -> Plugin:
        return self._plugin
