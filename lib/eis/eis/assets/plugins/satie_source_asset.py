import logging
import math
import os

from typing import Any, Optional

from eis.assets.satie_asset import SatieAsset
from eis.converters.gltf import GLTFConverter
from eis.graph.object_3d import Object3D
from eis.graph.material import Material, MaterialTexture
from eis.graph.primitives.box import Box
from eis.graph.sound_objects.satie_infinite_object import SatieInfiniteObject
from eis.graph.text import Text
from eis.graph.texture import Texture
from eis.utils.color import Colors
from satmath.euler import Euler
from satmath.matrix44 import Matrix44

from pysatie.plugin import Plugin

logger = logging.getLogger(__name__)


class SatieSourceAsset(SatieAsset):
    def __init__(self, data: Any, plugin: Optional[str]) -> None:
        super().__init__(data=data, plugin=plugin)

    @classmethod
    def create_asset(cls, plug: Any) -> Optional['SatieSourceAsset']:
        if type(plug) is Plugin:
            return cls(data=None, plugin=plug.name)
        return None

    def _make_asset_object(self, scale: float, shape: Object3D) -> Object3D:
        super()._make_asset_object(scale=scale, shape=shape)

        name: str = self._plugin
        text_scale = 0.12 * scale
        plugin_name = Text(
            text=name,
            text_color=Colors.white,
            text_scale=text_scale,
            font="SourceSansPro-Bold.ttf",
            align=Text.Align.CENTER,
            shadow_x=0.0,
            shadow_y=0.0
        )
        plugin_name.managed = True

        # Rotate the folder text to face the user
        text_rotation_matrix = Matrix44.from_euler(Euler((math.pi / 2.0, 0.0, 0.0)))

        # Center the text vertically and place it slightly in front of the plane
        text_rotation_matrix[3][1] = text_scale / 4.5
        # 0.5 so that it isn't in the cube, and an additional 0.01 for the text to be in front of the cube
        text_rotation_matrix[3][2] = -0.51 * scale

        plugin_name.matrix = text_rotation_matrix

        # Simple black rectangle
        texture = Texture()
        material = Material(
            name=self._plugin + "_material",
            textures=[MaterialTexture(texture=texture)]
        )

        object3D = Box(name=self._path, matrix=Matrix44.identity(), material=material)
        object3D.add_child(plugin_name)

        object3D.interactive = True
        object3D.matrix = Matrix44.from_euler(Euler((-math.pi / 2.0, 0.0, 0.0)))
        object3D.depth *= scale
        object3D.width *= scale
        object3D.height *= scale

        self._sound_object = SatieInfiniteObject(name=self._path, plugin=self._plugin)

        return object3D

    def _instantiate_behaviors(self) -> None:
        super()._instantiate_behaviors()
        # self._sound_object will be none if the object is created from the cache
        if self._sound_object is None:
            for child in self._object3D.children:
                if type(child) == SatieInfiniteObject:
                    self._sound_object = child

    def on_hovered(self) -> None:
        super().on_hovered()
        if self._sound_object is not None:
            self._object3D.add_child(self._sound_object)

    def on_unhovered(self) -> None:
        super().on_unhovered()
        if self._sound_object is not None:
            self._object3D.remove_child(self._sound_object)

    def object_3d_copy(self) -> Object3D:
        path = os.path.join(os.path.realpath(".."), "res/models/speaker/speaker.gltf")
        scene = GLTFConverter.import_path(path)
        asset_root = Object3D()
        asset_root.interactive = True
        asset_root.name = scene.model.root.name
        asset_root.add_child(scene.model.root)
        asset_root.matrix = self._object3D.matrix
        asset_root.matrix_offset = self._object3D.matrix_offset
        asset_root.add_child(self._sound_object)

        return asset_root

    def instantiate(self, scale: float, shape: Object3D) -> Object3D:
        self._object3D = self._make_asset_object(scale=scale, shape=shape)
        self._instantiate_behaviors()

        return self._object3D
