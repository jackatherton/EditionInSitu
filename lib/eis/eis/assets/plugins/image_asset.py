import math

from typing import Any, Optional

from PIL import Image

from eis.assets.file_asset import FileAsset
from eis.graph.object_3d import Object3D
from eis.graph.material import Material, MaterialTexture
from eis.graph.primitives.box import Box
from eis.graph.primitives.plane import Plane
from eis.graph.texture import Texture
from satmath.euler import Euler
from satmath.matrix44 import Matrix44


class ImageAsset(FileAsset):
    def __init__(self, data: Any, path: str) -> None:
        super().__init__(data=data, path=path)
        self._width, self._height = self._data.size
        self._changeable_shape = True

    @classmethod
    def create_asset(cls, file: str) -> Optional['ImageAsset']:
        """
        Tries to open file as an image, if successful, returns a PIL handle to the image that MUST be
        closed manually after use
        :param file: str - Path of the file to open
        :return: ImageAsset if successful, None otherwise
        """
        if type(file) is not str:
            return None
        try:
            img = Image.open(file)
        except Exception:
            return None
        else:
            return cls(img, file)

    def _make_asset_object(self, scale: float, shape: Object3D) -> Object3D:
        super()._make_asset_object(scale=scale, shape=shape)
        texture = Texture(
            name=self._path,
            data=self._data.convert('RGBA').tobytes(),
            format='rgba',
            size_x=self._width,
            size_y=self._height,
            component_type=Texture.ComponentType.UNSIGNED_BYTE,
            compression=Texture.Compression.NONE
        )

        material = Material(
            name=self._path + "_material",
            textures=[MaterialTexture(texture=texture)]
        )

        shape.name = self._path
        shape.material = material

        if type(shape) is Box or type(shape) is Plane:
            image_ratio = self._width / self._height
            plane_ratio = shape.width / shape.height
            if image_ratio > plane_ratio:
                shape.height *= plane_ratio / image_ratio
            elif image_ratio < plane_ratio:
                shape.width *= image_ratio / plane_ratio

        shape.interactive = True
        shape.matrix = Matrix44.from_euler(Euler((-math.pi / 2.0, 0.0, 0.0)))

        shape.scale *= scale

        return shape
