import math
import os

from typing import Any, Optional

from eis.assets.file_asset import FileAsset
from eis.graph.behaviors.video_lod_behavior import VideoLODBehavior
from eis.graph.material import Material, MaterialTexture
from eis.graph.object_3d import Object3D
from eis.graph.primitives.box import Box
from eis.graph.primitives.plane import Plane
from eis.graph.texture_video import TextureVideo
from satmath.euler import Euler
from satmath.matrix44 import Matrix44


class VideoAsset(FileAsset):
    def __init__(self, data: Any, path: str) -> None:
        super().__init__(data=data, path=path)
        self._changeable_shape = True

    @classmethod
    def create_asset(cls, file: str) -> Optional['VideoAsset']:
        # This is a temporary fix to address the issue of how assets are loaded (EIS-329)
        if type(file) is str:
            _, file_extension = os.path.splitext(file.lower())
            if file_extension in [".mkv", ".avi", ".mp4", ".mov"]:
                return cls(data=None, path=file)
        return None

    def _make_asset_object(self, scale: float, shape: Object3D) -> Object3D:
        super()._make_asset_object(scale=scale, shape=shape)
        # Make an image with the waveform of the sound file
        texture_vid = TextureVideo(
            name=self._path,
            video_uri=self._path
        )

        material = Material(
            name=self._path + "_material",
            textures=[MaterialTexture(texture=texture_vid)]
        )

        shape.name = self._path
        shape.path = self._path
        shape.material = material

        shape.interactive = True
        shape.matrix = Matrix44.from_euler(Euler((-math.pi / 2.0, 0.0, 0.0)))
        video_behavior = VideoLODBehavior()
        shape.add_behavior(video_behavior)

        shape.scale *= scale

        return shape

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)
        video_texture = self._object3D.material.textures[0].texture
        width = video_texture.size_x
        height = video_texture.size_y
        if width and height and type(self._object3D is Box or type(self._object3D) is Plane):
            video_ratio = width / height
            plane_ratio = self._object3D.width / self._object3D.height
            if video_ratio > plane_ratio:
                self._object3D.height *= plane_ratio / video_ratio
            elif video_ratio < plane_ratio:
                self._object3D.width *= video_ratio / plane_ratio

    def on_hovered(self) -> None:
        super().on_hovered()

    def on_unhovered(self) -> None:
        super().on_unhovered()

    def object_3d_copy(self) -> Object3D:
        copy = self.object3D.copy_shared()
        return copy
