from typing import Any, Dict, List


class Singleton(type):
    _instances: Dict[str, Any] = {}

    def __call__(cls: Any, *args: List[Any], **kwargs: Dict[str, Any]) -> Any:
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
