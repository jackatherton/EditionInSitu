import logging

from typing import Optional, TYPE_CHECKING

from eis.constants import CLIENT
from eis.requests.timeline import GetTimelinePosition

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor

logger = logging.getLogger(__name__)


class Timeline():
    """
    EIS timeline
    Holds the timeline, synchronized over server and clients
    """

    def __init__(self, editor: 'ClientEditor') -> None:
        self._editor = editor
        self._time = 0.0
        self._running = False
        self._reverse = False
        self._stop_time: Optional[float] = None

    @property
    def time(self) -> float:
        return self._time

    @time.setter
    def time(self, time: float) -> None:
        if time < 0.0:
            if self._editor.config.get('timeline.looping'):
                self._time = self._editor.config.get('timeline.duration')
            else:
                self._time = 0.0
        elif time > self._editor.config.get('timeline.duration'):
            if self._editor.config.get('timeline.looping'):
                self._time = 0.0
            else:
                self._time = self._editor.config.get('timeline.duration')
        else:
            self._time = time

    @property
    def running(self) -> bool:
        return self._running

    def next_keyframe(self) -> None:
        """
        Searches for the closest keyframe right after self._time
        """
        animation_curves = self._editor.scene.model.animation_curves
        next_timing = float('inf')
        for curve in animation_curves:
            next_keyframe = curve.get_enclosing_keyframes(time=self._time)[1]
            if next_keyframe is not None and next_timing > next_keyframe[0]:
                next_timing = next_keyframe[0]
        if next_timing != float('inf'):
            self._time = next_timing

    def previous_keyframe(self) -> None:
        """
        Searches for the closest keyframe right before self._time
        """
        previous_timing = -1.0
        animation_curves = self._editor.scene.model.animation_curves
        for curve in animation_curves:
            previous_keyframe, _ = curve.get_enclosing_keyframes(time=self._time)
            if previous_keyframe is not None and previous_timing < previous_keyframe[0]:
                previous_timing = previous_keyframe[0]
        if previous_timing != -1.0:
            self._time = previous_timing

    def reset(self, time: Optional[float] = None) -> None:
        """
        Reset the time position to 0.0, or to the given time
        :param time: Optional[float] - Time to reset to
        """
        self.time = time or 0.0

    def start(self, reverse: bool = False, stop_time: Optional[float] = None) -> None:
        """
        Start the timeline, set a stop time if provided and enable reverse mode if set to true
        """
        self._running = True
        self._reverse = reverse
        if stop_time is not None:
            if not reverse:
                self._stop_time = min(stop_time, self._editor.config.get('timeline.duration'))
            elif reverse:
                self._stop_time = max(stop_time, 0.0)

    def _step_animation(self, dt:float) -> None:
        """
        Step in time given a dt and update the state of the timeline according to the new time
        """
        if not self._reverse:
            new_time = self._time + dt

            if new_time > self._editor.config.get('timeline.duration'):
                if self._editor.config.get('timeline.looping'):
                    self._time = 0.0
                else:
                    self._time = self._editor.config.get('timeline.duration')
                    self.stop()
            elif self._stop_time is not None and new_time >= self._stop_time:
                self._time = self._stop_time
                self.stop()
            else:
                self._time = new_time
        else:
            new_time = self._time - dt
            if self._stop_time is not None and new_time <= self._stop_time:
                self._time = self._stop_time
                self.stop()
            elif new_time < 0.0:
                if self._editor.config.get('timeline.looping'):
                    self._time = self._editor.config.get('timeline.duration')
                else:
                    self._time = 0.0
                    self.stop()
            else:
                self._time = new_time

    def step(self, now: float, dt: float) -> None:
        if CLIENT:
            self._editor.machine.client.session.request(GetTimelinePosition())
            return

        if not self._running:
            return

        self._step_animation(dt)

    def stop(self) -> None:
        """
        Stop the timeline
        """
        self._running = False
        self._reverse = False
        self._stop_time = None
