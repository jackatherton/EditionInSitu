import math

from typing import Any, Callable, List, Optional, Tuple

from rx.core import Disposable  # type: ignore
from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.user import LocalEISUser
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_menu import EditorRadialMenu
from eis.graph.object_3d import Object3D
from eis.graph.primitives.shapes.circle import Circle
from eis.graph.text import Text
from eis.inputs.htc_vive import HTCVive

from satmath.matrix44 import Matrix44  # type: ignore
from satmath.euler import Euler  # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore


class CalibratingState(BaseEISState):
    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, name="calibrating", **kwargs)

        self._on_exit = on_exit

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="Calibrate",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00)
        )

        self._modes = ['CAMERA', 'TRACKING', 'CONTROLLER']
        self._mode = -1

        self._ui_container = None
        self._calibration_ui = None
        self._calibration_text = None
        self._current_mode_label = None
        self._current_mode_detail_1 = None
        self._current_mode_detail_2 = None
        self._camera_ui = None
        self._tracking_ui = None

        self._previous_axis_x = 0.0
        self._previous_axis_y = 0.0
        self._previous_axis_z = 0.0
        self._previous_axis_w = 0.0
        self._delta_x = 0.0
        self._delta_y = 0.0
        self._delta_z = 0.0
        self._delta_w = 0.0

        self._inputs_mapping.extend([("menu", "_back", "Go back"),
                                     ("select", "_switch_mode", "Switch mode"),
                                     ("axis_x", "_slide_x", None),
                                     ("axis_y", "_slide_y", None),
                                     ("axis_z", "_slide_z", None),
                                     ("axis_w", "_slide_w", None)])

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        return should

    def enter(self, event: EventData) -> None:
        super().enter(event)

        ui_distance = 10.0
        ui_height = 1.0

        # Cursor
        self.cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="Calibrate",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00)
        )

        # UI
        self._ui_container = Object3D()
        self.editor.gui.add_child(self._ui_container)

        self._calibration_ui = Object3D()
        self._ui_container.add_child(self._calibration_ui)

        self._calibration_text = Object3D()
        self._calibration_text.y = ui_distance
        self._calibration_text.z = ui_height + 2.00
        self._calibration_ui.add_child(self._calibration_text)

        self._current_mode_label = Text(font="SourceSansPro-Bold.ttf")
        self._current_mode_label.align = Text.Align.CENTER
        self._current_mode_label.z = 1.0
        self._calibration_text.add_child(self._current_mode_label)

        self._current_mode_detail_1 = Text(font="SourceSansPro-Bold.ttf")
        self._current_mode_detail_1.align = Text.Align.CENTER
        self._current_mode_detail_1.z = 0.0
        self._calibration_text.add_child(self._current_mode_detail_1)

        self._current_mode_detail_2 = Text(font="SourceSansPro-Bold.ttf")
        self._current_mode_detail_2.align = Text.Align.CENTER
        self._current_mode_detail_2.z = -1.0
        self._calibration_text.add_child(self._current_mode_detail_2)

        # Camera
        self._camera_ui = Object3D()
        self._camera_ui.visible = False
        self._calibration_ui.add_child(self._camera_ui)

        north = Text(text="N", text_color=(0.0, 1.0, 0.0, 1.0), text_scale=1.0, font="SourceSansPro-Bold.ttf")
        north.y = ui_distance
        north.z = ui_height
        self._camera_ui.add_child(north)

        south = Text(text="S", text_color=(0.0, 1.0, 0.0, 1.0), text_scale=1.0, font="SourceSansPro-Bold.ttf")
        south.matrix = Matrix44.from_euler(Euler((0.0, 0.0, -math.pi))) * \
            Matrix44.from_translation(Vector3((0.0, ui_distance, ui_height)))
        self._camera_ui.add_child(south)

        east = Text(text="E", text_color=(0.0, 1.0, 0.0, 1.0), text_scale=1.0, font="SourceSansPro-Bold.ttf")
        east.matrix = Matrix44.from_euler(Euler((0.0, 0.0, -math.pi * 0.5))) * \
            Matrix44.from_translation(Vector3((0.0, ui_distance, ui_height)))
        self._camera_ui.add_child(east)

        west = Text(text="W", text_color=(0.0, 1.0, 0.0, 1.0), text_scale=1.0, font="SourceSansPro-Bold.ttf")
        west.matrix = Matrix44.from_euler(Euler((0.0, 0.0, math.pi * 0.5))) * \
            Matrix44.from_translation(Vector3((0.0, ui_distance, ui_height)))
        self._camera_ui.add_child(west)

        # Tracking
        self._tracking_ui = Object3D()
        self._tracking_ui.visible = False
        self._calibration_ui.add_child(self._tracking_ui)

        north = Circle(radius=0.2, segments=12, width=2.0, color=(0.0, 1.0, 0.0, 1.0))
        north.matrix = Matrix44.from_translation(
            Vector3((0.0, ui_distance, ui_height))) * Matrix44.from_euler(Euler((-math.pi * 0.5, 0.0, 0.0)))
        self._tracking_ui.add_child(north)

        south = Circle(radius=0.2, segments=12, width=2.0, color=(0.0, 1.0, 0.0, 1.0))
        south.matrix = Matrix44.from_euler(Euler((0.0, 0.0, -math.pi))) * Matrix44.from_translation(
            Vector3((0.0, ui_distance, ui_height))) * Matrix44.from_euler(Euler((-math.pi * 0.5, 0.0, 0.0)))
        self._tracking_ui.add_child(south)

        east = Circle(radius=0.2, segments=12, width=2.0, color=(0.0, 1.0, 0.0, 1.0))
        east.matrix = Matrix44.from_euler(Euler((0.0, 0.0, -math.pi * 0.5))) * Matrix44.from_translation(
            Vector3((0.0, ui_distance, ui_height))) * Matrix44.from_euler(Euler((-math.pi * 0.5, 0.0, 0.0)))
        self._tracking_ui.add_child(east)

        west = Circle(radius=0.2, segments=12, width=2.0, color=(0.0, 1.0, 0.0, 1.0))
        west.matrix = Matrix44.from_euler(Euler((0.0, 0.0, math.pi * 0.5))) * Matrix44.from_translation(
            Vector3((0.0, ui_distance, ui_height))) * Matrix44.from_euler(Euler((-math.pi * 0.5, 0.0, 0.0)))
        self._tracking_ui.add_child(west)

    def exit(self, event: EventData) -> None:
        super().exit(event)

        self.machine.save_config()

        self.editor.gui.remove_child(self._ui_container)
        self._ui_container = None

        self._calibration_ui = None

        self._calibration_text = None
        self._current_mode_label = None
        self._current_mode_detail_1 = None
        self._current_mode_detail_2 = None

        self._camera_ui = None
        self._tracking_ui = None

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)

        delta_x = self._delta_x
        delta_y = self._delta_y
        delta_z = self._delta_z
        delta_w = self._delta_w
        self._delta_x = 0.0
        self._delta_y = 0.0
        self._delta_z = 0.0
        self._delta_w = 0.0

        if self._mode == -1:
            self._mode = 0
        mode = self._modes[self._mode]

        local_config = self.machine.local_config['editor']

        if mode == 'CAMERA':
            self._camera_ui.visible = True
            self._tracking_ui.visible = False

            self._current_mode_label.text = "Camera"
            self._current_mode_detail_1.text = "Use the trackpad on the secondary controller to rotate and align"
            self._current_mode_detail_2.text = "the scene relative to the projection space"

            rotation = Quaternion.from_euler(Euler((0.0, 0.0, delta_z))) * self.editor.rotation_offset
            self.editor.rotation_offset = rotation
            rotation_as_euler = Euler.from_quaternion(rotation)
            local_config['offset.rotation'] = [0.0, 0.0, rotation_as_euler[2] * 180.0 / math.pi]

        elif mode == 'TRACKING':
            self._camera_ui.visible = False
            self._tracking_ui.visible = True

            self._current_mode_label.text = "Tracking"
            self._current_mode_detail_1.text = "Aim at the targets with the primary controller and adjust alignment"
            self._current_mode_detail_2.text = "using the trackpads, primary = translation, secondary = rotation"

            rot = Euler([angle * math.pi / 180.0 for angle in local_config.get('input.vive.offset.rotation', [0.0, 0.0, 0.0])])
            rot[2] = rot[2] + delta_z

            loc = Vector3(local_config.get('input.vive.offset.location', [0.0, 0.0, 0.0]))
            loc = loc.inverse
            loc[0] = loc[0] + delta_x
            loc[1] = loc[1] + delta_y

            local_config['input.vive.offset.location'] = [*loc.inverse]
            local_config['input.vive.offset.rotation'] = [angle * 180.0 / math.pi for angle in rot]

            offset = Matrix44.from_translation(loc) * Matrix44.from_euler(rot)
            for user_id in self.editor.users:
                user = self.editor.users[user_id]
                for input_method_id in user.input_methods:
                    input_method = user.input_methods[input_method_id]
                    if isinstance(input_method, HTCVive):
                        input_method.primary.tracking_space_offset = offset
                        input_method.secondary.tracking_space_offset = offset

        elif mode == 'CONTROLLER':
            self._camera_ui.visible = False
            self._tracking_ui.visible = True

            self._current_mode_label.text = "Controller"
            self._current_mode_detail_1.text = "Fine-tune the controller orientation"
            self._current_mode_detail_2.text = ""

            rotation = Euler([angle * math.pi /
                              180.0 for angle in local_config.get('input.vive.offset.tracker_rotation', [0.0, 0.0, 0.0])])
            rotation[0] += delta_w

            local_config['input.vive.offset.tracker_rotation'] = [angle * 180.0 / math.pi for angle in rotation]

            for user_id in self.editor.users:
                user = self.editor.users[user_id]
                for input_method_id in user.input_methods:
                    input_method = user.input_methods[input_method_id]
                    if isinstance(input_method, HTCVive):
                        input_method.primary.tracker_rotation = rotation
                        input_method.secondary.tracker_rotation = rotation

    def _back(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if args[1]:
            self._on_exit()

    def _switch_mode(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if args[1]:
            self._mode = (self._mode + 1) % len(self._modes)

    def _slide_x(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        axis = args[1]
        if args[2] == 3:  # get initial position
            self._previous_axis_x = axis
        self._delta_x = self._previous_axis_x - axis
        self._previous_axis_x = axis

    def _slide_y(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        axis = args[1]
        if args[2] == 3:  # get initial position
            self._previous_axis_y = axis
        self._delta_y = self._previous_axis_y - axis
        self._previous_axis_y = axis

    def _slide_z(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        axis = args[1]
        if args[2] == 3:  # get initial position
            self._previous_axis_z = axis
        self._delta_z = self._previous_axis_z - axis
        self._previous_axis_z = axis

    def _slide_w(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        axis = args[1]
        if args[2] == 3:  # get initial position
            self._previous_axis_w = axis
        self._delta_w = self._previous_axis_w - axis
        self._previous_axis_w = axis
