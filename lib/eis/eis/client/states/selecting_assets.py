import logging

from typing import Any, Callable, List, Optional, Tuple

from transitions.core import EventData  # type: ignore

from eis.actions.engine import AddModelAction, SetMaterialAction
from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from eis.graph.geometry import Geometry
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from satmath.vector3 import Vector3  # type: ignore

logger = logging.getLogger(__name__)


class SelectingAssetsState(BaseEISState):
    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, name="selecting_assets", **kwargs)

        self._on_exit = on_exit

        self._highlighted_objects: List[Object3D] = []
        self._highlight_behavior_speed = 2.0

        # If True, the asset manager will assign the selected object material to the active object
        self._select_material = False
        self._target_object: Optional[Object3D] = None

        self._input_method = None

        # For Navigating
        self._movement = Vector3()
        self._velocity = Vector3()
        self._speed_multiplier = 0.04
        self._smoothness = 10.0

        self._move_horizontally_enabled = False
        self._horizontal_velocity = 0
        self._previous_horizontal_position = 0

        self._move_vertically_enabled = False
        self._vertical_velocity = 0
        self._previous_vertical_position = 0

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=True,
            label="Select asset to add",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("menu", "_back", "Go back"),
                                     ("select", "_select_asset", "Add asset to scene"),
                                     ("axis_x", "_move_horizontally", "Move assets horizontally"),
                                     ("axis_y", "_move_vertically", "Move assets vertically"),
                                     ("reset", "_toggle_shape", "Toggle assets shape")])

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        return should

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self._behavior = HighlightBehavior(color=(1.0, 0.6, 0.6, 1.0))
        self._behavior.managed = True

        input_method = event.kwargs.get('input')
        if input_method:
            self._input_method = input_method

        self._select_material = event.kwargs.get('select_material', False)
        self._target_object = event.kwargs.get('target_object', None)
        self.machine.editor.asset_library.rotation_z = event.kwargs.get('zrot')
        self.machine.editor.asset_library.location = Vector3([0.0, 0.0, 0.0])

    def exit(self, event: EventData) -> None:
        for highlighted_object in self._highlighted_objects:
            highlighted_object.remove_behavior_by_type(HighlightBehavior)
        self.machine.editor.asset_library.deactivate()
        self._highlighted_objects.clear()

        super().exit(event)

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        # Highlight picked object
        picked_objects: List[Object3D] = []
        for user in self.editor.users.values():
            result = self._input_method.picker.pick_in(root=self.machine.editor.asset_layer, interactive_objects=True)
            if result:
                picked_object, position = result
                if picked_object is not None:
                    picked_objects.append(picked_object)

        if picked_objects:
            for user in self.editor.users.values():
                for input_method in user.input_methods.values():
                    input_method.cursor.label = "Select asset - {}".format(picked_objects[-1].name)
        else:
            for user in self.editor.users.values():
                for input_method in user.input_methods.values():
                    input_method.cursor.label = "Select an asset to add"

        for highlighted_object in self._highlighted_objects:
            if highlighted_object not in picked_objects:
                self.machine.editor.asset_library.remove_hovered_object3D(highlighted_object)
                highlighted_object.remove_behavior_by_type(HighlightBehavior)
                self._highlighted_objects.remove(highlighted_object)
                self._input_method.cursor.color = (0.05, 0.75, 1.0, 1.0)

        for picked_object in picked_objects:
            if picked_object not in self._highlighted_objects:
                self.machine.editor.asset_library.add_hovered_object3D(picked_object)
                picked_object.add_behavior(HighlightBehavior(speed=self._highlight_behavior_speed))
                self._highlighted_objects.append(picked_object)
                self._input_method.cursor.color = (1.0, 0.75, 0.05, 1.0)

        # Movement
        # Horizontal plane movement first
        self._movement[0] += self._horizontal_velocity * dt * 60.0
        self._movement[1] += self._vertical_velocity * dt * 60.0
        movement = self._movement * self._speed_multiplier

        # Move and rotate the assets according to the movement
        self.machine.editor.asset_library.rotation_z += movement.x
        self.machine.editor.asset_library.location -= Vector3([0.0, 0.0, movement.y])
        self._movement = Vector3()

    # Should only be called by _select_asset
    def _check_add_asset_validity(self, args: Tuple[LocalInputMethod, bool]) -> bool:
        if args[0] is not self._input_method:
            return False
        if not args[1]:
            return False
        if len(self._highlighted_objects) == 0:
            return False
        return True

    def _add_asset(self) -> None:
        """
        This method should be extended to take into consideration any specific asset needs.
        """

        # Get the last element of the highlighted objects (last highlighted object)
        # If there is more than 1 cursor, more than 1 object can be highlighted, so we want the last highlighted
        # We have to make sure we remove the highlight behavior before copying the object
        self._highlighted_objects[-1].remove_behavior_by_type(HighlightBehavior)
        object = self.machine.editor.asset_library.get_asset_copy(self._highlighted_objects[-1])
        self._highlighted_objects.pop()

        if self._select_material and self._target_object is not None:
            if not self.editor.active_object:
                logger.warning("No object is active")
                return

            # We get the first material from the object, and apply it to the active object
            if not object.mesh or not object.mesh.geometries or object.mesh.geometries[0].material is None:
                return
            material = object.mesh.geometries[0].material
            # The material has to be bundled in a Model to be shipped with its textures, if any
            model = Model(root=Object3D(mesh=Mesh(geometries=[Geometry(material=material)])))
            self._machine.client.session.action(SetMaterialAction(object_uuid=self._target_object.uuid, model=model))

            self._machine.edit_object()
        elif not self._select_material:
            object.matrix_offset = self.machine.editor.matrix * object.matrix_offset
            object.apply_offset()
            self._machine.client.session.action(AddModelAction(model=Model(root=object)))
            self._machine.edit_scene()

    def _select_asset(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if not self._check_add_asset_validity(args):
            return
        self._add_asset()

    def _back(self, input: LocalInputMethod) -> None:
        self._on_exit()

    def _move_horizontally(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        if self._move_horizontally_enabled:
            if args[2]:
                if args[2] == 4:
                    self._move_horizontally_enabled = False
                else:
                    # This is because the trackpads records 0.0 values from time to time for no good reason whatsoever
                    # It notably happens the last value before disabling, and so we have to ignore them.
                    if args[1] != 0.0:
                        self._movement[0] += (args[1] - self._previous_horizontal_position) * -10.0
                        self._previous_horizontal_position = args[1]

        elif args[2] == 3:
            self._move_horizontally_enabled = True
            self._previous_horizontal_position = args[1]
        elif args[2] == 5:
            self._horizontal_velocity = args[1]

    def _move_vertically(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        # if Vive is used
        if self._move_vertically_enabled:
            if args[2]:  # We want to ignore args[2] at 0, the data isn't relevant for this
                if args[2] == 4:  # The trackpad is untouched
                    self._move_vertically_enabled = False
                else:
                    # This is because the trackpads records 0.0 values from time to time for no good reason whatsoever
                    # It notably happens the last value before disabling, and so we have to ignore them.
                    if args[1] != 0.0:
                        self._movement[1] += (args[1] - self._previous_vertical_position) * -10.0
                        self._previous_vertical_position = args[1]

        elif args[2] == 3:  # The trackpad is touched
            self._move_vertically_enabled = True
            self._previous_vertical_position = args[1]

        # if keyboard is used
        elif args[2] == 5:
            self._vertical_velocity = args[1]

    def _toggle_shape(self, input: LocalInputMethod) -> None:
        self.machine.editor.asset_library.toggle_shape()
