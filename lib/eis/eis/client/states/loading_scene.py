from typing import Any, Callable, Optional

from transitions.core import EventData  # type: ignore

from eis.client.states.base_eis_state import BaseEISState


class LoadingSceneState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="loading_scene", **kwargs)
        self._on_scene_loaded_callback = None  # type: Optional[Callable[[bool], None]]

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self.editor.engine.hide_status()
        self.editor.engine.show_status("LOADING SCENE")

        self._on_scene_loaded_callback = event.kwargs['on_loaded']
        self.editor.load_scene(scene=event.kwargs['scene'], on_loaded=self._on_scene_loaded)

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.engine.hide_status()

    def _on_scene_loaded(self, success: bool) -> None:
        self.machine.activate()
        if self._on_scene_loaded_callback:
            self._on_scene_loaded_callback(success)
