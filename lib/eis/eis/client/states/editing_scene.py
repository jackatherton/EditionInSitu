import colorsys
import logging
import math
import random

from typing import Any, List, Optional, Set, Tuple

from transitions.core import EventData  # type: ignore

from eis.actions.animation import AddKeyframeAction, LoopAnimation, RemoveKeyframeAction
from eis.actions.engine import AddBehaviorAction, AddModelAction, RemoveObject3DAction
from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.client.states.transforming import Transformations
from eis.commands.engine import RemoveBehaviorByTypeCommand, AddEventHandlerCommand, RemoveEventHandlerCommand
from eis.commands.animation import StartTimelineCommand, StopTimelineCommand, ResetTimelineCommand
from eis.display.components.cursor import Cursor
from eis.display.components.menus.editor_radial_menu import EditorRadialMenu
from eis.display.components.menus.editor_radial_timeline_menu import EditorRadialTimelineMenu
from eis.graph.behaviors.physics.physics_behavior import PhysicsBehavior, CollisionHandler
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.behaviors.auto_keyframe_behavior import AutoKeyframeBehavior
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from eis.graph.camera import Camera
from eis.graph.light import Light
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.material import Material
from eis.graph.primitives.box import Box
from eis.graph.primitives.circle import Circle
from eis.graph.primitives.cone import Cone
from eis.graph.primitives.cylinder import Cylinder
from eis.graph.primitives.plane import Plane
from eis.graph.primitives.ring import Ring
from eis.graph.primitives.sphere import Sphere
from eis.graph.primitives.torus import Torus
from eis.graph.texture_video import TextureVideo
from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satnet.commands.actions import RedoCommand, UndoCommand

logger = logging.getLogger(__name__)


class EditingSceneMenu(EditorRadialMenu):

    def __init__(self):
        super().__init__(name="EditingScene Menu")

        def _add(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, AddMenu())

        self.add_item("Add", _add)

        def _edit_object(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.edit_object()
            self.close()

        self._edit_object_button = self.add_item("Edit object", _edit_object)

        def _transform(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, TransformMenu())

        self._transform_button = self.add_item("Transform", _transform)

        def _animation(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, AnimatingMenu())

        self._animation_button = self.add_item("Animation", _animation)

        def _presentation(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.edit_presentation()
            self.close()

        self._presentation_button = self.add_item("Presentation", _presentation)

        def _physics(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, PhysicsMenu())

        self._physics_button = self.add_item("Physics", _physics)

        def _delete_selection(target: Object3D, input: LocalInputMethod) -> None:
            if self.editor.active_object:
                self.editor.machine.client.session.action(RemoveObject3DAction(self.editor.active_object))
                self.editor.active_object = None
                for object3D in self.editor.passive_objects:
                    self.editor.machine.client.session.action(RemoveObject3DAction(object3D))
                self.editor.clear_passive_objects()
            self.close()

        self._delete_button = self.add_item("Delete", _delete_selection)

        def _camera(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, CameraMenu())

        self.add_item("Camera", _camera)

        def _back(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.welcome()
            self.close()

        self.add_item("Main menu", _back)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True
        is_active_object = self.editor.active_object is not None
        self._transform_button.enabled = is_active_object
        self._delete_button.enabled = is_active_object
        self._edit_object_button.enabled = is_active_object

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class CameraMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Camera Menu")
        self.add_item("New camera", self._create)
        self._attach_camera_button = self.add_item("Attach camera", self._attach_camera)
        self._detach_camera_button = self.add_item("Detach camera", self._detach_camera)

    def _create(self, target: Object3D, input: LocalInputMethod) -> None:
        camera = Camera(name="camera")
        # We rotate by -pi/2 to be aligned with the editor's rotation
        camera.matrix = self.matrix_world * Matrix44.from_euler(Euler((-math.pi / 2.0, 0.0, 0.0)))
        self.editor.machine.client.session.action(AddModelAction(model=Model(root=camera)))
        self.close()

    def _attach_camera(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.camera = self.editor.active_object
        self.close()

    def _detach_camera(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.camera = None
        self.close()

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True
        self._attach_camera_button.enabled = self.editor.active_object is not None and self.editor.camera is None and type(
            self.editor.active_object) is Camera
        self._detach_camera_button.enabled = self.editor.camera is not None

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class AddMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Add Menu")

        def _assets(target: Object3D, input: LocalInputMethod) -> None:
            # zrot is the delta rotation between the menu's matrix and the editor's matrix
            # to get this difference, we multiply the inverse of the menu matrix world by the editor's matrix
            self.editor.machine.select_assets(
                input=input, zrot=-Euler.from_matrix((self.matrix_world.inverse * self.editor.matrix)).z)
            self.close()

        self.add_item("Asset", _assets)

        def _create_primitive(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, CreatePrimitiveMenu())

        self.add_item("Object", _create_primitive)

        def _satie_connect(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.select_satie_plugins(
                input=input, zrot=-Euler.from_matrix((self.matrix_world.inverse * self.editor.matrix)).z)
            self.close()

        self.add_item("SATIE source", _satie_connect)

        def _create_light(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, CreateLightMenu())

        self.add_item("Light", _create_light)

        def _repeat(target: Object3D, input: LocalInputMethod) -> None:
            last_added_asset = self.editor.asset_library.last_added_asset
            if last_added_asset is not None:
                new_object = last_added_asset.object_3d_copy()
                # Place the new object in the position of the cursor.
                new_object.matrix_offset = (
                    # Rotate the object 90 degrees so that it is upright
                    # (relative to the cursor).
                    self.matrix_world * Matrix44.from_euler(Euler((math.pi / 2.0, 0.0, 0.0)))
                )
                new_object.apply_offset()
                self.editor.machine.client.session.action(AddModelAction(model=Model(root=new_object)))
            self.close()

        self._add_last_asset_button = self.add_item("Add last asset", _repeat)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True
        self._add_last_asset_button.enabled = self.editor.asset_library.last_added_asset is not None

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class TransformMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Transform Menu")

        def _transform(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, ManipulateMenu())

        self.add_item("Transform", _transform)

        def _align(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, AlignmentMenu())

        self._align_button = self.add_item("Align", _align)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True
        self._align_button.enabled = bool(self.editor.passive_objects)

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class CreatePrimitiveMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__(name='Create Primitive Menu')

        primitives = [
            ("Plane", Plane),
            ("Cube", Box),
            ("Sphere", Sphere),
            ("Cylinder", Cylinder),
            ("Cone", Cone),
            ("Torus", Torus),
            ("Circle", Circle),
            ("Ring", Ring)
        ]

        for primitive in primitives:
            self.add_item(primitive[0], self._create, data=primitive[1])

    def _create(self, target: Object3D, input: LocalInputMethod) -> None:
        new_object = target.data()
        new_object.matrix = self.matrix_world
        new_object.material = Material(color=colorsys.hsv_to_rgb(h=random.uniform(0, 1), s=0.75, v=0.75) + (1.0,))
        self.editor.machine.client.session.action(AddModelAction(model=Model(root=new_object)))
        self.close()

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class CreateLightMenu(EditorRadialMenu):
    def __init__(self):
        super().__init__(name='Create Light Menu')

        light_types = [
            ("Point light", Light.LightType.POINT_LIGHT),
            ("Spot light", Light.LightType.SPOTLIGHT),
            # ("Sun light", Light.LightType.SUNLIGHT),  # Not supported by any renderer yet
        ]

        for light_type in light_types:
            self.add_item(light_type[0], self._create, data=light_type[1])

    def _create(self, target: Object3D, input: LocalInputMethod) -> None:
        new_light = Light(type=target.data, radius=16.0)
        new_light.matrix = self.matrix_world
        self.editor.machine.client.session.action(AddModelAction(model=Model(root=new_light)))
        self.close()


class ManipulateMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Manipulate Menu")

        def _translate(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.transform(transformation=Transformations.TRANSLATING)
            self.close()

        self.add_item("Translate", _translate)

        def _rotate(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.transform(transformation=Transformations.ROTATING)
            self.close()

        self.add_item("Rotate", _rotate)

        def _scale(target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.transform(transformation=Transformations.SCALING)
            self.close()

        self.add_item("Scale", _scale)

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class AlignmentMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Alignment Menu")
        self.add_item("Axis X", self._align_x)
        self.add_item("Axis Y", self._align_y)
        self.add_item("Axis Z", self._align_z)

    def _align_x(self, target: Object3D, input: LocalInputMethod) -> None:
        self._align(axis=0)
        self.close()

    def _align_y(self, target: Object3D, input: LocalInputMethod) -> None:
        self._align(axis=1)
        self.close()

    def _align_z(self, target: Object3D, input: LocalInputMethod) -> None:
        self._align(axis=2)
        self.close()

    def _align(self, axis: int) -> None:
        if self.editor.active_object:
            active_matrix_world = self.editor.active_object.matrix_world
            active_rotation = self.editor.rotation.matrix33
            for passive_object in self.editor.passive_objects:
                active_translation = active_matrix_world.translation
                passive_matrix_world = passive_object.matrix_world
                passive_translation = passive_matrix_world.translation
                # rotate both active and passive translations
                passive_translation = active_rotation.inverse.mul_vector3(passive_translation)
                active_translation = active_rotation.inverse.mul_vector3(active_translation)
                # apply alignment in accordance with the axis
                if axis != 0:
                    passive_translation[0] = active_translation[0]
                if axis != 1:
                    passive_translation[1] = active_translation[1]
                if axis != 2:
                    passive_translation[2] = active_translation[2]
                # rotate back the passive translation and overwrite the previous world_matrix with the new translation
                passive_translation = active_rotation.mul_vector3(passive_translation)
                passive_matrix_world[3, 0:3] = passive_translation[:3]
                passive_object.matrix_world = passive_matrix_world

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class PhysicsMenu(EditorRadialMenu):
    def __init__(self) -> None:
        super().__init__(name="Physics Menu")
        self._add_editor_phys_button = self.add_item("Add self physics", self._add_physics_to_editor, data=1.0)
        self._remove_editor_phys_button = self.add_item("Remove self physics", self._remove_physics_from_editor)
        self._active_phys_button = self.add_item("Add active physics", self._add_physics, data=1.0)
        self._passive_phys_button = self.add_item("Add passive physics", self._add_physics, data=0.0)
        self._remove_phys_button = self.add_item("Remove physics", self._remove_physics)

    def _add_physics_to_editor(self, target: Object3D, input: LocalInputMethod) -> None:
        matrix_world = self.editor.local_physical_body.matrix_world
        matrix_world.translation = self.editor.matrix.translation - self.editor.physical_offset
        self.editor.local_physical_body.matrix_world = matrix_world
        self.editor.machine.client.session.action(AddModelAction(model=Model(root=self.editor.local_physical_body)))
        self.close()

    def _remove_physics_from_editor(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.action(RemoveObject3DAction(self.editor.local_physical_body))
        self.close()

    def _add_physics(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.action(AddBehaviorAction(
            object=self.editor.active_object, behavior=PhysicsBehavior(density=target.data)))
        for passive_object in self.editor.passive_objects:
            self.editor.machine.client.session.action(AddBehaviorAction(
                object=passive_object, behavior=PhysicsBehavior(density=target.data)))
        self.close()

    def _remove_physics(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.command(RemoveBehaviorByTypeCommand(
            object=self.editor.active_object, behavior_type='PhysicsBehavior'))
        for passive_object in self.editor.passive_objects:
            self.editor.machine.client.session.command(RemoveBehaviorByTypeCommand(
                object=passive_object, behavior_type='PhysicsBehavior'))
        self.close()

    def _test(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.command(AddEventHandlerCommand(key='collision_handler',
                                                                          behavior=self.editor.active_object.behaviors[0],
                                                                          event_handler=CollisionHandler(trigger_behavior='HighlightBehavior')))
        self.close()

    def _test_remove(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.command(RemoveEventHandlerCommand(key='collision_handler',
                                                                             behavior=self.editor.active_object.behaviors[0]))
        self.close()

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        behavior = None
        active_object = self.editor.active_object
        if active_object is not None:
            behavior = active_object.get_behavior_by_type(PhysicsBehavior)
        self.editor.machine.state_object.selection_lock = True
        is_active_object = active_object is not None
        is_physics_object = self.editor.synchronized_physical_body is not None
        self._add_editor_phys_button.enabled = not is_physics_object
        self._remove_editor_phys_button.enabled = is_physics_object
        self._active_phys_button.enabled = is_active_object and behavior is None
        self._passive_phys_button.enabled = is_active_object and behavior is None
        self._remove_phys_button.enabled = is_active_object and behavior is not None

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False


class AddKeyframeMenu(EditorRadialTimelineMenu):
    def __init__(self):
        super().__init__(name="Add Keyframe Menu")
        self._add_locrotscale_button = self.add_item("Location/Rotation/Scale", self._add_locrotscale_key)
        self._add_video_toggle_playback_button = self.add_item("Video playback", self._add_video_toggle_playback_key)
        self._back_button = self.add_item("Back", self._back)

    def _get_video_textures(self) -> List[TextureVideo]:
        active_object = self.editor.active_object
        if active_object is None or active_object.material is None or not active_object.material.textures:
            return []

        video_textures = []
        for material_texture in active_object.material.textures:
            if isinstance(material_texture.texture, TextureVideo):
                video_textures.append(material_texture.texture)

        return video_textures

    def _back(self, target: Object3D, input: LocalInputMethod) -> None:
        self._manager.open_menu(input, AnimatingMenu())

    def _add_locrotscale_key(self, target: Object3D, input: LocalInputMethod) -> None:
        active_object = self.editor.active_object or self.editor.camera
        self.editor.machine.client.session.action(AddKeyframeAction(
            object=active_object, attribute="location", time=self.editor.timeline.time, value=active_object.location))
        self.editor.machine.client.session.action(AddKeyframeAction(
            object=active_object, attribute="rotation", time=self.editor.timeline.time, value=active_object.rotation))
        self.editor.machine.client.session.action(AddKeyframeAction(
            object=active_object, attribute="scale", time=self.editor.timeline.time, value=active_object.scale))

    def _add_video_toggle_playback_key(self, target: Object3D, input: LocalInputMethod) -> None:
        video_textures = self._get_video_textures()
        for video_texture in video_textures:
            self.editor.machine.client.session.action(AddKeyframeAction(
                object=video_texture, attribute="video_playback", time=self.editor.timeline.time, value=video_texture.video_playback))

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        video_textures = self._get_video_textures()
        has_video = len(video_textures) > 0
        self._add_video_toggle_playback_button.enabled = has_video


class AnimatingMenu(EditorRadialTimelineMenu):
    def __init__(self):
        super().__init__(name="Animation Menu")

        def _add_keyframe(target: Object3D, input: LocalInputMethod) -> None:
            self._manager.open_menu(input, AddKeyframeMenu())

        self._add_locrotscale_button = self.add_item("Add keyframe", _add_keyframe)
        self._start_button = self.add_item("Start", self._start_timeline)
        self._stop_button = self.add_item("Stop", self._stop_timeline)
        self.add_item("Reset", self._reset_timeline)
        self._previous_button = self.add_item("Previous keyframe", self._previous_keyframe)
        self._next_button = self.add_item("Next keyframe", self._next_keyframe)
        self._loop_button = self.add_item("Toggle loop", self._loop_animation)
        self._delete_keyframe_button = self.add_item("Delete keyframe", self._delete_keyframe)
        self._auto_keyframe_button = self.add_item("Toggle recording", self._auto_keyframe)

    def _auto_keyframe(self, target: Object3D, input: LocalInputMethod) -> None:
        active_object = self.editor.active_object or self.editor.camera
        behavior = active_object.get_behavior_by_type(AutoKeyframeBehavior)

        if behavior is not None:
            active_object.remove_behavior(behavior)
        else:
            active_object.add_behavior(behavior=AutoKeyframeBehavior(
                attributes=["location", "rotation", "scale"]), synchronize=False)

    def _start_timeline(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.command(StartTimelineCommand())
        self.editor.timeline.start()

    def _stop_timeline(self, target: Object3D, input: LocalInputMethod) -> None:
        self.editor.machine.client.session.command(StopTimelineCommand())
        self.editor.timeline.stop()

    def _reset_timeline(self, target: Object3D, input: LocalInputMethod) -> None:
        active_object = self.editor.active_object or self.editor.camera
        if active_object is not None:
            behavior = active_object.get_behavior_by_type(AutoKeyframeBehavior)
            if behavior is not None:
                active_object.remove_behavior(behavior)
        self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=0.0))
        self.editor.timeline.reset()

    def _loop_animation(self, target: Object3D, input: LocalInputMethod) -> None:
        active_object = self.editor.active_object or self.editor.camera
        behavior = active_object.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            self.editor.machine.client.session.action(LoopAnimation(
                object=active_object, looping=not behavior.looping))

    def _delete_keyframe(self, target: Object3D, input: LocalInputMethod) -> None:
        active_object = self.editor.active_object or self.editor.camera
        behavior = active_object.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            keyframe_count = len(behavior.keyframe_times)
            self.editor.machine.client.session.action(RemoveKeyframeAction(
                object=active_object, time=behavior.selected_keyframe))
            if keyframe_count == 1:
                self.editor.machine.client.session.command(RemoveBehaviorByTypeCommand(
                    object=self.editor.active_object, behavior_type='AnimationBehavior'))

    def _next_keyframe(self, target: Object3D, input: LocalInputMethod) -> None:
        if self.editor.timeline.running:
            self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=0.0))
            self.editor.timeline.stop()
        active_object = self.editor.active_object or self.editor.camera
        behavior = active_object.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            nextTime = self.editor.config['timeline.duration']
            current_time = self.editor.timeline.time
            for this_time in behavior.keyframe_times:
                if this_time > current_time and this_time < nextTime:
                    nextTime = this_time
        self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=nextTime))

    def _previous_keyframe(self, target: Object3D, input: LocalInputMethod) -> None:
        if self.editor.timeline.running:
            self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=0.0))
            self.editor.timeline.stop()

        active_object = self.editor.active_object or self.editor.camera
        behavior = active_object.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            previousTime = 0.0
            current_time = self.editor.timeline.time
            for this_time in behavior.keyframe_times:
                if this_time < current_time and this_time > previousTime:
                    previousTime = this_time
        self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=previousTime))

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        self.editor.machine.state_object.selection_lock = True
        active_object = self.editor.active_object or self.editor.camera
        behavior = None
        if active_object is not None:
            behavior = active_object.get_behavior_by_type(AnimationBehavior)
        self._add_locrotscale_button.enabled = active_object is not None
        self._stop_button.enabled = self.editor.timeline.running
        self._start_button.enabled = not self.editor.timeline.running
        self._loop_button.enabled = active_object is not None and behavior is not None
        self._previous_button.enabled = active_object is not None and behavior is not None
        self._next_button.enabled = active_object is not None and behavior is not None
        self._auto_keyframe_button.enabled = active_object is not None

    def on_closed(self) -> None:
        super().on_closed()
        self.editor.machine.state_object.selection_lock = False
        self.editor.machine.state_object._move_timeline_enabled = False

    def update_menu_components(self) -> None:
        super().update_menu_components()

        if self._selected_keyframe is not None:
            self._delete_keyframe_button.enabled = True
        else:
            self._delete_keyframe_button.enabled = False

        if self.editor.timeline.running:
            self._start_button.enabled = False
            self._stop_button.enabled = True
        else:
            self._start_button.enabled = True
            self._stop_button.enabled = False

        active_object = self.editor.active_object or self.editor.camera
        behavior = active_object.get_behavior_by_type(AnimationBehavior) if active_object is not None else None
        buttons_enabled = active_object is not None and behavior is not None
        self._loop_button.enabled = buttons_enabled
        self._previous_button.enabled = buttons_enabled
        self._next_button.enabled = buttons_enabled


class EditingSceneState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="editing_scene", verb="edit_scene", recallable=True, **kwargs)

        self._selection_lock = False  # Whether the selection is enabled or not, True = disabled, False = enabled
        self._added_objects_layer: Optional[Object3D] = None

        # Timeline movement
        self._previous_timeline_movement_position = 0
        self._move_timeline_enabled = False
        self._timeline_movement = 0
        self._timeline_velocity = 0
        self._timeline_time = self.editor.timeline.time

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=True,
            highlight_on_hover=False,
            label="select",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        self.menu = lambda: EditingSceneMenu()

        # Quick transformations
        self._quick_translating = False
        self._quick_rotating = False
        self._quick_scaling = False

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("select", "_select", "Select an object"),
                                     ("alternative_select", "_alternative_select", "Select multiple objects (hold)"),
                                     ("quick_translate", "_quick_translate", "Translate (hold)"),
                                     ("quick_rotate", "_quick_rotate", "Rotate (hold)"),
                                     ("quick_scale", "_quick_scale", "Scale (hold)"),
                                     ("axis_z", "_navigate_timeline", "Navigate within the timeline"),
                                     ("undo", "_undo", "Undo"),
                                     ("redo", "_redo", "Redo"),
                                     ("toggle_timeline", "_toggle_timeline", "Toggle timeline")])

        self._highlighted_objects: Set[Object3D] = set()
        self._highlight_behavior_speed = 2.0

    @property
    def selection_lock(self) -> bool:
        return self._selection_lock

    # locks/unlock selection/hovering (will unhighlight hovered items if locking)
    @selection_lock.setter
    def selection_lock(self, value: bool) -> None:
        if self._selection_lock != value:
            self._selection_lock = value

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def enter(self, event: EventData) -> None:
        # Should happen before super to be available for the cursor in super()
        super().enter(event)
        self._selection_lock = False
        selection = [child for child in self.machine.editor.scene.model.root.children if child.name == "Added_objects_layer"]
        if selection:
            self._added_objects_layer = selection[0]
        self.only_pick_assets = not self.machine.editor.config['input.pick_in_scene']

    def exit(self, event: EventData) -> None:
        super().exit(event)

        for object in self._highlighted_objects:
            object.remove_behavior_by_type(HighlightBehavior)
        self._highlighted_objects.clear()

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        time_point = self._machine.editor.timeline.time
        time_string = "t={:.2f}".format(time_point)

        for user in self.editor.users.values():
            for input_method in user.input_methods.values():
                input_method.cursor.label = "t={:.2f}".format(time_point)
                if self.editor.active_object is not None:
                    input_method.cursor.label = f"selected - {time_string} - {self.editor.active_object.name}"
                else:
                    input_method.cursor.label = f"select - {time_string}"

        # Detect which object is pointed at
        newly_picked_object: Set[Object3D] = set()
        if not self._selection_lock:
            for user in self.editor.users.values():
                for input_method in user.input_methods.values():
                    if self.only_pick_assets:
                        result = input_method.picker.pick_in(self._added_objects_layer, True)
                    else:
                        result = input_method.picker.pick()
                    if result:
                        picked_object, position = result
                        if picked_object is not None:
                            picked_name = picked_object.name
                            if len(picked_name) > 32:
                                picked_name = f"{picked_name[:15]}...{picked_name[-15:]}"
                            input_method.cursor.label = f"select - {time_string} - {picked_name}"

                            if picked_object is not self.editor.active_object \
                                    and picked_object not in self.editor.passive_objects:
                                newly_picked_object.add(picked_object)

        # Highlight objects pointed at, if they are not already selected
        for object in self._highlighted_objects:
            if object not in newly_picked_object:
                object.remove_behavior_by_type(HighlightBehavior)
        for object in newly_picked_object:
            if object not in self._highlighted_objects:
                object.add_behavior(HighlightBehavior(speed=self._highlight_behavior_speed))
        self._highlighted_objects = newly_picked_object

        # Movement
        if self._move_timeline_enabled:
            # Move the timeline cursor according to the velocity
            self._timeline_time += self._timeline_movement
            self.editor.machine.client.session.command(ResetTimelineCommand(timeline_position=self._timeline_time))
            self._timeline_movement = 0

    def _quick_translate(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if self.editor.active_object is not None and not self.editor.menu_manager.menu_open:
            if args[1]:
                self._machine.translate(input=args[0], quick=True)

    def _quick_rotate(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if self.editor.active_object is not None and not self.editor.menu_manager.menu_open:
            if args[1]:
                self._machine.rotate(input=args[0], quick=True)

    def _quick_scale(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if self.editor.active_object is not None and not self.editor.menu_manager.menu_open:
            if args[1]:
                self._machine.scale(input=args[0], quick=True)

    def _select(self, args: Tuple[LocalInputMethod, bool]) -> None:
        """
        Select a single object. If a multiple selection was already active, unselect everything
        to select only the targeted object.
        """
        if not self._selection_lock and args[0].picker is not None and args[1]:  # if pressed
            if self.only_pick_assets:
                result = args[0].picker.pick_in(self._added_objects_layer, True)
            else:
                result = args[0].picker.pick()
            if result is not None:
                active_object = self.editor.active_object
                passive_objects = self.editor.passive_objects
                object3D = result[0]

                if object3D in self._highlighted_objects:
                    self._highlighted_objects.remove(object3D)

                if active_object is object3D:
                    if passive_objects:
                        self.editor.clear_passive_objects()
                    else:
                        self.editor.active_object = None
                else:
                    object3D.remove_behavior_by_type(HighlightBehavior)
                    self.editor.clear_passive_objects()
                    self.editor.active_object = object3D

    def _alternative_select(self, args: Tuple[LocalInputMethod, bool]) -> None:
        """
        Selects multiple objects through the editor.active_object and editor.passive_objects properties
        """
        if not self._selection_lock and args[1]:  # if pressed
            if self.only_pick_assets:
                result = args[0].picker.pick_in(self._added_objects_layer, True)
            else:
                result = args[0].picker.pick()
            if result is not None:
                active_object = self.editor.active_object
                passive_objects = self.editor.passive_objects
                object3D = result[0]

                if object3D in self._highlighted_objects:
                    object3D.remove_behavior_by_type(HighlightBehavior)
                    self._highlighted_objects.remove(object3D)

                # Object is already active object
                if active_object is object3D:
                    if passive_objects:
                        new_active_object = passive_objects[0]
                        self.editor.remove_passive_object(new_active_object)
                        self.editor.active_object = new_active_object
                    else:
                        self.editor.active_object = None
                # Object isn't active object, but exists
                elif active_object is not None:
                    # Object isn't a passive object
                    if object3D not in passive_objects:
                        self.editor.add_passive_object(object3D)
                    # Ojbect is a passive object
                    else:
                        self.editor.remove_passive_object(object3D)
                        self.editor.add_passive_object(active_object)
                        self.editor.active_object = object3D
                # There is no active object
                else:
                    self.editor.active_object = object3D

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _navigate_timeline(self, args: Tuple[LocalInputMethod, float, int]) -> None:
        if args[2] and self.editor.menu_manager.menu_open:
            if self._move_timeline_enabled:
                if args[2] == 4:
                    self._move_timeline_enabled = False
                else:
                    # This is because the trackpads records 0.0 values from time to time for no good reason whatsoever
                    # It notably happens the last value before disabling, and so we have to ignore them.
                    if args[1] != 0.0:
                        self._timeline_movement = (args[1] - self._previous_timeline_movement_position) * 5.0
                        self._previous_timeline_movement_position = args[1]

            elif args[2] == 3:
                self._move_timeline_enabled = True
                self._timeline_time = self.editor.timeline.time
                self._timeline_velocity = 0
                self._previous_timeline_movement_position = args[1]

    def _toggle_timeline(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if not self.editor.menu_manager.menu_open:
            if args[1]:
                if self.editor.timeline.running:
                    self.editor.machine.client.session.command(StopTimelineCommand())
                    self.editor.timeline.stop()
                else:
                    self.editor.machine.client.session.command(StartTimelineCommand())
                    self.editor.timeline.start()

    def _undo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(UndoCommand())

    def _redo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(RedoCommand())
