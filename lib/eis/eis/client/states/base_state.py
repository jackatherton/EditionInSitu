from typing import Any, Optional, TYPE_CHECKING

from transitions.core import EventData  # type: ignore
from transitions.extensions.nesting import NestedState as State  # type: ignore

from eis.constants import DEBUG_STATES
from satlib.utils.logger import Logger

if TYPE_CHECKING:
    from eis.client.state_machine import StateMachine


class BaseState(State):  # type: ignore
    def __init__(self, *args: Any, verb: Optional[str] = None, machine: 'StateMachine', trigger: Optional[str] = None, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        if DEBUG_STATES:
            self._logger = Logger(self)

        self._machine = machine

        # Original name given to the state (before the machine prepends nested state's parent's name)
        self._original_name = kwargs['name']  # type: str
        # Label/Verb describing the state
        self._verb = verb if verb is not None else self.name  # type: str
        # Custom trigger for the state
        self._trigger = trigger if trigger is not None else self.verb

        self._cancel_nested_run = True

    @property
    def machine(self) -> 'StateMachine':
        return self._machine

    @property
    def original_name(self) -> str:
        return self._original_name

    @property
    def verb(self) -> str:
        return self._verb

    @property
    def trigger(self) -> str:
        return self._trigger

    # region State

    def should_enter(self, event: EventData) -> bool:
        """
        State method that can be used as a transition condition
        Note that you'll still have to specify it in the transition, this is only
        provided as a helper for custom scenarios
        :return: bool
        """
        return True

    def enter(self, event: EventData) -> None:
        super().enter(event)
        if DEBUG_STATES:
            self._logger.log('Enter', self.name)

        self._cancel_nested_run = False

    def exit(self, event: EventData) -> None:
        super().exit(event)
        if DEBUG_STATES:
            self._logger.log("Exit", self.name)

        self._cancel_nested_run = True

    def always_run_before(self, now: float, dt: float) -> None:
        """
        Always run (before the run() method), even when showing a modal
        :param now: float
        :param dt: float
        :return:
        """

        pass

    def run(self, now: float, dt: float) -> None:
        """
        Run the state
        Only executed when not showing a modal
        :param now: float
        :param dt: float
        :return:
        """

        pass

    def always_run_after(self, now: float, dt: float) -> None:
        """
        Always run (after the run() method), even when showing a modal
        :param now: float
        :param dt: float
        :return:
        """

        pass

    def run_nested(self, modal: bool, now: float, dt: float) -> None:
        """
        Run the nested state by first running its parent then self
        :param modal: bool
        :param now: float
        :param dt: float
        :return:
         """

        if DEBUG_STATES:
            self._logger.open("Run nested")

        if self.parent is not None:
            self.parent.run_nested(modal=modal, now=now, dt=dt)

        if self._cancel_nested_run:
            if DEBUG_STATES:
                self._logger.close("canceled")
            return

        if DEBUG_STATES:
            self._logger.open("always run (before)")

        self.always_run_before(now=now, dt=dt)

        if DEBUG_STATES:
            self._logger.close()

        if not modal:
            if DEBUG_STATES:
                self._logger.open("run")

            self.run(now=now, dt=dt)

            if DEBUG_STATES:
                self._logger.close()

        if DEBUG_STATES:
            self._logger.open("always run (after)")

        self.always_run_after(now=now, dt=dt)

        if DEBUG_STATES:
            self._logger.close()
            self._logger.close()

    def execute(self, now: float, dt: float, modal: bool = False) -> None:
        self.run_nested(modal=modal, now=now, dt=dt)

    # endregion
