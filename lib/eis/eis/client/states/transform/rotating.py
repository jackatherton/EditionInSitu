import math
from typing import Any

from transitions.core import EventData

from eis.actions.transform_object import TransformObjectAction
from eis.client.input import LocalInputMethod
from eis.client.states.transform import BaseTransformState
from eis.commands.transform_offset import TransformOffsetCommand
from eis.graph.helpers.rotation import RotationHelper
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class RotatingState(BaseTransformState):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="rotating", **kwargs)
        self._helper = RotationHelper()

        # Controller initial direction
        self._input_initial_direction = Vector3()

        # Positions and direction
        self._active_object_position = Vector3()
        self._rotation_matrix = Matrix44.identity()

    def enter(self, event: EventData) -> None:
        super().enter(event)

        if self.editor.active_object is None:
            return

        self._active_object_position = self.editor.active_object.matrix_world.translation
        self._input_initial_direction = self._input_method.secondary_orientation

    def _set_mapping(self) -> None:
        super()._set_mapping()
        if self._is_quick:
            self._inputs_mapping += [("quick_rotate", "_release", "Release to stop transforming")]

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)

        # We want to remove the inital direction rotation from the current direction
        # -> multiply current by the inverse of the initial direction
        diff_pose = self._input_method.secondary_orientation * self._input_initial_direction.inverse
        object_direction = (self._active_object_position - self.editor.matrix.translation).quaternion()
        self._rotation_matrix = Matrix44.from_quaternion(object_direction * diff_pose * object_direction.inverse)
        self._apply_rotation(self.editor.active_object)
        for passive_object in self.editor.passive_objects:
            self._apply_rotation(passive_object)

    def exit(self, event: EventData) -> None:
        super().exit(event)

        active_translation_matrix = Matrix44.from_translation(self.editor.active_object.matrix.translation)

        transformation_matrix = active_translation_matrix * self._rotation_matrix * \
            active_translation_matrix.inverse * self.editor.active_object.matrix
        self._machine.client.session.action(TransformObjectAction(
            object=self.editor.active_object, matrix=transformation_matrix))

        for passive_object in self.editor.passive_objects:
            passive_translation_matrix = Matrix44.from_translation(passive_object.matrix.translation)
            transformation_matrix = passive_translation_matrix * self._rotation_matrix * \
                passive_translation_matrix.inverse * passive_object.matrix
            self._machine.client.session.action(TransformObjectAction(
                object=passive_object, matrix=transformation_matrix))

    def _apply_rotation(self, object_3d: Object3D) -> None:
        transformation_matrix = Matrix44.from_translation(object_3d.matrix.translation) * self._rotation_matrix * Matrix44.from_translation(object_3d.matrix.translation).inverse
        object_3d.matrix_offset = transformation_matrix
        self._machine.client.session.command(TransformOffsetCommand(object=object_3d, matrix=transformation_matrix))
