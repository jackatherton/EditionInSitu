from typing import Any, Optional, TYPE_CHECKING, Callable

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.graph.helpers import AxisHelper

if TYPE_CHECKING:
    pass


class BaseTransformState(BaseEISState):
    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._on_exit = on_exit
        self._input_method: Optional[LocalInputMethod] = None
        self._helper: Optional[AxisHelper] = None

        # Redefine in each transformation to a string
        self._transformation = None

        # Whether this is a quick transformation or not
        self._is_quick = False

        # Cursor
        self._cursor = lambda: Cursor(
            radius=0.3,
            color=(0.15, 0.65, 0.95, 1.00),
        )

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected and self.editor.active_object is not None
        return should and can

    def enter(self, event: EventData) -> None:
        self._input_method = event.kwargs.get('input')
        self._is_quick = event.kwargs.get('quick')

        # Call this before calling super because the new mapping is subscribed in the super
        self._set_mapping()
        super().enter(event)

        # Helper
        if self._helper:
            self.editor.helpers.add_child(self._helper)

    # There is a set_mapping for the transformations because the mapping will change if it's a quick transfo.
    def _set_mapping(self) -> None:
        self._inputs_mapping = [("help", "_help", None)]

        # Must had the quick equivalent to every transformation
        if not self._is_quick:
            self._inputs_mapping += [("select", "_release", "Release to stop transforming")]

    def _back(self) -> None:
        self._on_exit()

    def _release(self, input: Any) -> None:
        if input[0] is self._input_method and not input[1]:
            self._back()

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self._input_method = None

        # Helper
        if self._helper:
            self.editor.helpers.remove_child(self._helper)

    def always_run_after(self, now: float, dt: float) -> None:
        super().always_run_after(now=now, dt=dt)

        """
        UI HELPERS
        """
        if self._helper is not None:
            self._helper.matrix_world = self.editor.active_object.matrix_world
            diff_matrix = self._helper.matrix * self.editor.active_object.matrix.inverse

            # TODO: Adapt the helper to the other transformations as well
            if self._machine.state_object.name == "translating":
                self._helper.matrix_offset = diff_matrix * self.editor.active_object.matrix_offset * diff_matrix.inverse
