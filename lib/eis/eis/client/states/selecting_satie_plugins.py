from eis.client.states.selecting_assets import SelectingAssetsState
from eis.editor_support.satie_manager import SatieManager
from transitions.core import EventData  # type: ignore
from typing import Any, Callable, Optional


class SelectingSatiePluginsState(SelectingAssetsState):
    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._on_exit = on_exit
        self._name = "selecting_satie_plugins"

    def enter(self, event: EventData) -> None:
        self.machine.editor.asset_manager.satie_plugins = SatieManager().satie.plugins
        super().enter(event)

    def exit(self, event: EventData) -> None:
        self.machine.editor.asset_manager.deactivate()
        super().exit(event)
