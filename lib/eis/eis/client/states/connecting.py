from typing import Any

from transitions.core import EventData  # type: ignore

from eis.client.states.base_eis_state import BaseEISState


class ConnectingState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="connecting", **kwargs)

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = True  # TODO: Check if we have a configuration for the connection
        return should and can

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self.editor.engine.show_status("CONNECTING")

        self.client.connect()

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.engine.hide_status()

    def run(self, now: float, dt: float) -> None:
        super().run(now=now, dt=dt)
        if self.client.connected:
            self.machine.initialize()
