import os


class EISPath:
    eis_path = os.path.join(os.path.expanduser('~'), '.eis')
    scenes_path = os.path.join(eis_path, 'scenes')
    export_path = os.path.join(eis_path, 'export')

    @classmethod
    def get_scenes_path(cls) -> str:
        if not os.path.exists(cls.scenes_path):
            os.makedirs(cls.scenes_path, exist_ok=True)
        return cls.scenes_path

    @classmethod
    def get_export_path(cls) -> str:
        if not os.path.exists(cls.export_path):
            os.makedirs(cls.export_path, exist_ok=True)
        return cls.export_path
