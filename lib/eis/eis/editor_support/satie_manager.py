import atexit
from pysatie.satie import Satie
from typing import Any, Dict
from eis.singleton import Singleton


class SatieManager(metaclass=Singleton):
    def __init__(self, config: Dict[str, Any]) -> None:

        self._config = config

        self._server = self._config.get("satie.server", "localhost")
        self._destination_port = self._config.get("satie.destination_port", 18032)
        self._server_port = self._config.get("satie.server_port", 12000)
        self._renderer_orientation_offset = tuple(
            [i for i in self._config.get("satie.renderer_orientation_offset", [0, 0])])
        self._satie = Satie()
        self.satie.server_port = self._server_port
        atexit.register(self.clear_scene)

        self._satie.destination = self._server
        self._satie.destination_port = self._destination_port
        self._satie.initialize()

        # tell SATIE to send to us
        self._satie.set_renderer_orientation(*self._renderer_orientation_offset)
        self._satie.audiosources()  # get a list of available plugins

    def clear_scene(self) -> None:
        self._satie.clear_scene()

    @property
    def satie(self) -> Satie:
        return self._satie
