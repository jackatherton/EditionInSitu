import logging
import os
import sys
import time

from eis.singleton import Singleton
from pyquid import InfoTree, Qrox, Quiddity, Switcher
from pyshmdata import Reader
from typing import Any, Callable, Dict, List, Optional

sys.path.append('/usr/local/lib/python3/dist-packages')

logger = logging.getLogger(__name__)


def set_quid_property(quid: Quiddity, prop: str, value: Any) -> bool:
    """
    Helper function to handle errors when setting a quiddity's property

    :param quid: Quiddity to call the 'set' method on
    :param prop: Quiddity property to set
    :param value: Value to set the attribute to
    :return: Return True if the property was set successfully
    """
    if not quid.set(prop, value):
        logger.warning(f"Error when setting property {prop} of quiddity {quid.getName()} to {value}")
        return False
    return True


def invoke_quid_method(quid: Quiddity, method: str, args: List[Any]) -> bool:
    """
    Helper function to handle errors when calling 'invoke' on a Quiddity

    :param quid: Quiddity to call the 'set' method on
    :param method: Quiddity method to invoke
    :param args: Arguments to pass to the invoked method
    :return: Return True if the method was invoked successfully
    """
    if not quid.invoke(method, args):
        logger.warning(f"Error when invoking method {method} of quiddity {quid.getName()} with arguments {args}")
        return False
    return True


class SwitcherManager(metaclass=Singleton):
    """
    Manages different switcher assets, e.g. videos. Each asset has a unique handle.
    """

    JACK_DEFAULT_SERVER_NAME = "Jack_in_EiS"
    JACK_DEFAULT_DRIVER = "alsa"
    JACK_DEFAULT_DEVICE = 0
    JACK_DEFAULT_SAMPLERATE = 48000

    def __init__(
        self,
        config: Dict[str, Any] = {}
    ) -> None:
        self._jack_create_server = config.get("switcher.jack.create_server", True)
        if self._jack_create_server:
            self._jack_server_name = config.get("switcher.jack.server_name", SwitcherManager.JACK_DEFAULT_SERVER_NAME)
        else:
            self._jack_server_name = ""
        self._jack_driver = config.get("switcher.jack.driver", SwitcherManager.JACK_DEFAULT_DRIVER)
        self._jack_device = config.get("switcher.jack.device", SwitcherManager.JACK_DEFAULT_DEVICE)
        self._jack_samplerate = config.get("switcher.jack.samplerate", SwitcherManager.JACK_DEFAULT_SAMPLERATE)

        self._sw = Switcher(name="pyquid")
        self._list_videos: Dict[int, Video] = {}
        self._list_audios: Dict[int, Audio] = {}
        self._qrox_jack_sink: Optional[Qrox] = None
        self._quid_jack_sink: Optional[Qrox] = None
        if self._jack_create_server:
            jack_config = f"{{\"name\" : \"{self._jack_server_name}\", \
                            \"driver\" : \"{self._jack_driver}\", \
                            \"device\" : \"{self._jack_device}\", \
                            \"rate\" : \"{self._jack_samplerate}\"}}"
            self._qrox_jackserver = self._sw.create("jackserver", config=InfoTree(jack_config))
            if self._qrox_jackserver is None:
                logger.warning(f"Unable to start a Jack server with the following configuration: \n {jack_config}")
            else:
                self._quid_jackserver = self._qrox_jackserver.quid()
                set_quid_property(self._quid_jackserver, "started", True)

        # The handle is a handle, but it also corresponds to the linked supercollider channel for the audio
        # False means this handle (that corresponds to the index) is free, True means it is taken
        self._free_handles: List[bool] = [False] * 256

    @property
    def jack_server_name(self) -> str:
        return self._jack_server_name

    def new_video(self, video_uri: str) -> Optional[int]:
        """
        Creates a new video asset
        :param video_uri: Video uri
        :return: Return a handle for the created video, or None if it failed
        """
        # Check whether the video file exists
        if not os.path.exists(video_uri):
            return None

        # Generate a handle - will be the lowest handle number that's currently free.
        this_handle = 0
        for idx, handle in enumerate(self._free_handles):
            if handle is False:
                # +3 so that the "lowest" supercollider input channel (in jack) you can connect to is 3, leaving the first two for the regular input
                this_handle = idx + 3
                self._free_handles[idx] = True
                break

        new_video = Video(this_handle)
        new_video.create_video(video_uri, self._sw)
        self._list_videos[this_handle] = new_video

        return new_video.handle

    def toggle_video_playback(self, handle: int, play: bool) -> None:
        """
        Set the playback state for the video with the given handle
        :param handle: Video handle
        :param play: Playback state, True for playing and False for pausing
        """
        assert(handle in self._list_videos)
        self._list_videos[handle].toggle_playback(play)

    def is_video_playing(self, handle: int) -> bool:
        """
        Get whether the video with the given handle is playing
        :param handle: Video handle
        :return: True if the video is playing, False otherwise
        """
        assert(handle in self._list_videos)
        return self._list_videos[handle].is_playing()

    def close_video(self, handle: int) -> bool:
        """
        Deletes the video asset that corresponds to the given handle (should unlink before closing)
        :param handle: Video handle
        :return: True if the deletion has succeeded
        """
        if handle in self._list_videos:
            self._list_videos[handle].delete_video(self._sw)
            self._list_videos.pop(handle)
            self._free_handles[handle - 3] = False
            return True
        return False

    def link_video(self, handle: int, callback: Callable[[Any, int, int], None]) -> None:
        """
        Links the designated video to a callback function
        :param handle: Video handle
        :param callback: Callback function, taking (data, height, width) as parameters
        """
        assert(handle in self._list_videos)
        self._list_videos[handle].read_video(callback)

    def unlink_video(self, handle: int) -> None:
        """
        Stop linking the video to the callback function, doesn't delete the video.
        :param handle: Video handle
        """
        assert(handle in self._list_videos)
        self._list_videos[handle].stop_reading_video()

    def new_audio(self, audio_uri: str) -> int:  # the return int is the handle
        """
        Creates a new audio asset
        :param video_uri: Audio uri
        :return: Return a handle for the created audio, or None if it failed
        """
        # Generate a handle - will be the lowest handle number that's currently free.
        this_handle = 0
        for idx, handle in enumerate(self._free_handles):
            if handle is False:
                # +3 so that the "lowest" supercollider input channel (in jack) you can connect to is 3, leaving the first two for the regular input
                this_handle = idx + 3
                self._free_handles[idx] = True
                break

        new_audio = Audio(this_handle)
        new_audio.create_audio(audio_uri, self._sw)
        self._list_audios[this_handle] = new_audio
        return new_audio.handle

    def toggle_audio_playback(self, handle: int, play: bool) -> None:
        """
        Set the playback state for the audio with the given handle
        :param handle: Audio handle
        :param play: Playback state, True for playing and False for pausing
        """
        assert(handle in self._list_audios)
        self._list_audios[handle].toggle_playback(play)

    def is_audio_playing(self, handle: int) -> bool:
        """
        Get whether the audio with the given handle is playing
        :param handle: Audio handle
        :return: True if the audio is playing, False otherwise
        """
        assert(handle in self._list_audios)
        return self._list_audios[handle].is_playing()

    def close_audio(self, handle: int) -> bool:
        """
        Deletes the audio asset that corresponds to the given handle (should unlink before closing)
        :param handle: Audio handle
        :return: True if the deletion has succeeded
        """
        if handle in self._list_audios:
            self._list_audios[handle].delete_audio(self._sw)
            self._list_audios.pop(handle)
            self._free_handles[handle - 3] = False
            return True
        return False

    def link_audio(self, handle: int) -> None:
        """
        Links the designated audio
        :param handle: Audio handle
        """
        assert(handle in self._list_audios)
        self._list_audios[handle].read_audio()

    def unlink_audio(self, handle: int) -> None:
        """
        Stop linking the designated audio
        :param handle: Audio handle
        """
        assert(handle in self._list_audios)
        self._list_audios[handle].stop_reading_audio()


# Single video that can be used as an asset for the SwitcherManager.
class Video:
    def __init__(self, handle: int) -> None:
        self._handle = handle
        self._qrox_video_reader: Optional[Qrox] = None
        self._quid_video_reader: Optional[Qrox] = None
        self._qrox_video_converter: Optional[Qrox] = None
        self._quid_video_converter: Optional[Qrox] = None
        self._qrox_jack_sink: Optional[Qrox] = None
        self._quid_jack_sink: Optional[Qrox] = None
        self._reader: Optional[Reader] = None
        self._first_frame = False  # Whether the first frame has been received

    def create_video(self, video_path: str, sw: Switcher) -> None:
        """
        Creates necessary quiddities to read the video and convert it to bgra
        :param video_path: Video path
        :param sw: Switcher object
        """
        # create uri reader quid
        self._qrox_video_reader = sw.create(type="filesrc", name="urivideo_" + str(self._handle))
        self._quid_video_reader = self._qrox_video_reader.quid()

        # create video convert quid
        self._qrox_video_converter = sw.create(type="videoconvert", name="BGRAvideo_" + str(self._handle))
        self._quid_video_converter = self._qrox_video_converter.quid()

        # create jacksink quid for the audio portion
        self._qrox_jack_sink = sw.create(type="jacksink", config=InfoTree(
            f"{{\"name\" : \"jacksink_{self._handle}\", \
                \"server_name\" : \"{SwitcherManager().jack_server_name}\"}}"
        ))

        self._quid_jack_sink = self._qrox_jack_sink.quid()
        if not set_quid_property(self._quid_jack_sink, "connect_all_to_first", True):
            return
        if not set_quid_property(self._quid_jack_sink, "connect_to", "SuperCollider:in_%d"):
            return
        if not set_quid_property(self._quid_jack_sink, "auto_connect", True):
            return
        if not set_quid_property(self._quid_jack_sink, "index", self._handle):
            return

        # set properties for quiddities
        if not set_quid_property(self._quid_video_reader, "location", video_path):
            return
        # the sleep is needed due to an issue in Switcher, see issue SWIT-806
        time.sleep(0.1)
        if not set_quid_property(self._quid_video_reader, "loop", True):
            return
        if not set_quid_property(self._quid_video_reader, "play", True):
            return

        if not set_quid_property(self._quid_video_converter, "pixel_format", "BGRA"):
            return
        if not invoke_quid_method(self._quid_video_converter, "connect", [self._quid_video_reader.make_shmpath("video")]):
            return

        invoke_quid_method(self._quid_jack_sink, "connect", [self._quid_video_reader.make_shmpath("audio")])

    def toggle_playback(self, play: bool) -> None:
        """
        Pause video or start it (after it has been paused)
        :param play: Specify True to start the video, False to pause it
        """
        assert(self._quid_video_reader is not None)
        set_quid_property(self._quid_video_reader, "play", play)

    def is_playing(self) -> bool:
        """
        Get whether the video is started
        :return: True if the video is started
        """
        assert(self._quid_video_reader is not None)
        return self._quid_video_reader.get("play")

    def read_video(self, callback: Callable[[Any, int, int], None]) -> None:
        """
        Connect the shmreader to a callback function.
        :param callback: Callback function, taking (data, height, width) as parameters
        """
        # Shmdata's callback. It calls texture_video's callback.
        def cb(user_data: Any, data: Any, parameters: str, dict_parameters: Dict[str, Any]) -> None:
            callback(data, dict_parameters['height'], dict_parameters['width'])
            if not self._first_frame:
                self._first_frame = True

        if self._quid_video_converter is not None and self._quid_video_reader is not None:
            if not set_quid_property(self._quid_video_reader, "play", True):
                return
            self._reader = Reader(self._quid_video_converter.make_shmpath("video"), callback=cb, drop_frames=True)

            # waits for first frame before continuing
            while(not self._first_frame):
                time.sleep(0.05)

    def stop_reading_video(self) -> None:
        """
        Disconnect the shmreader
        """
        self._reader = None
        self._first_frame = False

    def delete_video(self, sw: Switcher) -> None:
        """
        Delete the video
        :param sw: Switcher object
        """
        assert(self._quid_video_converter is not None)
        assert(self._quid_video_reader is not None)
        assert(self._quid_jack_sink is not None)
        assert(self._qrox_video_reader is not None)
        assert(self._qrox_video_converter is not None)
        assert(self._qrox_jack_sink is not None)

        invoke_quid_method(self._quid_video_converter, "disconnect-all",
                           [self._quid_video_reader.make_shmpath("video")])
        invoke_quid_method(self._quid_jack_sink, "disconnect-all", [self._quid_video_reader.make_shmpath("audio")])
        sw.remove(self._qrox_video_reader.id())
        sw.remove(self._qrox_video_converter.id())
        sw.remove(self._qrox_jack_sink.id())
        self._first_frame = False

    @property
    def handle(self) -> int:
        return self._handle


# Single audio that can be used as an asset for the SwitcherManager.
class Audio:
    def __init__(self, handle: int) -> None:
        self._handle = handle
        self._qrox_audio_reader: Optional[Qrox] = None
        self._quid_audio_reader: Optional[Qrox] = None
        self._qrox_jack_sink: Optional[Qrox] = None
        self._quid_jack_sink: Optional[Qrox] = None
        self._reader: Optional[Reader] = None

    def create_audio(self, audio_path: str, sw: Switcher) -> None:
        """
        Creates necessary quiddities to read the audio and convert it to bgra
        :param audio_path: Path to the audio file
        :param sw: Switcher object
        """
        # create uri reader quid
        self._qrox_audio_reader = sw.create(type="filesrc", name="uriaudio_" + str(self._handle))
        self._quid_audio_reader = self._qrox_audio_reader.quid()

        # create jacksink quid for the audio portion
        self._qrox_jack_sink = sw.create(type="jacksink", config=InfoTree(
            f"{{\"name\" : \"jacksink_{self._handle}\", \
                \"server_name\" : \"{SwitcherManager().jack_server_name}\"}}"
        ))

        self._quid_jack_sink = self._qrox_jack_sink.quid()
        if not set_quid_property(self._quid_jack_sink, "connect_all_to_first", True):
            return
        if not set_quid_property(self._quid_jack_sink, "connect_to", "SuperCollider:in_%d"):
            return
        if not set_quid_property(self._quid_jack_sink, "auto_connect", True):
            return
        if not set_quid_property(self._quid_jack_sink, "index", self._handle):
            return

        # set properties for quiddities
        if not set_quid_property(self._quid_audio_reader, "location", audio_path):
            return
        if not set_quid_property(self._quid_audio_reader, "loop", True):
            return
        invoke_quid_method(self._quid_jack_sink, "connect", [self._quid_audio_reader.make_shmpath("audio")])

    def toggle_playback(self, play: bool) -> None:
        """
        Pause audio or start it (after it has been paused)
        :param play: Specify True to start the video, False to pause it
        """
        assert(self._quid_audio_reader is not None)

        if play:
            set_quid_property(self._quid_audio_reader, "play", True)
        else:
            set_quid_property(self._quid_audio_reader, "play", False)

    def is_playing(self) -> bool:
        """
        Get whether the audio is started
        :return: True if the audio is started
        """
        assert(self._quid_audio_reader is not None)
        return self._quid_audio_reader.get("play")

    def read_audio(self) -> None:
        """
        Read the audio
        """
        assert(self._quid_audio_reader is not None)
        set_quid_property(self._quid_audio_reader, "play", True)

    def stop_reading_audio(self) -> None:
        """
        Stop reading the audio
        """
        self._reader = None

    def delete_audio(self, sw: Switcher) -> None:
        """
        Delete the audio
        :param sw: Switcher object
        """
        assert(self._quid_audio_reader is not None)
        assert(self._quid_jack_sink is not None)
        assert(self._qrox_audio_reader is not None)
        assert(self._qrox_jack_sink is not None)

        invoke_quid_method(self._quid_jack_sink, "disconnect-all", [self._quid_audio_reader.make_shmpath("audio")])
        sw.remove(self._qrox_audio_reader.id())
        sw.remove(self._qrox_jack_sink.id())

    @property
    def handle(self) -> int:
        return self._handle
