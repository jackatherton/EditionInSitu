import math
from typing import Any, Callable

from eis.client.input import LocalInputMethod
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from eis.graph.primitives.ring import Ring
#from eis.graph.primitives.shapes.ring import Ring as RingShape
from eis.graph.text import Text
from eis.utils.color import Colors
from satmath.euler import Euler
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class RadialButton(Object3D):
    segments_per_radian = 12

    def __init__(
            self,
            label: str = "",
            toggle: bool = False,
            data: Any = None,
            on_hover: Callable[[Object3D, LocalInputMethod], None] = None,
            on_click: Callable[[Object3D, LocalInputMethod], None] = None,
            index: int = 0,
            total: int = 1,
            inner_radius: float = 1.00,
            outer_radius: float = 3.00
    ) -> None:
        super().__init__()

        self._interactive = True  # Important!

        self._data = data
        self._on_hover = on_hover
        self._on_click = on_click
        self._index = index
        self._total = total
        self._inner_radius = inner_radius
        self._outer_radius = outer_radius

        # self._animate_opening = False
        # self._animate_opening_time = None
        # self._animate_closing = False
        # self._animate_closing_time = None
        # self._close_callback = None

        self._toggle = toggle
        self._selected = False
        self._highlighted = False
        self._enabled = True

        self._background = Ring(material=Material())
        self.add_child(self._background)

        # pickable invisible background with a much farther outer radius than the buttons
        self._invisible_background = Ring(material=Material())
        self._invisible_background.visible = False
        self._background.add_child(self._invisible_background)

        # Commented the stroke without removing the code - Strokes are disabled for the time being
        # They contribute to the slow load times of the menus, and "glitch" because of the overlap with the background
        # Keeping the code because it might be fixable eventually, maybe using the cache (?)

        # self._stroke = RingShape(width=2, color=(1.00, 1.00, 1.00, 1.0))
        # self.add_child(self._stroke)

        self._text_field = Text(
            text=label,
            font="SourceSansPro-Bold.ttf",
            text_color=(1.00, 1.00, 1.00, 1.00),
            text_scale=0.25,
            align=Text.Align.CENTER
        )
        self._text_field.x_rotation = -math.pi / 2.0
        self.add_child(self._text_field)

    # region Properties

    @property
    def text_field(self) -> Text:
        return self._text_field.text

    @text_field.setter
    def text_field(self, value: str) -> None:
        self.remove_child(self._text_field)
        self._text_field = Text(
            text=value,
            font="SourceSansPro-Bold.ttf",
            text_color=(1.00, 1.00, 1.00, 1.00),
            text_scale=0.25,
            align=Text.Align.CENTER
        )
        self._text_field.x_rotation = -math.pi / 2.0
        self.add_child(self._text_field)

    @property
    def toggle(self) -> bool:
        return self._toggle

    @toggle.setter
    def toggle(self, value) -> None:
        if self._toggle != value:
            self._toggle = value
            self.invalidate()

    @property
    def selected(self) -> bool:
        return self._selected

    @selected.setter
    def selected(self, value) -> None:
        if self._selected != value:
            self._selected = value
            self.invalidate()

    @property
    def highlighted(self) -> bool:
        return self._highlighted

    @highlighted.setter
    def highlighted(self, value) -> None:
        if self._highlighted != value:
            self._highlighted = value
            self.invalidate()

    @property
    def enabled(self) -> bool:
        return self._enabled

    @enabled.setter
    def enabled(self, value) -> None:
        if self._enabled != value:
            self._enabled = value
            self.invalidate()

    @property
    def data(self) -> Any:
        return self._data

    @data.setter
    def data(self, value) -> None:
        if self._data != value:
            self._data = value

    @property
    def index(self) -> int:
        return self._index

    @index.setter
    def index(self, value) -> None:
        if self._index != value:
            self._index = value
            self.invalidate()

    @property
    def total(self) -> int:
        return self._total

    @total.setter
    def total(self, value) -> None:
        if self._total != value:
            self._total = value
            self.invalidate()

    @property
    def inner_radius(self) -> float:
        return self._inner_radius

    @inner_radius.setter
    def inner_radius(self, value) -> None:
        if self._inner_radius != value:
            self._inner_radius = value
            self.invalidate()

    @property
    def outer_radius(self) -> float:
        return self._outer_radius

    @outer_radius.setter
    def outer_radius(self, value) -> None:
        if self._outer_radius != value:
            self._outer_radius = value
            self.invalidate()

    def do_action(self, input: LocalInputMethod) -> None:
        if not self._enabled:
            return
        if self._toggle:
            self.selected = not self._selected
        if self._on_click is not None and callable(self._on_click):
            self._on_click(self, input)

    def on_cursor_entered(self, input: LocalInputMethod) -> None:
        super().on_cursor_entered(input)
        if not self._enabled:
            return
        if self._on_hover is not None and callable(self._on_hover):
            self._on_hover(self, input, True)

    def on_cursor_exited(self) -> None:
        super().on_cursor_exited()
        if self._on_hover is not None and callable(self._on_hover):
            self._on_hover(self, input, False)

    def on_cursor_released(self, input: LocalInputMethod) -> None:
        super().on_cursor_released(input)
        self.do_action(input)

    def on_opening(self) -> None:
        pass
        # self._animate_opening = True
        # self.visible = False

    def on_opened(self) -> None:
        pass

    def on_closing(self) -> None:
        pass

    def on_closed(self) -> None:
        pass

    def update(self) -> None:
        # Start at top and go counter clockwise
        length = (math.pi * 2.0) / self._total
        start = ((1.0 - (self._index / self._total)) * (math.pi * 2.0)) + \
            (math.pi / 2.0 - length / 2.0)  # 1 - position + offset_for_starting at top
        segments = round(length * RadialButton.segments_per_radian)

        rot = Quaternion.from_euler(Euler((0.00, 0.00, start + (length / 2))))
        self._text_field.location = rot.mul_vector3(
            Vector3((((self._outer_radius - self._inner_radius) / 2) + self._inner_radius, 0.00, 0.1)))
        self.location = rot.mul_vector3(Vector3((0.0625, 0.00, 0.00)))
        self._background.inner_radius = self._inner_radius
        self._background.outer_radius = self._outer_radius
        self._background.theta_segments = segments
        self._background.theta_start = start
        self._background.theta_length = length

        self._invisible_background.inner_radius = self._outer_radius
        self._invisible_background.outer_radius = self._outer_radius * 4
        self._invisible_background.theta_segments = segments
        self._invisible_background.theta_start = start
        self._invisible_background.theta_length = length

        # See comment about the strokes in the init
        # self._stroke.inner_radius = self._inner_radius
        # self._stroke.outer_radius = self._outer_radius
        # self._stroke.theta_segments = segments
        # self._stroke.theta_start = start
        # self._stroke.theta_length = length

        if not self._enabled:
            self._background.material.color = (0.0, 0.0, 0.0, 0.92)
            self._text_field.material.color = (100.0, 100.0, 100.0, 1.0)
        elif self._cursor_over or self._highlighted:
            self._background.material.color = (100, 100, 100, 1.0)
            self._text_field.material.color = (0.0, 0.0, 0.0, 1.0)
        elif self._selected:
            self._background.material.color = Colors.yellow
            self._text_field.material.color = (100.0, 100.0, 100.0, 1.0)
        else:
            self._background.material.color = (0, 0, 0, 1.0)
            self._text_field.material.color = (100.0, 100.0, 100.0, 1.0)
            self._text_field.text_color = (100.0, 100.0, 100.0, 1.0)

        super().update()
