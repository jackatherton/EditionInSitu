from typing import Any, Callable, Optional

from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu
from eis.display.components.menus.radial.radial_button import RadialButton
from eis.graph.object_3d import Object3D
from eis.editor_support.menu_manager import UserMenuData


class RadialMenu(Menu):

    def __init__(self, name: str = "Radial Menu", inner_radius: float = 1.00, outer_radius: float = 3.00):
        super().__init__(name=name)

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius

        self._items_changed_flag = True
        self._last_hovered: Optional[RadialButton] = None

    @property
    def last_hovered(self) -> RadialButton:
        return self._last_hovered

    def add_item(self, *args: Any, **kwargs: Any):
        return self.add_item_at(sum(isinstance(child, RadialButton) for child in self.children), *args, **kwargs)

    def add_item_at(
            self,
            index: int,
            label: str,
            callback: Optional[Callable[[Object3D, LocalInputMethod], None]] = None,
            toggle: bool = False,
            data: Any = None
    ) -> RadialButton:
        item_button = RadialButton(
            label=label,
            toggle=toggle,
            data=data,
            on_click=callback,
            on_hover=self._item_hover,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius
        )
        self.add_child_at(item_button, index)
        self._items_changed_flag = True
        self.invalidate()
        return item_button

    def remove_item(self, item) -> None:
        self.remove_child(item)
        self._items_changed_flag = True
        self.invalidate()

    def remove_all_items(self) -> None:
        self.remove_all_children()
        self._items_changed_flag = True

    def reset_selection(self) -> None:
        for item in self.children:
            item.highlighted = False

    def _item_hover(self, target: Object3D, input: LocalInputMethod, hover: bool) -> None:
        if hover and self._last_hovered is not target:
            self.reset_selection()
            self._last_hovered = target
            self._last_hovered.highlighted = True
        elif not hover and self._last_hovered is target:
            self._last_hovered = None
            self.reset_selection()

    def update(self) -> None:
        if self._items_changed_flag:
            self._items_changed_flag = False
            self._last_hovered = None
            num_items = sum(isinstance(child, RadialButton) for child in self.children)
            for index, item in enumerate(self.children):
                item.index = index
                item.total = num_items

        super().update()

    def on_opening(self, input: LocalInputMethod) -> None:
        super().on_opening(input)
        for item in self.children:
            item.on_opening()

    def on_opened(self, input: LocalInputMethod) -> None:
        super().on_opened(input)
        for item in self.children:
            item.on_opened()

    def on_quick_action(self, input: 'LocalInputMethod') -> None:
        super().on_quick_action(input)
        if self._last_hovered:
            self._last_hovered.do_action(input)

    def on_closing(self, user_menu_data: UserMenuData, callback: Callable[[], None]) -> None:
        for item in self.children:
            item.on_closing()
        super().on_closing(user_menu_data, callback)

    def on_closed(self) -> None:
        super().on_closed()
        for item in self.children:
            item.on_closed()
