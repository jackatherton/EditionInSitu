import logging
from typing import Optional, Dict, Generic, TypeVar, Callable, Any, TYPE_CHECKING
from uuid import UUID

from eis.input import InputMethod, PeerInputMethod
from satlib.categories import Disposable
from satlib.tasks import TaskManager, Task
from satnet.entity import Entity

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.session import PeerBase
    # noinspection PyUnresolvedReferences
    from eis.editor import Editor
    # noinspection PyUnresolvedReferences
    from eis.session import EISSession


logger = logging.getLogger(__name__)


E = TypeVar('E', bound='Editor')
S = TypeVar('S', bound='EISSession')
I = TypeVar('I', bound='InputMethod')


class EISUser(Generic[E, S, I], Entity, Disposable):
    """
    Base class for users.
    Users are owned by sessions. In EIS there can be multiple users
    on the same session, since we could easily have a client that connects
    to two or more HTC Vive for example, each user working a different
    object simultaneously.
    """

    def __init__(self, config: Dict[str, Any] = {}) -> None:
        super().__init__()

        self._config = config
        self._editor: Optional[E] = None
        self._session: Optional[S] = None

        self._task_manager = TaskManager()

        # Input Methods
        self._input_methods: Dict[UUID, I] = {}

        # Tasks
        self._check_update_controllers_task: Optional[Task] = None

    @property
    def editor(self) -> Optional[E]:
        """
        Editor instance this user is part of.
        :return: E
        """
        return self._editor

    @editor.setter
    def editor(self, value: Optional[E]) -> None:
        if self._editor != value:
            self._editor = value

    @property
    def session(self) -> S:
        """
        Session that owns this remote user.
        NOTE: For the moment remote users are still owned by the session since
              we don't have/need remote editors for every session.
        :return: S
        """
        assert self._session
        return self._session

    @session.setter
    def session(self, value: S) -> None:
        if self._session != value:
            self._session = value

    @property
    def input_methods(self) -> Dict[UUID, I]:
        """
        Get a Dict of input methods by their uuid.
        :return: Dict[UUID, I]
        """
        return self._input_methods

    def add_input(self, input: I) -> None:
        """
        Add an input method to this user
        :param input: Input Method
        :return: None
        """
        # TODO: Notify server, we currently only send input methods assigned at the creation
        input.user = self
        self._input_methods[input.uuid] = input
        if self._ready:
            input.ready()

    def initialize(self) -> None:
        super().initialize()
        for input_method in self._input_methods.values():
            input_method.user = self

    def ready(self) -> None:
        """
        Called when the user was added to a session
        This is useful when a user needs to do some initialization related
        tasks that require the network to be available
        :return: None
        """
        super().ready()

        if not self._editor:
            logger.warning("Called ready() on user without an editor")
            return

        for input_method in self._input_methods.values():
            input_method.ready()

        # Controller update task
        self._check_update_controllers_task = self._task_manager.create_task(
            callback=self._check_update_controllers,
            frequency=self._editor.config.get('user.controller.update.rate')
        )

    def dispose(self) -> None:
        # Stop Tasks
        if self._check_update_controllers_task:
            self._task_manager.remove_task(self._check_update_controllers_task)

        for input_method in self._input_methods.values():
            input_method.dispose()

        super().dispose()

    def step(self, now: float, dt: float) -> None:
        """
        Step this user instance
        This is called by the application's step method

        :param now: float - Time
        :param dt: float - Delta time since last step
        :return: None
        """
        for input_method in self._input_methods.values():
            input_method.step(now=now, dt=dt)

        self._task_manager.step(now=now)

    def _check_update_controllers(self) -> None:
        """
        Task handler for notifying the server of input controls.
        Input methods that wishes to do so can implement a notify method
        in order to send updated values and/or state to the server when needed.
        :return: None
        """
        for input in self._input_methods.values():
            input.notify()

    def pack(self, write: Callable[[Any], None]) -> None:
        # Do a custom serialization for input methods since they are not all shared/synced
        write([im for im in self._input_methods.values() if im.sync])

    def unpack(self, read: Callable[[], Any]) -> None:
        # Do a custom serialization for input methods since they are not all shared/synced
        self._input_methods = {im.uuid: im for im in read()}


class PeerUserBase(Entity, Disposable):
    """
    This is a base class for peer users.
    Peer users are used on the client-side to represent the other users
    of the application. They are created from a remote user but don't
    have the server logic, only what is needed to interact with them
    from another client.
    """

    _fields = ['_input_methods']

    def __init__(self) -> None:
        super().__init__()
        self._input_methods: Dict[UUID, PeerInputMethod] = {}
        self._peer: Optional['PeerBase'] = None

    def initialize(self) -> None:
        super().initialize()
        for input in self._input_methods.values():
            input.user = self

    def ready(self) -> None:
        for input in self._input_methods.values():
            input.ready()

    def dispose(self) -> None:
        for input_method in self._input_methods.values():
            input_method.dispose()
        super().dispose()

    @property
    def input_methods(self) -> Dict[UUID, PeerInputMethod]:
        return self._input_methods

    @property
    def peer(self) -> Optional['PeerBase']:
        return self._peer

    @peer.setter
    def peer(self, value: Optional['PeerBase']) -> None:
        if self._peer != value:
            self._peer = value

    def step(self, now: float, dt: float) -> None:
        for input_method in self._input_methods.values():
            input_method.step(now=now, dt=dt)
