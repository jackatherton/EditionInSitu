import logging
import time

logger = logging.getLogger(__name__)


class Clock:
    """
    Clock, keeps track of client/server time difference
    """

    def __init__(self) -> None:
        self._ping = 0.00
        self._delta = 0.00

    # region Properties

    @property
    def ping(self) -> float:
        """
        Ping time between client and server
        :return:
        """
        return self._ping

    @property
    def delta(self) -> float:
        """
        Estimated clock difference between client and server
        :return:
        """
        return self._delta

    # endregion

    def update(self, ping: float, pong: float) -> None:
        now = time.time()
        # Assume same time to and from so divide by 2
        self._ping = (now - ping) / 2
        self._delta = now - (pong + self._ping)
        logger.debug("Updated clock (ping: {}, delta:{})".format(self._ping, self._delta))
