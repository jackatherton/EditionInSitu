import logging
from typing import TYPE_CHECKING

from satlib.utils.debug_utils import bytes_to_hex
from satnet.errors import ParsingError
from satnet.message import MessageParser

if TYPE_CHECKING:
    from satnet.message import Message

logger = logging.getLogger(__name__)

VERSION = 0x01


class EISMessageParser(MessageParser):  # type: ignore
    """
    EIS Message Parser
    Reads and writes EIS-specific protocol information
    """

    @classmethod
    def from_bytes(cls, data: bytes, offset: int = 0) -> 'Message':
        length = len(data) - offset
        if length < 4:
            raise ParsingError("Message too short")

        if bytes(data[offset:offset + 3]) != b'EIS':
            logger.debug(bytes_to_hex(data))
            raise ParsingError("Wrong protocol")

        msg_version = data[offset + 3]
        if msg_version == 0x01:
            return super().from_bytes(data, offset + 4)

        else:
            raise ParsingError("Unsupported protocol version \"{}\"".format(msg_version))

    @classmethod
    def to_bytes(cls, message: 'Message') -> bytes:
        return b'EIS' + bytes([VERSION]) + super().to_bytes(message)  # type: ignore
