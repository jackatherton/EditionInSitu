import logging
from abc import ABCMeta, abstractmethod
from typing import Any, Callable, Dict, Generic, Optional, Set, TypeVar
from uuid import UUID

# Must appear before any EIS import since they can use it
defaultEditorConfig: Dict[str, Any] = {}

from eis.clock import Clock
from eis.constants import SERVER
from eis.engine import Engine
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.timeline import Timeline
from satlib.tasks import TaskManager
from eis.entity import Sync, SyncEntity
# noinspection PyUnresolvedReferences
from eis.user import EISUser

logger = logging.getLogger(__name__)

defaultEditorConfig['location.update.rate'] = 30
defaultEditorConfig['property.update.rate'] = 30
defaultEditorConfig['user.controller.update.rate'] = 30

U = TypeVar('U', bound='EISUser')
D = TypeVar('D', bound='EditorDelegate')


class Editor(Generic[U, D], metaclass=ABCMeta):
    """
    Editor Base Class
    """

    def __init__(self, engine: Optional[Engine] = None, delegate: Optional[D] = None, config: Optional[Dict[str, Any]] = None) -> None:
        """
        Create an editor

        :param config: Optional[Dict[str, Any]] - Part of the Dict[str, Any] holding our configuration
        :param engine: Engine
        :param delegate: Optional[D]
        """

        assert config is None or isinstance(config, dict)

        self._config = defaultEditorConfig
        if config is not None:
            self._config.update(config)
        self._engine = engine
        self._delegate = delegate

        # Main scene
        self._scene = Scene(engine=self._engine)
        if self._engine is not None:
            self._engine.add_model(self._scene.model)

        self._clock = Clock()

        self._timeline = Timeline(editor=self)

        self._users: Dict[UUID, U] = {}

        self._changed_properties: Dict[SyncEntity, Set[Sync]] = {}

        self._task_manager = TaskManager()

        # Sessions owns the location since we don't do split screen for users
        self._check_update_location_task = self._task_manager.create_task(
            callback=self._sync_properties,
            frequency=self._config.get('property.update.rate')
        )

    @property
    def config(self) -> Dict[str, Any]:
        """
        Editor configuration section from the EIS config.
        :return: Dict[str, Any]
        """
        return self._config

    @property
    def engine(self) -> Engine:
        """
        Engine that this editor uses/controls
        :return: Engine
        """
        return self._engine

    @property
    def delegate(self) -> Optional[D]:
        return self._delegate

    @delegate.setter
    def delegate(self, value: Optional[D]) -> None:
        if self._delegate != value:
            self._delegate = value
            # We have to also change the scene's model delegate here since it currently reuses the editor's
            self._scene.model.delegate = self._delegate

    @property
    def clock(self) -> Clock:
        """
        Network clock, synced between client/server
        :return: Clock
        """
        return self._clock

    @property
    def scene(self) -> Scene:
        """
        Current scene
        :return: Scene
        """
        return self._scene

    @property
    def timeline(self) -> Timeline:
        """
        Timeline
        :return: Timeline
        """
        return self._timeline

    @property
    def users(self) -> Dict[UUID, U]:
        """
        Users by uuid
        :return: Dict[UUID, U]
        """
        return self._users

    def step(self, now: float, dt: float) -> None:
        """
        Step this editor instance
        This should be called by the application's main loop

        :param now: float - Time
        :param dt: float - Delta time since last step
        :return: None
        """

        # Users step the input methods
        for user in self._users.values():
            user.step(now, dt)

        # Step the graph nodes
        self._scene.step(now=now, dt=dt)

        self._task_manager.step(now=now)

        self._timeline.step(now=now, dt=dt)

        # Step the engine
        if self._engine is not None:
            self._engine.step(now, dt)

    def save_scene_to_file(self, path: str) -> None:
        try:
            ext = path[path.rindex(".") + 1:]
        except ValueError:
            return

        from eis.converters import converters
        for converter in converters:
            if converter.handles(ext):
                converter.export_path(self.scene, path)
                return

    @staticmethod
    def load_scene_from_file(path: str) -> Optional[Scene]:
        try:
            ext = path[path.rindex(".") + 1:]
        except ValueError:
            return None

        from eis.converters import converters
        for converter in converters:
            if converter.handles(ext):
                return converter.import_path(path)

    def load_scene(self, scene: Scene, on_loaded: Optional[Callable[[bool], None]] = None) -> None:
        """
        Load scene

        :param scene: Scene
        :param on_loaded: Optional[Callable[[bool], None]]
        :return: None
        """
        assert isinstance(scene, Scene)
        logger.info("Loading scene {}".format(scene))

        def on_scene_loaded(scene: Optional[Scene]) -> None:
            if not scene:
                logger.error("Error while loading scene: could not load model data or add it to the graph")
            else:
                assert isinstance(scene, Scene)
                self._scene = scene
                self._scene.editor = self
                if self._delegate:
                    self._delegate.on_scene_loaded(self._scene)
                    # We have to set the model's delegate here, as the Scene has no Engine
                    # when on the server side.
                    self._scene.model.delegate = self.delegate
            if on_loaded:
                on_loaded(scene is not None)

        if self._engine is not None:
            if self._scene is not None:
                self._engine.root.remove_child(self._scene.model.root)
            self._engine.load_scene(scene=scene, callback=on_scene_loaded)
        else:
            on_scene_loaded(scene)

        # User objects layer, added after loading a new scene
        if SERVER:
            self.initialize_layers()

    def add_model(self, model: Model, parent: Optional[Object3D] = None) -> None:
        """
        Load a model into the current scene

        :param model: Model
        :param parent: Optional[Object3D]
        :return: None
        """
        assert isinstance(model, Model)
        logger.info("Loading model {}".format(model))

        if not parent:
            parent = self._scene.model.root

        model_root_matrix = model.root.matrix_world
        child = parent.add_child(model.root)
        child.matrix_world = model_root_matrix

    def add_user(self, user: U) -> None:
        """
        Add a user to the editor
        :param user: U
        :return: None
        """
        user.editor = self
        self._users[user.uuid] = user
        user.ready()
        if self._delegate:
            self._delegate.on_user_added(user=user)

    def remove_user(self, user: U) -> None:
        """
        Remove a user
        :param user: U
        :return: None
        """
        if self._delegate:
            self._delegate.on_user_removed(user=user)

        del self._users[user.uuid]
        user.dispose()

    def remove_user_by_id(self, user_id: UUID) -> None:
        user = self._users.get(user_id)
        if user:
            self.remove_user(user)

    def property_changed(self, entity: SyncEntity, sync: Sync) -> None:
        """
        Calling this methods adds the property to the list of changed properties.
        A task is then responsible for synchronizing at a regular interval.

        :param entity: SyncEntity
        :param sync: Sync
        :return: None
        """
        entity_syncs = self._changed_properties.get(entity)
        if not entity_syncs:
            entity_syncs = set()
            self._changed_properties[entity] = entity_syncs
        entity_syncs.add(sync)

    def _sync_properties(self) -> None:
        """
        Property sync task, calls the delegate for every changed property.

        :return: None
        """
        if self._delegate:
            for entity, syncs in self._changed_properties.items():
                for sync in syncs:
                    self._delegate.on_property_changed(entity=entity, sync=sync)
        self._changed_properties = {}
    ...


class EditorDelegate(Generic[U], metaclass=ABCMeta):
    """
    Editor Delegate
    Receives callbacks from the editor and communicates with the network.
    """

    def on_reset(self) -> None:
        """
        Called on editor reset
        :return: None
        """
        logger.info("Editor reset!")

    def on_scene_loaded(self, scene: Scene) -> None:
        """
        Called when a scene was loaded
        :param scene: Scene
        :return: None
        """
        logger.info("Scene loaded!")

    def on_model_loaded(self, model: Model) -> None:
        """
        Called when a model was loaded
        :param model: Model
        :return: None
        """
        logger.info("Model loaded!")

    def on_user_added(self, user: U) -> None:
        logger.info("User added!")

    def on_user_removed(self, user: U) -> None:
        logger.info("User removed!")

    @abstractmethod
    def on_property_changed(self, entity: SyncEntity, sync: Sync) -> None:
        """
        Called when a property's value has changed. This is always preceded by
        at least one call to `on_sync_changed` (from engine delegate) but this one is rate limited and
        is used to notify the network.

        :param entity: SyncEntity
        :param sync: Sync
        :return: None
        """
