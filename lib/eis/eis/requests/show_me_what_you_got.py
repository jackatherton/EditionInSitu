import logging
from typing import Optional, TYPE_CHECKING

from eis.request import RequestId, ResponseId, EISServerRequest, EISClientResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.SHOW_ME_WHAT_YOU_GOT)
class ShowMeWhatYouGot(EISServerRequest):
    """
    Easter egg'd `Hello` request from the server.
    In our scenario the server initiates the communication since we use zmq, that's why this is a server request.
    The client cannot announce itself on connection since the server can close and reopen the connection
    without the client ever knowing about it (in ZMQ anyway).
    """

    _fields = ['session_id']

    def __init__(self, session_id: Optional[int] = None) -> None:
        super().__init__()
        self.session_id = session_id

    def handle(self, session: 'EISLocalSession', respond: ResponseCallback) -> None:
        logger.info("Received identification request from server")

        if self.session_id is not None:
            logger.info("Session id: {}".format(self.session_id))
            session.id = self.session_id
        else:
            logger.warning("Server did not assign an id to this session")

        respond(GetSchwifty(nickname=session.nickname))


@response(id=ResponseId.GET_SCHWIFTY)
class GetSchwifty(EISClientResponse[ShowMeWhatYouGot]):
    """
    Easter egg'd response to `Hello` request from the server.
    In our scenario the server initiates the communication since we use zmq
    """

    _fields = ['nickname']

    def __init__(self, nickname: Optional[str]=None) -> None:
        super().__init__()
        self.nickname = nickname

    def handle(self, request: ShowMeWhatYouGot, session: 'EISRemoteSession') -> None:
        #assert isinstance(request, ShowMeWhatYouGot)
        if not isinstance(request, ShowMeWhatYouGot):
            logger.error("Request is not ShowMeWhatYouGot, fix!")
            print(request)  # FIXME: the last assert has a tendency to crash, and we don't know why.
            exit()
        logger.info("Received identification response from client \"{}\" ({})".format(self.nickname, session))
        if self.nickname:
            session._nickname = self.nickname
            session.ready()
        else:
            logger.warning("Incomplete identification, disconnecting client")
            session.disconnect()
