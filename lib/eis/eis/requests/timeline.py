import logging

from typing import Optional, TYPE_CHECKING

from eis.request import RequestId, ResponseId, EISClientRequest, EISServerResponse
from satnet.request import request, response, ResponseCallback

if TYPE_CHECKING:
    from eis.client.session import EISLocalSession
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@request(id=RequestId.GET_TIMELINE_POSITION)
class GetTimelinePosition(EISClientRequest):
    """
    Timeline position request
    """

    def __init__(self) -> None:
        super().__init__()

    def handle(self, session: 'EISRemoteSession', respond: ResponseCallback) -> None:
        # logger.debug("Received timeline position request from {}".format(session))
        respond(SetTimelinePosition(session.editor.timeline.time, session.editor.timeline.running))


@response(id=ResponseId.SET_TIMELINE_POSITION)
class SetTimelinePosition(EISServerResponse[GetTimelinePosition]):
    """
    Timeline position response
    """

    _fields = ['timeline_position', 'running']

    def __init__(self, timeline_position: Optional[float] = None, running: Optional[bool] = None) -> None:
        super().__init__()
        self.timeline_position = timeline_position
        self.running = running

    def handle(self, request: GetTimelinePosition, session: 'EISLocalSession') -> None:
        assert isinstance(request, GetTimelinePosition)
        assert(self.timeline_position is not None)
        assert(self.running is not None)

        session.editor.timeline.time = self.timeline_position
        if self.running:
            session.editor.timeline.start()
        else:
            session.editor.timeline.stop()
