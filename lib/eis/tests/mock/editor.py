from eis.editor import Editor
from eis.graph.object_3d import Object3D


class DummyEntity:
    pass

class DummySync:
    pass


class DummyEngine:
    delegate = None

    def add_model(self, model):
        pass

class LessDumbEngine:
    delegate = None

    def __init__(self):
       self._root = Object3D(name="Engine Root")

    @property
    def root(self):
        return self._root

    def load_scene(self, scene, callback=None):
        pass

    def add_model(self, model):
        pass

class MockDelegate:

    def on_property_changed(self, entity, sync):
        pass

    def set_object_behaviors(self, model, object):
        pass


class MockEditor(Editor):
    def __init__(self):
        super().__init__(engine=DummyEngine(), delegate=MockDelegate())


class MockEditor_extended(Editor):
    def __init__(self):
        super().__init__(engine=LessDumbEngine(), delegate=MockDelegate())
