from unittest import TestCase

from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4
from eis.all_serializable import MathSerializer
from satnet.serialization import Serializable, serializable


@serializable(prefix=0xFF, id=0xD3)
class MathSerializable(Serializable):
    _fields = ['m', 'q', 'v3', 'v4']

    def __init__(self, m=None, q=None, v3=None, v4=None):
        super().__init__()
        self.m = m
        self.q = q
        self.v3 = v3
        self.v4 = v4


class MathTest(TestCase):
    def test_math_serializer1(self):
        ms = MathSerializer[Matrix44](math_type=Matrix44)
        m1 = Matrix44([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
        m1b = Matrix44([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
        self.assertEqual(m1, m1b)
        m2 = ms.deserialize(ms.serialize(m1))
        self.assertEqual(m1, m2)

    def test_math_serializer2(self):
        ms = MathSerializer[Matrix44](math_type=Matrix44)
        m1 = Matrix44([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
        m1b = Matrix44([1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16.])
        self.assertEqual(m1, m1b)
        m2 = ms.deserialize(ms.serialize(m1))
        self.assertEqual(m1, m2)

    def test_math_serializer3(self):
        ms = MathSerializer[Matrix44](math_type=Matrix44)
        m1 = Matrix44([1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16.])
        m1b = Matrix44([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
        self.assertEqual(m1, m1b)
        m2 = ms.deserialize(ms.serialize(m1))
        self.assertEqual(m1, m2)

    def test_math_serialization1(self):
        m1 = MathSerializable(
            m=Matrix44([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]),
            q=Quaternion([1, 2, 3, 4]),
            v3=Vector3([1, 2, 3]),
            v4=Vector4([1, 2, 3, 4])
        )

        m2 = Serializable.deserialize(m1.serialize())

        self.assertEqual(m1.m, m2.m)
        self.assertEqual(m1.q, m2.q)
        self.assertEqual(m1.v3, m2.v3)
        self.assertEqual(m1.v4, m2.v4)

    def test_math_serialization2(self):
        m1 = MathSerializable(
            m=Matrix44([1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14., 15., 16.]),
            q=Quaternion([1., 2., 3., 4.]),
            v3=Vector3([1., 2., 3.]),
            v4=Vector4([1., 2., 3., 4.])
        )

        m2 = Serializable.deserialize(m1.serialize())

        self.assertEqual(m1.m, m2.m)
        self.assertEqual(m1.q, m2.q)
        self.assertEqual(m1.v3, m2.v3)
        self.assertEqual(m1.v4, m2.v4)
