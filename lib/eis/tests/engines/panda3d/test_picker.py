import os
import unittest
from unittest import TestCase

from eis.engine import defaultEngineConfig
from eis.engines.panda3d.converter import Panda3DConverter
from eis.engines.panda3d.engine import Panda3DEngine
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class Scene():
    def __init__(self, model):
        self.model = model


class Editor():
    def __init__(self, engine, model):
        self.scene = Scene(model)
        self.engine = engine


@unittest.skip
class Panda3DPickerTest(TestCase):
    def setUp(self):
        defaultEngineConfig['headless'] = True
        self.engine = Panda3DEngine()
        self.engine._headless = True
        self.engine.initialize()

        converter = Panda3DConverter()
        path = os.path.realpath(os.path.join(os.path.dirname(__file__),
                                             "../../../../../res/scenes/Sponza/export/Sponza.bam"))
        self.model = self.engine.read_model_from_file(path=path)
        self.engine.add_model(self.model)
        self.engine.step(now=0, dt=1 / 30)
        self.editor = Editor(self.engine, self.model)

        self.picker = self.engine.get_picker(self.editor)

    def tearDown(self):
        self.engine.shutdown()
        del self.picker
        del self.model
        del self.engine

    def test_pick(self):
        self.picker.origin = Vector3([0, 3, 2])
        self.picker.direction = Vector3([0, 0, -1])
        self.engine.base.cTrav.traverse(self.engine.base.render)
        pickedObject, position = self.picker.pick()
        print("Pick single object: Picked", pickedObject, "at position", position)

    def test_pick_all(self):
        self.picker.origin = Vector3([0, 3, 2])
        self.picker.direction = Vector3([0, 0, -1])
        self.engine.base.cTrav.traverse(self.engine.base.render)
        objects = self.picker.pick_all()
        print("Picked all objects:", objects)

    def test_pick_object(self):
        self.picker.origin = Vector3([0, 3, 2])
        self.picker.direction = Vector3([0, 0, -1])
        self.engine.base.cTrav.traverse(self.engine.base.render)
        pickedObject, position = self.picker.pick()
        picked, position = self.picker.pick_object(pickedObject)
        print("Re-pick single object at position", position)

    def test_origin(self):
        self.assertTrue(self.picker.origin is not self.picker._origin)
        self.assertEqual(type(self.picker.origin), Vector3)

        a_location = Vector3([1.0, 2.0, 3.0])
        self.picker.origin = a_location
        self.assertEqual(self.picker.origin, a_location)

    def test_direction(self):
        self.assertTrue(self.picker.direction is not self.picker._direction)
        self.assertEqual(type(self.picker.direction), Vector3)

        a_location = Vector3([1.0, 2.0, 3.0])
        self.picker.direction = a_location
        self.assertEqual(self.picker.direction, a_location.normalized)

    def test_origin_corrected_copy(self):
        self.assertTrue(self.picker.origin_corrected is not self.picker._origin_corrected)
        self.assertEqual(type(self.picker.origin_corrected), Vector3)

    def test_direction_corrected_copy(self):
        self.assertTrue(self.picker.direction_corrected is not self.picker._direction_corrected)
        self.assertEqual(type(self.picker.direction_corrected), Vector3)

    def test_ray_orientation(self):
        self.assertEqual(self.picker.ray_orientation, Quaternion.from_vector_track_up(self.picker.direction_corrected))
        self.assertEqual(type(self.picker.ray_orientation), Quaternion)

    def test_ray_vector_copy(self):
        self.assertTrue(self.picker.ray_vector is not self.picker._ray_vector)
        self.assertEqual(type(self.picker.ray_vector), Vector3)

    def test_cursor_matrix_copy(self):
        self.assertTrue(self.picker.cursor_matrix is not self.picker._cursor_matrix)
        self.assertEqual(type(self.picker.cursor_matrix), Matrix44)

    def test_picked_location_copy(self):
        self.assertTrue(self.picker.picked_location is not self.picker._picked_location)
