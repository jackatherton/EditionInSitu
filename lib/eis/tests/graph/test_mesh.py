from unittest import TestCase, mock

from eis.graph.mesh import Mesh
from eis.graph.geometry import Geometry
from eis.graph.model import Model
from eis.graph.object_3d import Object3D


class TestMesh(TestCase):

    def test_copy(self):
        geometry = Geometry()
        original = Mesh(
            name="mesh",
            geometries=[geometry]
        )

        with mock.patch.object(geometry, "copy", wraps=geometry.copy) as geometry_copy:
            copy = original.copy()
            geometry_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.geometries, copy.geometries)

    def test_copy_unless_managed(self):
        geometry = Geometry()
        geometry.managed = True
        original = Mesh(geometries=[geometry])

        with mock.patch.object(geometry, "copy", wraps=geometry.copy) as geometry_copy:
            copy = original.copy()
            geometry_copy.assert_not_called()

        self.assertEqual(len(copy.geometries), 0)

    def test_copy_shared(self):
        geometry = Geometry()
        original = Mesh(
            name="mesh",
            geometries=[geometry]
        )

        with mock.patch.object(geometry, "copy_shared", wraps=geometry.copy_shared) as geometry_copy:
            copy = original.copy_shared()
            geometry_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.geometries, copy.geometries)

    def test_copy_shared_unless_managed(self):
        geometry = Geometry()
        geometry.managed = True
        original = Mesh(geometries=[geometry])

        with mock.patch.object(geometry, "copy_shared", wraps=geometry.copy_shared) as geometry_copy:
            copy = original.copy_shared()
            geometry_copy.assert_not_called()

        self.assertEqual(len(copy.geometries), 0)

    def test_adding_geometry_registers_with_owners(self):
        model = Model()
        mesh = Mesh()
        mesh.add_owner(model)
        geom = Geometry()
        with mock.patch.object(geom, "added_to_model") as added:
            mesh.add_geometry(geom)
            added.assert_called_once_with(model)
            self.assertEqual(geom.mesh, mesh)

    def test_removing_geometry_unregisters_with_owners(self):
        model = Model()
        mesh = Mesh()
        mesh.add_owner(model)
        geom = Geometry()
        mesh.add_geometry(geom)
        with mock.patch.object(geom, "removed_from_model") as removed:
            mesh.remove_geometry(geom)
            removed.assert_called_once_with(model)
            self.assertIsNone(geom.mesh)

    def test_adding_to_model_propagates_to_geometry(self):
        model = Model()
        geom = Geometry()
        mesh = Mesh(geometries=[geom])
        with mock.patch.object(geom, "added_to_model") as added:
            mesh.added_to_model(model)
            added.assert_called_once_with(model)
            self.assertEqual(geom.mesh, mesh)

    def test_removing_from_model_propagates_to_geometry(self):
        model = Model()
        geom = Geometry()
        mesh = Mesh(geometries=[geom])
        mesh.added_to_model(model)
        with mock.patch.object(geom, "removed_from_model") as removed:
            mesh.removed_from_model(model)
            removed.assert_called_once_with(model)
            self.assertEqual(geom.mesh, mesh)  # Safety, we had a bug there once

    def test_adding_to_model_registers_geometry(self):
        model = Model()
        geom = Geometry()
        mesh = Mesh(geometries=[geom])
        with mock.patch.object(model, "add_mesh") as add:
            mesh.added_to_model(model)
            add.assert_called_once_with(mesh)

    def test_removing_from_model_unregisters_geometry(self):
        model = Model()
        geom = Geometry()
        mesh = Mesh(geometries=[geom])
        model.root.add_child(Object3D(mesh=mesh))
        with mock.patch.object(model, "remove_mesh") as remove:
            mesh.removed_from_model(model)
            remove.assert_called_once_with(mesh)
