from unittest import TestCase, mock

from eis.graph.light import Light
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44


class TestLight(TestCase):
    # region Copy

    def test_copy(self):
        child = Object3D()
        original = Light(
            name="light",
            children=[child],  # Really just to see if it was implemented correctly
            matrix=Matrix44((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)),
            type=Light.LightType.SUNLIGHT,
            radius=123,
            energy=456,
            color=(1, 2, 3, 4),
            spot_fov=789
        )

        with mock.patch.object(child, "copy", wraps=child.copy) as child_copy:
            copy = original.copy()
            child_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.children, copy.children)
        self.assertEqual(original.matrix, copy.matrix)
        self.assertIsNot(original.matrix, copy.matrix)
        self.assertEqual(original.type, copy.type)
        self.assertEqual(original.radius, copy.radius)
        self.assertEqual(original.energy, copy.energy)
        self.assertEqual(original.color, copy.color)
        self.assertEqual(original.spot_fov, copy.spot_fov)

    def test_copy_unless_managed(self):
        child = Object3D()
        child.managed = True
        original = Light(children=[child])

        with mock.patch.object(child, "copy", wraps=child.copy) as child_copy:
            copy = original.copy()
            child_copy.assert_not_called()

        self.assertEqual(len(copy.children), 0)

    def test_copy_shared(self):
        child = Object3D()
        original = Light(
            name="light",
            children=[child],  # Really just to see if it was implemented correctly
            matrix=Matrix44((1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)),
            type=Light.LightType.SUNLIGHT,
            radius=123,
            energy=456,
            color=(1, 2, 3, 4),
            spot_fov=789
        )

        with mock.patch.object(child, "copy_shared", wraps=child.copy_shared) as child_copy:
            copy = original.copy_shared()
            child_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertNotEqual(original.children, copy.children)
        self.assertEqual(original.matrix, copy.matrix)
        self.assertIsNot(original.matrix, copy.matrix)
        self.assertEqual(original.type, copy.type)
        self.assertEqual(original.radius, copy.radius)
        self.assertEqual(original.energy, copy.energy)
        self.assertEqual(original.color, copy.color)
        self.assertEqual(original.spot_fov, copy.spot_fov)

    def test_copy_shared_unless_managed(self):
        child = Object3D()
        child.managed = True
        original = Light(children=[child])

        with mock.patch.object(child, "copy_shared", wraps=child.copy_shared) as child_copy:
            copy = original.copy_shared()
            child_copy.assert_not_called()

        self.assertEqual(len(copy.children), 0)

    # endregion

    ...
