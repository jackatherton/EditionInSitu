from unittest import TestCase, mock

from eis.graph.behavior import Behavior
from eis.graph.geometry import Geometry
from eis.graph.material import Material, MaterialTexture
from eis.graph.mesh import Mesh
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.graph.texture import Texture


class MockBehavior(Behavior):

    def step(self, now: float, dt: float):
        pass


class TestModel(TestCase):

    def test_model_init(self):
        model = Model()
        self.assertIsNotNone(model.root)
        self.assertEqual(model, model.root.model)

    def test_model_with_root_init(self):
        root = Object3D()
        model = Model(root=root)
        self.assertIsNotNone(model.root)
        self.assertEqual(model, model.root.model)

        # Root will self register when assigned a model
        self.assertIn(root.uuid, model._object_uuid_map)
        self.assertIn(root.uuid, model._all_syncable_entities)

    def test_adding_object_registers_it(self):
        model = Model()
        # After, as to not catch the init default model addition
        model.delegate = mock.Mock()
        obj = Object3D()
        model.add_object(obj)
        self.assertEqual(model.get_object_by_uuid(obj.uuid), obj)
        self.assertIn(obj.uuid, model._object_uuid_map)
        self.assertIn(obj.uuid, model._all_syncable_entities)
        self.assertEqual(model.on_sync_changed, obj.on_sync_changed)
        model.delegate.on_object3d_added.assert_called_once_with(model, obj)

    def test_removing_object_unregisters_it(self):
        model = Model(delegate=mock.Mock())
        obj = Object3D()
        model.add_object(obj)
        model.remove_object(obj)
        self.assertIsNone(model.get_object_by_uuid(obj.uuid))
        self.assertNotIn(obj.uuid, model._object_uuid_map)
        self.assertNotIn(obj.uuid, model._all_syncable_entities)
        self.assertIsNone(obj.on_sync_changed)
        model.delegate.on_object3d_removed.assert_called_once_with(model, obj)

    def test_adding_behavior_registers_it(self):
        model = Model()
        # After, as to not catch the init default model addition
        model.delegate = mock.Mock()
        behavior = MockBehavior()
        model.add_behavior(behavior)
        self.assertEqual(model.get_behavior_by_uuid(behavior.uuid), behavior)
        self.assertIn(behavior.uuid, model._behavior_map)
        self.assertIn(behavior.uuid, model._all_syncable_entities)
        self.assertEqual(model.on_sync_changed, behavior.on_sync_changed)
        model.delegate.on_behavior_added.assert_called_once_with(model, behavior)

    def test_removing_behavior_unregisters_it(self):
        model = Model(delegate=mock.Mock())
        behavior = MockBehavior()
        model.add_behavior(behavior)
        model.remove_behavior(behavior)
        self.assertIsNone(model.get_behavior_by_uuid(behavior.uuid))
        self.assertNotIn(behavior.uuid, model._behavior_map)
        self.assertNotIn(behavior.uuid, model._all_syncable_entities)
        self.assertIsNone(behavior.on_sync_changed)
        model.delegate.on_behavior_removed.assert_called_once_with(model, behavior)

    def test_adding_mesh_registers_it(self):
        model = Model(delegate=mock.Mock())
        mesh = Mesh()
        with mock.patch.object(mesh, "add_owner") as add:
            model.add_mesh(mesh)
            add.assert_called_once_with(model)
        self.assertEqual(model.meshes, [mesh])
        self.assertEqual(model._mesh_map[mesh.uuid], mesh)
        self.assertEqual(model._all_syncable_entities[mesh.uuid], mesh)
        self.assertEqual(model.get_mesh_by_uuid(mesh.uuid), mesh)
        self.assertEqual(mesh.on_sync_changed, model.on_sync_changed)
        model.delegate.on_mesh_added.assert_called_once_with(model, mesh)

    def test_adding_managed_mesh_registers_it(self):
        model = Model()
        mesh = Mesh()
        mesh.managed = True
        model.add_mesh(mesh)
        self.assertIsNone(mesh.on_sync_changed)

    def test_removing_mesh_unregisters_it(self):
        model = Model(delegate=mock.Mock())
        mesh = Mesh()
        model.add_mesh(mesh)
        with mock.patch.object(mesh, "remove_owner") as remove:
            model.remove_mesh(mesh)
            remove.assert_called_once_with(model)
        self.assertEqual(model.meshes, list())
        self.assertNotIn(mesh.uuid, model._mesh_map)
        self.assertNotIn(mesh.uuid, model._all_syncable_entities)
        self.assertIsNone(model.get_mesh_by_uuid(mesh.uuid))
        self.assertIsNone(mesh.on_sync_changed)
        model.delegate.on_mesh_removed.assert_called_once_with(model, mesh)

    def test_removing_mesh_keeps_it_if_still_used(self):
        model = Model()
        mesh = Mesh()
        model.add_mesh(mesh)
        with mock.patch.object(mesh, "remove_owner", return_value=False) as remove:
            model.remove_mesh(mesh)
            remove.assert_called_once_with(model)
        self.assertEqual(model.meshes, [mesh])
        self.assertEqual(model._mesh_map[mesh.uuid], mesh)
        self.assertEqual(model._all_syncable_entities[mesh.uuid], mesh)
        self.assertEqual(model.get_mesh_by_uuid(mesh.uuid), mesh)
        self.assertEqual(mesh.on_sync_changed, model.on_sync_changed)

    def test_adding_material_registers_it(self):
        model = Model(delegate=mock.Mock())
        material = Material()
        with mock.patch.object(material, "add_owner") as add:
            model.add_material(material)
            add.assert_called_once_with(model)
        self.assertEqual(model.materials, [material])
        self.assertEqual(model.get_material_by_uuid(material.uuid), material)
        self.assertIn(material.uuid, model._material_map)
        self.assertIn(material.uuid, model._all_syncable_entities)
        self.assertEqual(model.on_sync_changed, material.on_sync_changed)
        model.delegate.on_material_added.assert_called_once_with(model, material)

    def test_removing_material_unregisters_it(self):
        model = Model(delegate=mock.Mock())
        material = Material()
        model.add_material(material)
        with mock.patch.object(material, "remove_owner") as remove:
            model.remove_material(material)
            remove.assert_called_once_with(model)
        self.assertEqual(model.materials, list())
        self.assertIsNone(model.get_material_by_uuid(material.uuid))
        self.assertNotIn(material.uuid, model._material_map)
        self.assertNotIn(material.uuid, model._all_syncable_entities)
        self.assertEqual(None, material.on_sync_changed)
        model.delegate.on_material_removed.assert_called_once_with(model, material)

    def test_removing_material_keeps_it_if_still_used(self):
        model = Model()
        material = Material()
        model.add_material(material)
        with mock.patch.object(material, "remove_owner", return_value=False) as remove:
            model.remove_material(material)
            remove.assert_called_once_with(model)
        self.assertEqual(model.materials, [material])
        self.assertEqual(model.get_material_by_uuid(material.uuid), material)
        self.assertIn(material.uuid, model._material_map)
        self.assertIn(material.uuid, model._all_syncable_entities)
        self.assertEqual(model.on_sync_changed, material.on_sync_changed)

    def test_adding_texture_registers_it(self):
        model = Model(delegate=mock.Mock())
        texture = Texture()
        with mock.patch.object(texture, "add_owner") as add:
            model.add_texture(texture)
            add.assert_called_once_with(model)
        self.assertEqual(model.textures, [texture])
        self.assertEqual(model.get_texture_by_uuid(texture.uuid), texture)
        self.assertIn(texture.uuid, model._texture_map)
        self.assertIn(texture.uuid, model._all_syncable_entities)
        self.assertEqual(model.on_sync_changed, texture.on_sync_changed)
        model.delegate.on_texture_added.assert_called_once_with(model, texture)

    def test_removing_texture_unregisters_it(self):
        model = Model(delegate=mock.Mock())
        texture = Texture()
        model.add_texture(texture)
        with mock.patch.object(texture, "remove_owner") as remove:
            model.remove_texture(texture)
            remove.assert_called_once_with(model)
        self.assertEqual(model.textures, list())
        self.assertIsNone(model.get_texture_by_uuid(texture.uuid))
        self.assertNotIn(texture.uuid, model._texture_map)
        self.assertNotIn(texture.uuid, model._all_syncable_entities)
        self.assertEqual(None, texture.on_sync_changed)
        model.delegate.on_texture_removed.assert_called_once_with(model, texture)

    def test_removing_texture_keeps_it_if_still_used(self):
        model = Model()
        texture = Texture()
        model.add_texture(texture)
        with mock.patch.object(texture, "remove_owner", return_value=False) as remove:
            model.remove_texture(texture)
            remove.assert_called_once_with(model)
        self.assertEqual(model.textures, [texture])
        self.assertEqual(model.get_texture_by_uuid(texture.uuid), texture)
        self.assertIn(texture.uuid, model._texture_map)
        self.assertIn(texture.uuid, model._all_syncable_entities)
        self.assertEqual(model.on_sync_changed, texture.on_sync_changed)

    def test_copy(self):
        texture = Texture()
        material = Material(textures=[MaterialTexture(texture=texture)])
        mesh = Mesh(geometries=[Geometry(material=material)])
        root = Object3D(mesh=mesh)
        original = Model(
            root=root,
            meshes=[mesh],
            materials=[material],
            textures=[texture]
        )

        with mock.patch.object(texture, "copy", wraps=texture.copy) as texture_copy, \
                mock.patch.object(material, "copy", wraps=material.copy) as material_copy, \
                mock.patch.object(mesh, "copy", wraps=mesh.copy) as mesh_copy, \
                mock.patch.object(root, "copy", wraps=root.copy) as root_copy:
            copy = original.copy()
            root_copy.assert_called_once_with()
            mesh_copy.assert_called_once_with()
            material_copy.assert_called_once_with()
            texture_copy.assert_called_once_with()

        self.assertIsNotNone(copy.root)
        self.assertNotEqual(original.root, copy.root)

        # Meshes
        self.assertNotEqual(original.root.mesh, copy.root.mesh)
        self.assertEqual(len(copy.meshes), 1)
        self.assertEqual(copy.meshes[0], copy.root.mesh)

        # Materials
        self.assertNotEqual(
            original.root.mesh.geometries[0].material,
            copy.root.mesh.geometries[0].material
        )
        self.assertEqual(len(copy.materials), 1)
        self.assertEqual(
            copy.materials[0],
            copy.root.mesh.geometries[0].material
        )

        # Textures
        self.assertNotEqual(
            original.root.mesh.geometries[0].material.textures[0].texture,
            copy.root.mesh.geometries[0].material.textures[0].texture
        )
        self.assertEqual(len(copy.textures), 1)
        self.assertEqual(
            copy.textures[0],
            copy.root.mesh.geometries[0].material.textures[0].texture
        )

    def test_copy_shared(self):
        texture = Texture()
        material = Material(textures=[MaterialTexture(texture=texture)])
        mesh = Mesh(geometries=[Geometry(material=material)])
        root = Object3D(mesh=mesh)
        original = Model(
            root=root,
            meshes=[mesh],
            materials=[material],
            textures=[texture]
        )

        with mock.patch.object(texture, "copy_shared", wraps=texture.copy_shared) as texture_copy, \
                mock.patch.object(material, "copy_shared", wraps=material.copy_shared) as material_copy, \
                mock.patch.object(mesh, "copy_shared", wraps=mesh.copy_shared) as mesh_copy, \
                mock.patch.object(root, "copy_shared", wraps=root.copy_shared) as root_copy:
            copy = original.copy_shared()
            root_copy.assert_called_once_with()
            # We're copy_shared'ing from the model so we don't event copy shared entities
            mesh_copy.assert_not_called()
            material_copy.assert_not_called()
            texture_copy.assert_not_called()

        self.assertIsNotNone(copy.root)
        self.assertNotEqual(original.root, copy.root)

        # Meshes
        self.assertEqual(original.root.mesh, copy.root.mesh)
        self.assertEqual(len(copy.meshes), 1)
        self.assertEqual(copy.meshes[0], copy.root.mesh)

        # Materials
        self.assertEqual(
            original.root.mesh.geometries[0].material,
            copy.root.mesh.geometries[0].material
        )
        self.assertEqual(len(copy.materials), 1)
        self.assertEqual(
            copy.materials[0],
            copy.root.mesh.geometries[0].material
        )

        # Textures
        self.assertEqual(
            original.root.mesh.geometries[0].material.textures[0].texture,
            copy.root.mesh.geometries[0].material.textures[0].texture
        )
        self.assertEqual(len(copy.textures), 1)
        self.assertEqual(
            copy.textures[0],
            copy.root.mesh.geometries[0].material.textures[0].texture
        )


class TestModelFromData(TestCase):
    def test_model_should_distribute_shared_objects(self):
        texture = Texture()
        material_texture = MaterialTexture(texture_id=texture.uuid)
        material = Material(textures=[material_texture])
        geometry = Geometry(material_id=material.uuid)
        mesh = Mesh(geometries=[geometry])
        geom_object_3d = Object3D(mesh_id=mesh.uuid)
        root = Object3D(children=[geom_object_3d])

        model = Model(
            root=root,
            meshes=[mesh],
            materials=[material],
            textures=[texture]
        )

        # Initialize should have been called on the model

        self.assertEqual(model.get_object_by_uuid(root.uuid), root)
        self.assertEqual(model.get_object_by_uuid(geom_object_3d.uuid), geom_object_3d)
        self.assertEqual(model.get_mesh_by_uuid(mesh.uuid), mesh)
        self.assertEqual(model.get_material_by_uuid(material.uuid), material)
        self.assertEqual(model.get_texture_by_uuid(texture.uuid), texture)

        self.assertEqual(model.root, root)
        self.assertEqual(model.root.children[0], geom_object_3d)
        self.assertEqual(model.root.children[0].mesh, mesh)
        self.assertEqual(model.root.children[0].mesh.geometries[0], geometry)
        self.assertEqual(model.root.children[0].mesh.geometries[0].material, material)
        self.assertEqual(model.root.children[0].mesh.geometries[0].material.textures[0], material_texture)
        self.assertEqual(model.root.children[0].mesh.geometries[0].material.textures[0].texture, texture)


class TestModelFromTree(TestCase):
    def setUp(self):
        self.detached = Object3D(name="detached")
        self.attached = Object3D(name="attached")
        self.root = Object3D(name="root", children=[self.attached])
        self.delegate = mock.Mock()
        self.model = Model(root=self.root, delegate=self.delegate)
        self.scene = Scene(model=self.model)

        self.texture = Texture()
        self.material_texture = MaterialTexture(texture=self.texture)
        self.material = Material(textures=[self.material_texture])
        self.geometry = Geometry(material=self.material)
        self.mesh = Mesh(geometries=[self.geometry])
        self.geom_object_3d = Object3D(mesh=self.mesh)
        self.sub = Object3D(children=[self.geom_object_3d])
        self.obj = Object3D(children=[self.sub])

    def assert_shared(self, num=1):
        self.assertIn(self.model, self.texture.owners)
        self.assertEqual(self.texture.owners[self.model], num)
        self.assertIn(self.texture, self.model.textures)
        self.assertIn(self.texture.uuid, self.model._texture_map)
        self.assertEqual(self.model.get_texture_by_uuid(self.texture.uuid), self.texture)

        self.assertIn(self.model, self.material.owners)
        self.assertEqual(self.material.owners[self.model], num)
        self.assertIn(self.material, self.model.materials)
        self.assertIn(self.material.uuid, self.model._material_map)
        self.assertEqual(self.model.get_material_by_uuid(self.material.uuid), self.material)

        self.assertIn(self.model, self.mesh.owners)
        self.assertEqual(self.mesh.owners[self.model], num)
        self.assertIn(self.mesh, self.model.meshes)
        self.assertIn(self.mesh.uuid, self.model._mesh_map)
        self.assertEqual(self.model.get_mesh_by_uuid(self.mesh.uuid), self.mesh)

    def assert_not_shared(self):
        self.assertNotIn(self.model, self.texture.owners)
        self.assertNotIn(self.texture, self.model.textures)
        self.assertNotIn(self.texture.uuid, self.model._texture_map)
        self.assertIsNone(self.model.get_texture_by_uuid(self.texture.uuid))

        self.assertNotIn(self.model, self.material.owners)
        self.assertNotIn(self.material, self.model.materials)
        self.assertNotIn(self.material.uuid, self.model._material_map)
        self.assertIsNone(self.model.get_material_by_uuid(self.material.uuid))

        self.assertNotIn(self.model, self.mesh.owners)
        self.assertNotIn(self.mesh, self.model.meshes)
        self.assertNotIn(self.mesh.uuid, self.model._mesh_map)
        self.assertIsNone(self.model.get_mesh_by_uuid(self.mesh.uuid))

    def test_adding_a_child_shares_data(self):
        with mock.patch.object(self.mesh, "add_owner", wraps=self.mesh.add_owner) as mesh_add_owner, \
                mock.patch.object(self.material, "add_owner", wraps=self.material.add_owner) as material_add_owner, \
                mock.patch.object(self.texture, "add_owner", wraps=self.texture.add_owner) as texture_add_owner:
            self.root.add_child(self.obj)
            mesh_add_owner.assert_called_once_with(self.model)
            material_add_owner.assert_called_once_with(self.model)
            texture_add_owner.assert_called_once_with(self.model)
        self.assert_shared()

    def test_adding_a_child_late_shares_data(self):
        self.detached.add_child(self.obj)
        with mock.patch.object(self.mesh, "add_owner", wraps=self.mesh.add_owner) as mesh_add_owner, \
                mock.patch.object(self.material, "add_owner", wraps=self.material.add_owner) as material_add_owner, \
                mock.patch.object(self.texture, "add_owner", wraps=self.texture.add_owner) as texture_add_owner:
            self.root.add_child(self.detached)
            mesh_add_owner.assert_called_once_with(self.model)
            material_add_owner.assert_called_once_with(self.model)
            texture_add_owner.assert_called_once_with(self.model)
        self.assert_shared()

    def test_removing_a_child_unshares_data(self):
        self.root.add_child(self.obj)
        with mock.patch.object(self.mesh, "remove_owner", wraps=self.mesh.remove_owner) as mesh_remove_owner, \
                mock.patch.object(self.material, "remove_owner", wraps=self.material.remove_owner) as material_remove_owner, \
                mock.patch.object(self.texture, "remove_owner", wraps=self.texture.remove_owner) as texture_remove_owner:
            self.root.remove_child(self.obj)
            mesh_remove_owner.assert_called_once_with(self.model)
            material_remove_owner.assert_called_once_with(self.model)
            texture_remove_owner.assert_called_once_with(self.model)
        self.assert_not_shared()

    def test_removing_a_child_that_is_still_used_decrements_count_but_keeps_data(self):
        self.root.add_child(self.obj)
        obj = Object3D(mesh=self.mesh)
        self.root.add_child(obj)
        self.assert_shared(2)
        self.root.remove_child(self.obj)
        self.assert_shared(1)

    def test_adding_an_object_does_call_delegate(self):
        self.root.add_child(self.detached)
        self.delegate.on_object3d_added.assert_called_once_with(self.model, self.detached)

    def test_adding_a_tree_does_not_call_delegate_for_children(self):
        self.root.add_child(self.obj)
        self.delegate.on_object3d_added.assert_called_once_with(self.model, self.obj)

    def test_adding_mesh_late_shares_data(self):
        obj = Object3D()
        self.root.add_child(obj)
        with mock.patch.object(self.mesh, "add_owner", wraps=self.mesh.add_owner) as mesh_add_owner, \
                mock.patch.object(self.material, "add_owner", wraps=self.material.add_owner) as material_add_owner, \
                mock.patch.object(self.texture, "add_owner", wraps=self.texture.add_owner) as texture_add_owner:
            obj.mesh = self.mesh
            mesh_add_owner.assert_called_once_with(self.model)
            material_add_owner.assert_called_once_with(self.model)
            texture_add_owner.assert_called_once_with(self.model)
        self.assert_shared()

    def test_removing_mesh_late_shares_data(self):
        obj = Object3D(mesh=self.mesh)
        self.root.add_child(obj)
        with mock.patch.object(self.mesh, "remove_owner", wraps=self.mesh.remove_owner) as mesh_remove_owner, \
                mock.patch.object(self.material, "remove_owner", wraps=self.material.remove_owner) as material_remove_owner, \
                mock.patch.object(self.texture, "remove_owner", wraps=self.texture.remove_owner) as texture_remove_owner:
            obj.mesh = None
            mesh_remove_owner.assert_called_once_with(self.model)
            material_remove_owner.assert_called_once_with(self.model)
            texture_remove_owner.assert_called_once_with(self.model)
        self.assert_not_shared()

    def test_adding_geometry_late_shares_data(self):
        mesh = Mesh()
        obj = Object3D(mesh=mesh)
        self.root.add_child(obj)
        with mock.patch.object(self.material, "add_owner", wraps=self.material.add_owner) as material_add_owner, \
                mock.patch.object(self.texture, "add_owner", wraps=self.texture.add_owner) as texture_add_owner:
            mesh.add_geometry(self.geometry)
            material_add_owner.assert_called_once_with(self.model)
            texture_add_owner.assert_called_once_with(self.model)
        self.assertIn(self.model, self.texture.owners)
        self.assertEqual(self.texture.owners[self.model], 1)
        self.assertIn(self.model, self.material.owners)
        self.assertEqual(self.material.owners[self.model], 1)

    def test_removing_geometry_late_shares_data(self):
        self.root.add_child(self.obj)
        with mock.patch.object(self.material, "remove_owner", wraps=self.material.remove_owner) as material_remove_owner, \
                mock.patch.object(self.texture, "remove_owner", wraps=self.texture.remove_owner) as texture_remove_owner:
            self.mesh.remove_geometry(self.geometry)
            material_remove_owner.assert_called_once_with(self.model)
            texture_remove_owner.assert_called_once_with(self.model)

        self.assertNotIn(self.model, self.texture.owners)
        self.assertNotIn(self.model, self.material.owners)
        self.assertIn(self.model, self.mesh.owners)

    def test_adding_material_late_shares_data(self):
        geometry = Geometry()
        mesh = Mesh(geometries=[geometry])
        obj = Object3D(mesh=mesh)
        self.root.add_child(obj)
        with mock.patch.object(self.material, "add_owner", wraps=self.material.add_owner) as material_add_owner, \
                mock.patch.object(self.texture, "add_owner", wraps=self.texture.add_owner) as texture_add_owner:
            geometry.material = self.material
            material_add_owner.assert_called_once_with(self.model)
            texture_add_owner.assert_called_once_with(self.model)
        self.assertIn(self.model, self.texture.owners)
        self.assertEqual(self.texture.owners[self.model], 1)
        self.assertIn(self.model, self.material.owners)
        self.assertEqual(self.material.owners[self.model], 1)

    def test_removing_material_late_shares_data(self):
        geometry = Geometry(material=self.material)
        mesh = Mesh(geometries=[geometry])
        obj = Object3D(mesh=mesh)
        self.root.add_child(obj)
        with mock.patch.object(self.material, "remove_owner", wraps=self.material.remove_owner) as material_remove_owner, \
                mock.patch.object(self.texture, "remove_owner", wraps=self.texture.remove_owner) as texture_remove_owner:
            geometry.material = None
            material_remove_owner.assert_called_once_with(self.model)
            texture_remove_owner.assert_called_once_with(self.model)

        self.assertNotIn(self.model, self.texture.owners)
        self.assertNotIn(self.model, self.material.owners)
        self.assertIn(self.model, mesh.owners)
