from unittest import TestCase
from os import getcwd
from time import sleep

from eis.editor_support.switcher_manager import SwitcherManager


class TestSwitcherManager(TestCase):
    def setUp(self):
        self._sw = SwitcherManager(config={
            "switcher.jack.driver": "dummy"
        })

    def test_video_playback(self):
        handle = self._sw.new_video(video_uri="/random/path/to/video.avi")
        self.assertEqual(handle, None)

        handle = self._sw.new_video(video_uri=getcwd() + "/res/video/big_buck_bunny_180p_stereo.avi")
        self.assertNotEqual(handle, None)

        self._sw.toggle_video_playback(handle=handle, play=True)
        self.assertEqual(self._sw.is_video_playing(handle=handle), True)

        frame_data = None
        frame_grabbed = False
        video_width = 0
        video_height = 0

        def video_callback(data, height: int, width: int):
            nonlocal video_width, video_height, frame_grabbed, frame_data
            video_width = width
            video_height = height
            frame_grabbed = True
            frame_data = data

        self._sw.link_video(handle=handle, callback=video_callback)
        while frame_grabbed is False:
            sleep(0.1)

        self.assertEqual(video_height, 180)
        self.assertEqual(video_height * video_width * 4, len(frame_data))

        self._sw.toggle_video_playback(handle=handle, play=False)
        self.assertEqual(self._sw.is_video_playing(handle=handle), False)

        result = self._sw.close_video(handle=handle)
        self.assertEqual(result, True)

    def test_audio_playback(self):
        # This test is deactivated for now as it freezes the tests
        # handle = self._sw.new_audio(audio_uri="/random/path/to/audio.wav")
        # self.assertEqual(handle, None)

        handle = self._sw.new_audio(audio_uri=getcwd() + "/res/audio/radio_sqeals.wav")
        self.assertNotEqual(handle, None)

        self._sw.toggle_audio_playback(handle=handle, play=True)
        self.assertEqual(self._sw.is_audio_playing(handle=handle), True)

        result = self._sw.close_audio(handle=handle)
        self.assertEqual(result, True)
