EiS release notes
===================

EiS 0.1.0 (2019-05-31)
----------------------
First release of EiS.

Features:
* Import 3D models (GLTF files in ASCII format, not binary)
* Import images
* Import videos
* Import sounds
* Support for immersive video rendering (as a cubemap)
* Support for spatialized audio rendering (through [SATIE](https://gitlab.com/sat-metalab/satie))
* Navigate in the 3D space
* Translate, rotate and scale objects
* Record animations
* Some preliminary physics
* Export to GTLF
