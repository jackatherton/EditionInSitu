.. eis documentation master file, created by
   sphinx-quickstart on Fri Jan 24 10:05:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

EiS API documentation
=====================

`EiS <https://sat-metalab.gitlab.io/EditionInSitu>`_ is an in-situ immersive environment creation tool. It stands for Edition In Situ. EiS allows for editing an immersive scene from inside the immersive space, using VR controllers. It is meant to be used in a creation workflow similar to what follows:

- create 2D, 3D and sound assets in separate software
- import them into EiS
- create a draft of a scenography with these assets, possibly involving animations
- export to GLTF format
- import in a 3D editor of choice (Blender for example) and improve on the draft

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   eis/eis


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
