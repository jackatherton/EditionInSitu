Contributing
============


Directory structure
-------------------

|Directory                 |Description|
|--------------------------|---|
|**bin**                   |Launch script for the client `eisclient` and server `eisserver`|
|**client**                |Client application code|
|**server**                |Server application code|
|**lib**                   |Shared libraries|
|**lib/eis**               |Shared EIS editor/engine code (though the engine code could be separated eventually)|
|**extlib**                |External libraries|
|**extlib/py-satlib**      |General purpose reusable code|
|**extlib/py-satmath**     |Custom math library|
|**extlib/py-satnet**      |Custom networking library|
|**extlib/render_pipeline**|Panda3D Render Pipeline (could be moved in the engine library if we ever separate it)|
|**config**                |Shared configuration files|
|**res**                   |Shared static resources|
|**tools**                 |Tools|
|**doc**                   |Documentation|


Virtual environment
-------------------

When running `setup.sh`, Python dependencies are installed in a virtual environment to prevent them bleeding into the system module. This is also to have a better control over which version of each module is used and prevent issues with breaking changes which might be introduced in new version of a module.

You need to activate this virtual environment when working with EiS. To do so, type the following:
```bash
source ./env/bin/activate
```

From now on, the modules installed in the virtual environment will be used, and no module installed in the system will be available anymore. To leave the virtual environment, type:
```bash
deactivate
```

Lastly, to get a list of the currently installed modules (for example to update the `requirements.txt` file), type (with the virtual environment activated):
```bash
pip3 freeze
```


Code Style & Conventions
------------------------

This project uses Python 3.6 and follows the [PEP8 style guide](https://www.python.org/dev/peps/pep-0008/).

It is mandatory that reusable code uses [type hints](https://www.python.org/dev/peps/pep-0008/). Example:

```python
property: str = None
```

In case static type checking reports false negatives, type checking can be ignored with `# type: ignore`.

```python
some.thing(complains="Without reason")  # type: ignore
```


Linting
-------

It is possible to let git ensure that you are conforming to the standards by using pre-commit hooks and clang-format:
```
sudo apt-get install python-autopep8

# Then in EditionInSitu's .git folder:
rm -rf hooks && ln -s ../.hooks hooks
```


Static Analysis
---------------

[Mypy](http://mypy-lang.org/) is used for static type checking.

Install mypy using the latest github version:

    python3 -m pip install -U git+git://github.com/python/mypy.git

Use the following to run mypy:

    MYPYPATH=lib/satlib:lib/satnet:lib/eis mypy --strict client|server

Use either `client` or `server` to check one or the other. If you want to have both checked at the same time, you can run `./tools/type_check.sh` from the project root directory. It will run both client and server and remove duplicate entries from shared code, but it will be slower since it has to run both and a lot of code is shared.


Unit Tests
----------

Tests are written using Python's `unittest`. To run all tests at once, use the `test.sh` script:

```bash
./tools/test.sh
```

### Adding new test cases

To add new tests, refer to the structure of the **lib/eis/tests** subdirectory. The simplest test case looks as follows:

```python
from unittest import TestCase

class DummyTest(TestCase):
    def test_truth(self):
        self.assertEqual(True, True)
```

See the Python [unittest](https://docs.python.org/3.6/library/unittest.html) documentation for more details on what is possible to do, especially regarding `mocks` which allow for tooling classes to test more precisely what happens.


Profiling
---------

### CPU profiling

For CPU-time profiling, the Python [profiling module](https://github.com/what-studio/profiling) is embedded inside EiS to facilitate its usage. Currently it profiles the whole server and/or client processes, but live-profiling could probably be added if necessary. To profile the EiS client, do the following:

```bash
cd bin
./eisclient --profile
```

Then do whatever you have to do to activate the code section you need to profile. Once done, quit the process (either by pressing `ctrl-c` in the terminal or by closing the window). A detailed tree-like view of the profile will appear in the terminal, in which you can navigate with the arrow keys to pinpoint the code paths which eat through your resources.

![CPU profiling](./images/cpu_profiling.png)


### Rendering profiling

For profiling the render engine, and when using the Panda3D as a rendering backend, you can use the Panda3D [pstats](https://docs.panda3d.org/1.10/python/optimization/using-pstats) live-profiling tool. To do so, run:

```bash
pstats &
cd bin
./eisclient
```

Then in the client press the 'p' key for it to connect to pstats. A couple new windows should appear and give you real-time information about the client. One of them shows profiling data about the whole application. You can move inside the various parts of the pipeline by double-clicking on the labels on the left, and go upwards by clicking in the gray area above the labels.

![pstats](./images/pstats.png)

The other window allows for profiling other measurements, from the node counts in the scene graph to the state changes. You'll have to dive in the software and in `pstats` documentation to make the best out of it, but it clearly is a useful profiling tool.



Extending EiS
-------------

### Adding a new Behavior

Behaviors are defined in `lib/eis/eis/graph/behaviors`, and derive from Behavior. When adding a new behavior, a new behavior id must be created and added to `lib/eis/eis/__init__.py`. For example:

```python
# Behaviors
BEHAVIOR_TEST = 0x40
BEHAVIOR_CYCLE_SCALE = 0x41
BEHAVIOR_MY_AWESOME_BEHAVIOR = 0x42 # This is a new behavior
```

The new behavior must also be imported in `lib/eis/eis/all_serializable.py`:

```python
...
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from eis.graph.behaviors.random_motion_behavior import RandomMotionBehavior
from eis.graph.behaviors.tracker_behavior import TrackerBehavior
from eis.graph.behaviors.test_behavior import TestBehavior
from eis.graph.behaviors.my_awesome_behavior import MyAwesomeBehavior
...
```

Then create a new file in `lib/eis/eis/graph/behaviors/my_awesome_behavior.py`:

```python
from eis import EISEntityId
from eis.graph.behavior import Behavior, behavior

@behavior(id=EISEntityId.BEHAVIOR_MY_AWESOME_BEHAVIOR)
class MyAwesomeBehavior(Behavior):
    def __init__(self) -> None:
        super().__init__()

    def step(self, now: float, dt: float) -> None:
        # Do some stuff

    def added_to_object(self) -> None:
        super().added_to_object()

    def removed_from_object(self) -> None:
        super().removed_to_object()
```

It is possible to set up a behavior which does nothing on the server when created from a project file, and is only active on the clients. The trick is to use the `CLIENT` constant defined in `eis.constants`:

```python
from eis.constants import CLIENT
from eis.graph.behavior import behavior, Behavior

@behavior(id=EISEntityID.BEHAVIOR_CLIENT_ONLY)
class ClientOnlyBehavior(Behavior):

    _fields = ['_some_param']

    def __init__(self, some_param: float = 1.0) -> None:
        super().__init__()
        self._some_param = other_param

    def step(self, now: float, dt: float) -> None:
        if CLIENT:
            # Do some client-side stuff
            ...
        else:
            # Do some server-side stuff
            ...
```

Then in the configuration file:

```json
{
  "behaviors" : {
    "some_object" : {
      "ClientOnlyBehavior" : {
        "client_sync" : true,
        "kwargs" : {
          "some_param" : 3.14159
        }
      }
    }
  }
}
```

### Adding a new event handler

Event handlers are attached to behaviors and create a new behavior when triggered or untriggered by a condition determined inside the class itself. Currently only one exists, `CollisionHandler` and it is inside `lib/eis/eis/graph/behaviors\physics_behavior`. When adding a new event handler, a new event handler id must be created and added to `lib/eis/eis/__init__.py`. For example:

```python
# Event handlers
EVENT_HANDLER_COLLISION = 0x50
EVENT_HANDLER_I_BARELY_KNOW_HER = 0x51
```

The new event handler must also be imported in `lib/eis/eis/all_serializable.py`.

This is how you create a derived `EventHandler` class:

```python
from typing import Any, Dict, Optional

from eis import EISEntityId
from eis.graph.behavior import Behavior
from eis.graph.event_handler import EventHandler, event_handler

@event_handler(id=EISEntityId.EVENT_HANDLER_I_BARELY_KNOW_HER)
class BarelyHandler(EventHandler):
    def __init__(self,
                 parent: Optional[Behavior] = None,
                 trigger_behavior: Optional[str] = None,
                 trigger_kwargs: Optional[Dict[str, Any]] = None,
                 untrigger_behavior: Optional[str] = None,
                 untrigger_kwargs: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(parent=parent,
                         trigger_behavior=trigger_behavior,
                         trigger_kwargs=trigger_kwargs,
                         untrigger_behavior=untrigger_behavior,
                         untrigger_kwargs=untrigger_kwargs)

    def my_condition(self) -> bool:
        # Only an example to show you must have a condition to determine the state of the handler (triggered on untriggered)
        ...

    def step(self, now: float, dt: float) -> None:
        parent = self._parent
        if not parent:
            return

        if not self._triggered and self.my_condition():
            self.on_triggered()
        elif self._triggered and not self.my_condition():
            self.on_untriggered()

```
`trigger_behavior` and `untrigger_behavior` are types in string form to identify the type of behaviors to create.
`trigger_kwargs` and `untrigger_kwargs` are dictionaries of arguments for the future behaviors that will be created. They can be `None` and must fit the arguments of the associated behavior type.

Just like the behaviors, it is possible to set up an event handler that does nothing on the server when created from a project file, and is only active on the clients or the server.

It is possible to add an event handler to all the behaviors of a certain type from the configuration file:

```json
{
  "behaviors" : {
    "some_object" : {
      "MyAwesomeBehavior" : {
        "kwargs" : {
          "some_param" : 3.14159
        }
      }
    }
  },
  "event_handlers" : {
    "MyAwesomeBehavior": {
      "handler_key" : {
        "handler" : "BarelyHandler",
        "trigger_behavior": "TrigBehavior",
        "trigger_kwargs" : {
          "some_param" : 3.14159
        },
        "untrigger_behavior": "UntrigBehavior",
        "untrigger_kwargs" : {
          "some_other_param" : "coincoin"
        }
      }
    }
  }
}
```

You can also do it for a specific object or wildcard:

```json
{
  "behaviors" : {
    "some_object" : {
      "ClientOnlyBehavior" : {
        "client_sync" : true,
        "kwargs" : {
          "some_param" : 3.14159
        },
        "event_handlers": {
          "collision_handler_highlight": {
            "handler": "CollisionHandler",
            "trigger_behavior": "HighlightBehavior"
          },
          "collision_handler_motion": {
              "handler": "CollisionHandler",
              "trigger_behavior": "RandomMotionBehavior"
          }
        }
      }
    }
  }
}
```

The specific one will have precedence over the global one if both are defined.
