Installation and usage
======================

Get the EiS code
----------------
This repository uses git-lfs. You need to have git-lfs installed to work with it.

```bash
sudo apt install git-lfs
mkdir -p ~/src
cd ~/src
git clone https://gitlab.com/sat-metalab/EditionInSitu.git
```

Quick install
-------------

Run:

```bash
cd ~/src/EditionInSitu
 ./setup.sh
```

If asked about building C++ modules for RenderPipeline answer **yes** and if prompted to download samples you can say **no** as we don't require them.


Generating the documentation
----------------------------

Documentation is generated using Sphinx for the API and MkDocs for the rest. It is generated by the CI and automatically uploaded to the [offical website](https://sat-metalab.gitlab.io/EditionInSitu) when a new commit is pushed either in the `master` branch, or in a `website` branch which can be forced pushed if needed.

To generate the documentation locally on a Ubuntu system, first install EiS as described before, then:

```bash
sudo apt install python3-pip
pip3 install mkdocs markdown-include mkdocs-bootswatch sphinx sphinx-autodoc-typehints sphinx-rtd-theme
mkdocs build -f doc/mkdocs.yml
cd doc/sphinx
mkdir -p eis
sphinx-apidoc -P -o ./eis ../../lib/eis
make html
cp -r _build/html ../../static/sphinx
cd ../..
```

Then open the file located at `./static/index.html`.


Running the Application
-----------------------
You need to run two applications: the server that keeps the state of the 3D scene, and at least one client that takes care of display. You can have both running in the same computer. They are located in the `bin` directory.


### Server
Launch using

```bash
~/src/EditionInSitu/bin/eisserver
```

Use the `-h` or `--help` option to see usage information.

```bash
~/src/EditionInSitu/bin/eisserver -h
```

### Configuration with Project file
Project files specify the configuration of the server, written in JSON. It currently enables setting behaviors to objects based on their names. The syntax is as follows, and accepts wildcards for object names:

```json
{
  "behaviors" : {
    "some_object" : {
      "some_behavior" : {
        "client_sync" : false,
        "kwargs" : {
            "arg_1" : 2,
            "arg_2" : "kartoffel"
        }
      }
    },
    "other_objects_.*" : {
      "another_behavior" : {}
    }
  }
}
```

Parameters for the behaviors are the following:

* client_sync: if true, the behavior is synced to all clients. This enables creating behaviors which have effect on the server and the client, or only on the client, for example for behaviors controlling sound.
* kwargs: arguments which will be sent to the behavior.

The server must be launched with the `--project` option:

```bash
~/src/bin/EditionInSitu/bin/eisserver --project project.json
```

### Local configuration
A local configuration file can be as `config/local.json`.
The configurations set in this file will override the configurations set in the other json files, and can set additional things. The syntax is as follows:

```json
{
  "config_section" : {
    "config.name" : "config value"
  }
  "editor" : {
    "input.mouse_picker.enabled" : true,
    "input.vive.enabled" : false,
    "input.vive.vrpn.host" : "localhost",
    "pod.model_path" : "res/models/dome/dome.bam",
    "assets.assets_path" : "res/sounds"
  }
}
```


### Client
Launch using `bin/eisclient`.

Use the `-h` or `--help` option to see usage information.


### Assets
Assets are image, 3D model, sound or video files that can be loaded in EIS. If assets are going to be used:
* The relative path to the folder(s) used for the assets has to be specified in the local configuration file. It is set with `"assets.assets_path" : "res/sounds"` in the `"editor"` section. Multiple paths can be set, separated by `:`. 
* It is also possible to set the maximum number of assets per floor as well as the loading distance for assets, with the configs `"assets.assets_per_floor" : 24` and `"assets.asset_loading_distance" : 4` respectively, also in the`"editor"` section.


### SATIE client and sound assets
For sound assets to be used locally, a SATIE server needs to be launched locally as well.
* First, install SATIE and supercollider.
* Open the supercollider file in `res/satie_config/soundAssetsServer.scd`.
* Set the SATIE server to localhost in the EIS configuration files using `"satie.server" : "localhost"` in the `"editor"` section.
* Launch the SATIE server, then launch EIS.
* Add assets to the scene, and you should hear the playback.

### Physics
Physics behaviors can take a Wavefront .obj file to use as their bounding box.
* Make sure the model is separated in convex objects, do not join different objects together.
* When exporting the .obj from blender, set forward to Y and up to Z

### Note
This project uses [Render Pipeline](https://github.com/tobspr/RenderPipeline)

### Optional Optimization with Cython
It is possible to compile the Python sources with Cython to improve overall performances, although it is not an obligation. To do so, run the following command:

```bash
make
```

To remove any generated file, call:

```bash
make clean
```
