PYTHON=python3
GIT=git

.PHONY: all clean EIS SATLIB SATMATH SATNET

all: SATLIB SATMATH SATNET EIS

EIS:
	$(GIT) submodule update --init --recursive
	cd lib/eis && \
	$(PYTHON) setup.py build_ext --inplace

SATLIB:
	cd extlib/py-satlib && \
	$(PYTHON) setup.py build_ext --inplace

SATMATH:
	cd extlib/py-satmath && \
	$(PYTHON) setup.py build_ext --inplace

SATNET:
	cd extlib/py-satnet && \
	$(PYTHON) setup.py build_ext --inplace

CYTHON_FILES := $(shell find . -name '*cpython*.so')
CPP_FILES := $(shell find . -name '*cpython*so' -exec bash -c 'echo {} | sed s/.cpython.*.so/.cpp/' \;)
clean:
	rm -f $(CPP_FILES)
	rm -f $(CYTHON_FILES)
	rm -rf lib/eis/build
	rm -rf extlib/py-satlib/build
	rm -rf extlib/py-satnet/build
	rm -rf extlib/py-satmath/build
