#!/bin/bash
export MYPYPATH=extlib/py-satlib:extlib/py-satnet:extlib/py-satmath:lib/eis

OPTIONS="--incremental --ignore-missing-import"
{ mypy ${OPTIONS} client; mypy ${OPTIONS} server; } | awk '!seen[$0]++'
